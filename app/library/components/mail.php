<?php
namespace Components;
require_once __DIR__ . '/../../vendor/swiftmailer/lib/swift_required.php';
/**
*
* Sends e-mails based on pre-defined templates
*/
class Mail extends Component
{
	protected $_transport;

/**
* Sends e-mails via gmail based on predefined templates
*
* @param array $to
* @param string $subject
* @param string $name
* @param array $params
*/
public function send($to, $subject, $name, $params)
{
//Settings
	$mailSettings = $this->config->mail;
	$template = $name . $params;
// Create the message
	$message = Swift_Message::newInstance()
	->setSubject($subject)
	->setTo($to)
	->setFrom(array(
		$mailSettings->fromEmail => $mailSettings->fromName
		))
	->setBody($template, 'text/html');
	if (!$this->_transport) {
		$this->_transport = Swift_SmtpTransport::newInstance(
			$mailSettings->smtp->server,
			$mailSettings->smtp->port,
			$mailSettings->smtp->security
			)
		->setUsername($mailSettings->smtp->username)
		->setPassword($mailSettings->smtp->password);
	}
// Create the Mailer using your created Transport
	$mailer = Swift_Mailer::newInstance($this->_transport);
	return $mailer->send($message);
}
}