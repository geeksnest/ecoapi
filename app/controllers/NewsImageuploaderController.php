<?php
namespace Controllers;
use \Models\Newsimage as Newsimage;
use \Models\Newsvideo as Newsvideo;
use \Models\Authorimage as Authorimage;
class NewsImageuploaderController extends \Phalcon\Mvc\Controller
{

    public function ajaxfileuploaderAction($filename, $type){


        $filename = $_POST['imgfilename'];
        $picture = new Newsimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        echo json_encode($data);        
    }

    public function savevideoAction(){

        $video = $_POST['embedvideo'];

        $guid = new \Utilities\Guid\Guid();
        $videoid = $guid->GUID();

        $vid = new Newsvideo();
        $vid->assign(array(            
            'videoid' => $videoid,
            'video' => $video
            ));

        if (!$vid->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

        echo json_encode($data);     
    }
    
    public function imagelistAction(){

        $getImage= Newsimage::find(array("order" => "id DESC"));
        foreach ($getImage as $getImages) {;
            $data[] = array(
                'id'   => $getImages->id,
                'filename'   => $getImages->filename
                );
        }
        echo json_encode($data);
    }
    
    public function videolistAction(){

        $getVideo= Newsvideo::find(array("order" => "num DESC"));
        foreach ($getVideo as $getVideo) {;
            $data[] = array(
                'videoid'   => $getVideo->videoid,
                'video'   => $getVideo->video
                );
        }
        echo json_encode($data);
    }

    public function updateinfoimgAction(){
        $id = $_POST['id'];
        $sliderimages = Newsimage::findFirst('id='.$id.' ');
        $sliderimages->description = $_POST['description'];
        $sliderimages->title= $_POST['title'];
        if(!$sliderimages->save()){
            echo 'Error';
        }else{

        }
    }

    public function dltnewsimgAction(){
        $id = $_POST['id'];
        $dltPhoto = Newsimage::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }

    public function dltnewsvidAction(){
        $id = $_POST['videoid'];
        $dlt = Newsvideo::findFirst('videoid = "'.$id.'"');
        $data = array('error' => 'Not Found');
        if ($dlt) {
            if($dlt->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }

    ////////////////////==========AUTHOR===========////////////////

    public function uploadauthorimageAction(){

        $filename = $_POST['imgfilename'];
        $picture = new Authorimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        
    }

    
    public function authorimagelistAction(){

        $getImage= Authorimage::find(array("order" => "id DESC"));
        foreach ($getImage as $getImages) {;
            $data[] = array(
                'id'   => $getImages->id,
                'filename'   => $getImages->filename
                );
        }
        echo json_encode($data);

    }


    public function dltauthorimageAction(){

        $id = $_POST['id'];
        $dltPhoto = Authorimage::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }

}

