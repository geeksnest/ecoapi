<?php

namespace Controllers;

use \Models\Members as Members;
use \Models\Donationlog as Donationlog;
use \Models\Donationlog1 as Donationlog1;
use \Models\Donationlogothers as Donationlogothers;
use \Models\Donationlogothers1 as Donationlogothers1;
use \Models\Donationloghouston as Donationloghouston;
use \Models\Donationlogseattle as Donationlogseattle;
use \Models\Donation as Donation;
use \Models\Centername as Centername;
use \Models\Callheroesonduty as Callheroesonduty;
use \Models\Memberconfirmation as Memberconfirmation;
use Controllers\ControllerBase as CB;
use PHPMailer as PHPMailer;
use MailChimp as MailChimp;

class MembersController extends \Phalcon\Mvc\Controller {

    public function centernamesAction() {
        $mc = new MembersController;
        $getcname = Centername::find()->toArray();
        $var = json_encode($getcname, JSON_NUMERIC_CHECK);
        echo json_encode($mc->utf8izeAction($getcname), JSON_NUMERIC_CHECK);
    }

    public function utf8izeAction($d) {
        $mc = new MembersController;
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $mc->utf8izeAction($v);
            }
        } else if (is_string($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

    public function addToMemberAction() {       

        $data = array();

            if ($_POST) {
                // Search Member table if email is exists
                $userEmail = Members::findFirst("email='" . $_POST['email'] . "'");
                if ($userEmail == true) {
                    ($userEmail == true) ? $data["emailtaken"] = "Email already taken." : '';
                } else {

                    // Insert data to Members table
                    $bnbuser = new Members();
                    $a = '';
                    for ($i = 0; $i < 6; $i++) {
                        $a .= mt_rand(0, 9);
                    }
                    $password = sha1($a);

                    if ($_POST['bmonth'] < 10 ){
                        $bm = '-0'. $_POST['bmonth'];
                    }else{
                        $bm = '-'. $_POST['bmonth'];
                    }

                    if ($_POST['bday'] < 10 ){
                        $bd = '-0'. $_POST['bday'];
                    }else{
                        $bd = '-'. $_POST['bday'];
                    }


                    $loc = $_POST['location']['name'];
                    $bnbuser->assign(array(
                        // 'username' => $_POST['fname'],
                        'email' => $_POST['email'],
                        'password' => $password,
                        'firstname' => $_POST['fname'],
                        'lastname' => $_POST['lname'],
                        'birthday' => $_POST['byear'] .''. $bm . '' . $bd,
                        'gender' => $_POST['gender'],
                        'location' => $_POST['location']['name'],
                        'zipcode' => $_POST['zip'],
                        'status' => 2,
                        'centername' => $_POST['centersname'],
                        'howdidyoulearn' => $_POST['howdidyoulearn'],
                        'refer' => $_POST['refer'],
                        'date_created' => date('Y-m-d'),
                        'date_updated' => date("Y-m-d H:i:s"),
                        'reg_inden' => 'Offline registration'
                    ));

                    if (!$bnbuser->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                        echo json_encode(["error" => $bnbuser->getMessages()]);
                    } else {   

                        // Insert data to Memberconfirmation table
                        $confirmation = new Memberconfirmation();
                        $confirmationCode = sha1($bnbuser->userid);
                        $confirmation->assign(array(
                            'members_id' => $bnbuser->userid,
                            'members_code' => $confirmationCode
                        ));

                        if (!$confirmation->save()) {
                            $data['error'] = "Something went wrong saving the data, please try again.";
                            foreach ($confirmation->getMessages() as $message) {
                                echo $message;
                            }
                        } else {

                            if ($_POST['paymode'] != 'None') {
                                if ($_POST['paymode'] == 'ECO Program') {
                                    $amountpay = 0;
                                }else{
                                    $amountpay = $_POST['amt'];
                                }

                                $checkmode = $_POST['rn'] . '-' . $_POST['an'] . '-' . $_POST['cn'];
                                // Insert data to Donationlog table
                                $bnbdonor = new Donationlog();
                                $bnbdonor->assign(array(
                                    'useremail' => $_POST['email'],
                                    'transactionId' => '',
                                    'datetimestamp' => date("Y-m-d H:i:s"),
                                    'amount' => $amountpay,
                                    'paymentmode' => $_POST['paymode'],
                                    'forcheckmode' => $checkmode
                                ));

                                if (!$bnbdonor->save()) {
                                    $data['error'] = "Something went wrong saving the data, please try again.";
                                } else {
                                    $data['success'] = "Success";
                                }


                            }


                            $data['success'] = "User profile has been stored.";


                            $MailChimp = new MailChimp('289ce16d32317e58af6e65afe7a6c538-us10');
                            $result = $MailChimp->call('lists/subscribe', array(
                                'id'                => 'fc51891e6f',
                                'email'             => array('email'=>$_POST['email']),
                                'merge_vars'        => array('FNAME'=>$_POST['fname'], 'LNAME'=>$_POST['lname']),
                                'double_optin'      => false,
                                'update_existing'   => true,
                                'replace_interests' => false,
                                'send_welcome'      => false,
                                ));


                            $dc = new CB();
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => 'Earth Citizen Organizations Confirmation Email',
                                'HtmlBody' => 'Welcome to ECO, '.$_POST['fname'].' '.$_POST['lname'].' <br>  <br> Please click the confirmation link below to complete and activate your account. <br><br> Your Temporary Password: '.$a.' <br><br> Create username/Change Password Link: <br><a href="' . $GLOBALS["baseURL"] . '/newmember/'.$bnbuser->userid.'">' . $GLOBALS["baseURL"] . '/newmember/'.$bnbuser->userid.'</a><br><br>You are the newest earth citizen, and we are very excited that you’re here because it means we are one step further toward mindful living and a sustainable world. By meeting with other like-minded Earth Citizens and joining ECO’s activities, locally and nationally, you will find hope becomes real and change gets easier for you and others.<br><br>Please visit our websites to learn more about ECO and our programs. <br><a href="www.earthcitizens.org">www.earthcitizens.org</a> <br><a href="www.heroesconnect.org ">www.heroesconnect.org </a> <br><br>Thanks,<br><br>ECO Staff'
                            ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                                //////refer

                                if(isset($_POST['refer'])){
                                    $json = json_encode(array(
                                        'From' => $dc->config->postmark->signature,
                                        'To' => $_POST['refer'],
                                        'Subject' => 'Earth Citizen Organization Member Invitaion',
                                        'HtmlBody' => 'Hello friend, <br/><br/> We are pleased to let you know '. $_POST['fname'] .' '. $_POST['lname'] .' wanted you to become a member of the Earth Citizens Organization (ECO). <br><br> ECO was established as a 501(c)(3) nonprofit organization to promote mindful living for a sustainable world. The name of the organization represents the understanding that we all are citizens of the Earth and share the responsibility for its well-being. <br><br> We all know that we need to change to make our lives on this planet sustainable. To make the change feasible, ECO proposes to start making changes from soft simple things such as the way we breathe, the way we eat, the way we manage our stress, and the way we feel toward other people and other living beings. <br><br> These elements, together with training and practices for mindfulness and leadership, are the foundation of the training and education that ECO provides for the people who desire to initiate the change in their lives and their community. Graduates of our programs return to their communities ready to share what they have learned and experienced. The generous donations of individuals and supporting institutions make it possible for ECO to provide this invaluable education below cost. For a $10 minimum donation, you can become a part of the community of Earth Citizens. <br><br> To join ECO and become an Earth Citizen, please visit the link below. <br> <br> <a href="' . $GLOBALS["baseURL"] . '/donation"> '.$GLOBALS["baseURL"].'/donation </a> <br> <br> Thank you for your kind interest, <br> <br> ECO Staff'
                                    ));

                                    $ch2 = curl_init();
                                    curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                                    curl_setopt($ch2, CURLOPT_POST, true);
                                    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                        'Accept: application/json',
                                        'Content-Type: application/json',
                                        'X-Postmark-Server-Token: '.$dc->config->postmark->token
                                    ));
                                    curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                                    $response = json_decode(curl_exec($ch2), true);
                                    $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                                    curl_close($ch2);
                                }

                                if ($http_code==200) {
                                    $data = array('success' => 'success');
                                } else {
                                    $data = array('error' => $response);
                                }
                            }

                            
                        // end member confirmation save
                        }
                    // end member save
                    }
                // end user email check 
                }
            // end post
            }
        echo json_encode($data);
    }

    public function registrationCheckAction(){
            $userName = Members::findFirst("username='" . $_POST['username'] . "'");
            $userEmail = Members::findFirst("email='" . $_POST['email'] . "'");
            $userStatus = Members::findFirst("status='" . 0 . "' AND email='" . $_POST['email'] . "'");
            $userStatus2 = Members::findFirst("status='" . 2 . "' AND email='" . $_POST['email'] . "'");
            $data = array('success' => 'Username and email ok!');
            if ($userName == true) {
                ($userName == true) ? $data["usernametaken"] = "Username already taken." : '';
            }
            if ($userEmail == true && $userStatus != true && $userStatus2 != true) {
                ($userEmail == true) ? $data["emailtaken"] = "Email already taken." : '';
            }
            //check if not yet confirm 
            if ($userStatus == true && $userEmail == true) {
                ($userStatus == true) ? $data["notyetconfirm"] = "Not yet confirm email." : '';                
            }
            //check if not yet completed 
            if ($userStatus2 == true && $userEmail == true) {
                ($userStatus2 == true) ? $data["notyetcompleted"] = "Not yet completed registration." : '';                
            }
            echo json_encode($data);
    }

    public function resendconAction($email){

        $userEmail = Members::findFirst("email='" . $email . "'");
        
        
        if ($userEmail == true) {
            $data = array('success' => 'Username and email ok!');
            $re_email = Memberconfirmation::findFirst("members_id='" . $userEmail->userid . "'");

            $dc = new CB();      
            $json = json_encode(array(
                'From' => $dc->config->postmark->signature,
                'To' => $email,
                'Subject' => 'Earth Citizen Organization Confirmation Email',
                'HtmlBody' => '<h3>Welcome to ECO, '.$userEmail->firstname.' '.$userEmail->lastname.' </h3> Please click the confirmation link below to activate your account. <br/> <br/> <a href="' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $re_email->members_id . '/' . $re_email->members_code . '">' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $re_email->members_id . '/' . $re_email->members_code . '</a> <br/><br/> You are the newest earth citizen, and we are very excited that you’re here because it means we are one step further toward mindful living and a sustainable world. By meeting with other like-minded Earth Citizens and joining ECO’s activities, locally and nationally, you will find hope becomes real and change gets easier for you and others. <br/><br/> Please visit our websites to learn more about ECO and our programs. <br/> <a href="www.earthcitizens.org">www.earthcitizens.org </a> <br/> <a href="www.heroesconnect.org">www.heroesconnect.org</a> <br> <br> Thanks, <br><br>ECO Staff'
                ));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $dc->config->postmark->url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: application/json',
                'Content-Type: application/json',
                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                ));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $response = json_decode(curl_exec($ch), true);
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            ($userEmail == true) ? $data["confirmation"] = "Confirmation email has been sent." : '';
        }

      
            
        echo json_encode($data);
    }

    public function resendcomAction($email){
        
        $userEmail = Members::findFirst("email='" . $email . "'");
        
        if ($userEmail == true) {
            
            $conditions = "email='" . $email . "'";
            $re_complete = Members::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
                    $a = '';
                    for ($i = 0; $i < 6; $i++) {
                        $a .= mt_rand(0, 9);
                    }
                    $password = sha1($a);

                    
                    if ($re_complete) {
                        $re_complete->password = $password;
                    }
                    if ($re_complete->save()) {
                    $data = array('success' => 'Username and email ok!');

                        $dc = new CB();      
                        $json = json_encode(array(
                            'From' => $dc->config->postmark->signature,
                            'To' => $email,
                            'Subject' => 'Earth Citizen Organizations Confirmation Email',
                            'HtmlBody' => 'Welcome to ECO, '.$userEmail->firstname.' '.$userEmail->lastname.' <br>  <br> Please click the confirmation link below to complete and activate your account. <br><br> Your Temporary Password: '.$a.' <br><br> Create username/Change Password Link: <br><a href="' . $GLOBALS["baseURL"] . '/newmember/index/'.$userEmail->userid.'">' . $GLOBALS["baseURL"] . '/newmember/'.$userEmail->userid.'</a><br><br>You are the newest earth citizen, and we are very excited that you’re here because it means we are one step further toward mindful living and a sustainable world. By meeting with other like-minded Earth Citizens and joining ECO’s activities, locally and nationally, you will find hope becomes real and change gets easier for you and others.<br><br>Please visit our websites to learn more about ECO and our programs. <br><a href="www.earthcitizens.org">www.earthcitizens.org</a> <br><a href="www.heroesconnect.org ">www.heroesconnect.org </a> <br><br>Thanks,<br><br>ECO Staff'
                            ));

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $dc->config->postmark->url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Accept: application/json',
                            'Content-Type: application/json',
                            'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                        $response = json_decode(curl_exec($ch), true);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

            ($userEmail == true) ? $data["completion"] = "Completion email has been sent." : '';
            }
        }

      
            
        echo json_encode($data);
    }

    public function resendconfirmationAction(){
        $userEmail = Members::findFirst("email='" . $_POST['email'] . "'");
        
        if ($userEmail == true) {
            $data = array('success' => 'Username and email ok!');
            $re_email = Memberconfirmation::findFirst("members_id='" . $userEmail->userid . "'");

            $dc = new CB();      
            $json = json_encode(array(
                'From' => $dc->config->postmark->signature,
                'To' => $_POST['email'],
                'Subject' => 'Earth Citizen Organization Confirmation Email',
                'HtmlBody' => '<h3>Welcome to ECO, '.$userEmail->firstname.' '.$userEmail->lastname.' </h3> Please click the confirmation link below to activate your account. <br/> <br/> <a href="' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $re_email->members_id . '/' . $re_email->members_code . '">' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $re_email->members_id . '/' . $re_email->members_code . '</a> <br/><br/> You are the newest earth citizen, and we are very excited that you’re here because it means we are one step further toward mindful living and a sustainable world. By meeting with other like-minded Earth Citizens and joining ECO’s activities, locally and nationally, you will find hope becomes real and change gets easier for you and others. <br/><br/> Please visit our websites to learn more about ECO and our programs. <br/> <a href="www.earthcitizens.org">www.earthcitizens.org </a> <br/> <a href="www.heroesconnect.org">www.heroesconnect.org</a> <br> <br> Thanks, <br><br>ECO Staff'
                ));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $dc->config->postmark->url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: application/json',
                'Content-Type: application/json',
                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                ));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $response = json_decode(curl_exec($ch), true);
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            ($userEmail == true) ? $data["confirmation"] = "Email already taken." : '';
        }

      
            
        echo json_encode($data);
    }

    public function resendcompletionAction(){
        $userEmail = Members::findFirst("email='" . $_POST['email'] . "'");
        
        if ($userEmail == true) {
            
            $conditions = "email='" . $_POST['email'] . "'";
            $re_complete = Members::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
                    $a = '';
                    for ($i = 0; $i < 6; $i++) {
                        $a .= mt_rand(0, 9);
                    }
                    $password = sha1($a);

                    
                    if ($re_complete) {
                        $re_complete->password = $password;
                    }
                    if ($re_complete->save()) {
                    $data = array('success' => 'Username and email ok!');

                        $dc = new CB();      
                        $json = json_encode(array(
                            'From' => $dc->config->postmark->signature,
                            'To' => $_POST['email'],
                            'Subject' => 'Earth Citizen Organizations Confirmation Email',
                            'HtmlBody' => 'Welcome to ECO, '.$userEmail->firstname.' '.$userEmail->lastname.' <br>  <br> Please click the confirmation link below to complete and activate your account. <br><br> Your Temporary Password: '.$a.' <br><br> Create username/Change Password Link: <br><a href="' . $GLOBALS["baseURL"] . '/newmember/index/'.$userEmail->userid.'">' . $GLOBALS["baseURL"] . '/newmember/'.$userEmail->userid.'</a><br><br>You are the newest earth citizen, and we are very excited that you’re here because it means we are one step further toward mindful living and a sustainable world. By meeting with other like-minded Earth Citizens and joining ECO’s activities, locally and nationally, you will find hope becomes real and change gets easier for you and others.<br><br>Please visit our websites to learn more about ECO and our programs. <br><a href="www.earthcitizens.org">www.earthcitizens.org</a> <br><a href="www.heroesconnect.org ">www.heroesconnect.org </a> <br><br>Thanks,<br><br>ECO Staff'
                            ));

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $dc->config->postmark->url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Accept: application/json',
                            'Content-Type: application/json',
                            'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                        $response = json_decode(curl_exec($ch), true);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

                        ($userEmail == true) ? $data["completion"] = "Email already taken." : '';
                    }
        }

      
            
        echo json_encode($data);
    }
    
    public function registrationAction() {
        $data = array();
        if ($_POST) {
            $userName = Members::findFirst("username='" . $_POST['username'] . "'");
            $userEmail = Members::findFirst("email='" . $_POST['email'] . "'");
            if ($userName == true || $userEmail == true) {
                ($userName == true) ? $data["usernametaken"] = "Username already taken." : '';
                ($userEmail == true) ? $data["emailtaken"] = "Email already taken." : '';
            } else {
                $user = new Members();
                $password = sha1($_POST['password']);
                if(isset($_POST['cname'])){
                    $cname = $_POST['cname'];
                }else{
                    $cname = "";
                }
                if(isset($_POST['howdidyoulearn'])){
                    $howdidyoulearn = $_POST['howdidyoulearn'];
                }else{
                    $howdidyoulearn = "";
                }
                if(isset($_POST['refer'])){
                    $refer = $_POST['refer'];
                }else{
                    $refer = "";
                }

                if(isset($_POST['bday']['val'])){
                    if($_POST['bday']['val'] < 10){
                        $bday = '-0' . $_POST['bday']['val'];
                    }else{
                        $bday = '-'. $_POST['bday']['val'];  
                    }
                    
                }else{
                    $bday = "";
                }

                if(isset($_POST['bmonth']['val'])){
                    if($_POST['bmonth']['val'] < 10){
                        $bmonth = '-0' . $_POST['bmonth']['val'];
                    }else{
                         $bmonth = '-'.$_POST['bmonth']['val'];  
                    }
                    
                }else{
                    $bmonth = "";
                }

                $user->assign(array(
                    'username' => $_POST['username'],
                    'email' => $_POST['email'],
                    'password' => $password,
                    'birthday' => $_POST['byear']['val'] .''. $bmonth . '' . $bday,
                    'gender' => $_POST['gender'],
                    'firstname' => $_POST['fname'],
                    'lastname' => $_POST['lname'],
                    'location' => $_POST['location']['name'],
                    'zipcode' => $_POST['zipcode'],
                    'centername' => $cname,
                    'howdidyoulearn' => $howdidyoulearn,
                    'refer' => $refer,
                    'status' => 0,
                    'date_created' => date('Y-m-d'),
                    'date_updated' => date("Y-m-d H:i:s"),
                    'reg_inden' => $_POST['reg_inden']
                ));
                if (!$user->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $user->getMessages()]);
                } else {

                    

                    $confirmation = new Memberconfirmation();
                    $confirmationCode = sha1($user->userid);
                    $confirmation->assign(array(
                        'members_id' => $user->userid,
                        'members_code' => $confirmationCode
                    ));
                    if (!$confirmation->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                        foreach ($confirmation->getMessages() as $message) {
                            echo $message;
                        }
                    } else {
                        $data['success'] = "User profile has been stored.";



                        $MailChimp = new MailChimp('289ce16d32317e58af6e65afe7a6c538-us10');
                        $result = $MailChimp->call('lists/subscribe', array(
                            'id'                => 'fc51891e6f',
                            'email'             => array('email'=>$_POST['email']),
                            'merge_vars'        => array('FNAME'=>$_POST['fname'], 'LNAME'=>$_POST['lname']),
                            'double_optin'      => false,
                            'update_existing'   => true,
                            'replace_interests' => false,
                            'send_welcome'      => false,
                            ));
   
                        $dc = new CB();
                        $json = json_encode(array(
                            'From' => $dc->config->postmark->signature,
                            'To' => $_POST['email'],
                            'Subject' => 'Earth Citizen Organization Confirmation Email',
                            'HtmlBody' => '<h3>Welcome to ECO, '.$_POST['fname'].' '.$_POST['lname'].' </h3> Please click the confirmation link below to activate your account. <br/> <br/> <a href="' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $user->userid . '/' . $confirmationCode . '">' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $user->userid . '/' . $confirmationCode . '</a> <br/><br/> You are the newest earth citizen, and we are very excited that you’re here because it means we are one step further toward mindful living and a sustainable world. By meeting with other like-minded Earth Citizens and joining ECO’s activities, locally and nationally, you will find hope becomes real and change gets easier for you and others. <br/><br/> Please visit our websites to learn more about ECO and our programs. <br/> <a href="www.earthcitizens.org">www.earthcitizens.org </a> <br/> <a href="www.heroesconnect.org">www.heroesconnect.org</a> <br> <br> Thanks, <br><br>ECO Staff'
                            ));

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $dc->config->postmark->url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Accept: application/json',
                            'Content-Type: application/json',
                            'X-Postmark-Server-Token: '.$dc->config->postmark->token
                        ));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                        $response = json_decode(curl_exec($ch), true);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);


                        //////refer
                        if(isset($_POST['refer'])){

                        $json = json_encode(array(
                            'From' => $dc->config->postmark->signature,
                            'To' => $_POST['refer'],
                            'Subject' => 'Earth Citizen Organization Member Invitation',
                            'HtmlBody' => 'Hello friend, <br/><br/> We are pleased to let you know '. $_POST['fname'] .' '. $_POST['lname'] .' wanted you to become a member of the Earth Citizens Organization (ECO). <br><br> ECO was established as a 501(c)(3) nonprofit organization to promote mindful living for a sustainable world. The name of the organization represents the understanding that we all are citizens of the Earth and share the responsibility for its well-being. <br><br> We all know that we need to change to make our lives on this planet sustainable. To make the change feasible, ECO proposes to start making changes from soft simple things such as the way we breathe, the way we eat, the way we manage our stress, and the way we feel toward other people and other living beings. <br><br> These elements, together with training and practices for mindfulness and leadership, are the foundation of the training and education that ECO provides for the people who desire to initiate the change in their lives and their community. Graduates of our programs return to their communities ready to share what they have learned and experienced. The generous donations of individuals and supporting institutions make it possible for ECO to provide this invaluable education below cost. For a $10 minimum donation, you can become a part of the community of Earth Citizens. <br><br> To join ECO and become an Earth Citizen, please visit the link below. <br> <br> <a href="' . $GLOBALS["baseURL"] . '/donation"> '.$GLOBALS["baseURL"].'/donation </a> <br> <br> Thank you for your kind interest, <br> <br> ECO Staff'
                        ));

                        $ch2 = curl_init();
                        curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                        curl_setopt($ch2, CURLOPT_POST, true);
                        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                            'Accept: application/json',
                            'Content-Type: application/json',
                            'X-Postmark-Server-Token: '.$dc->config->postmark->token
                        ));
                        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                        $response = json_decode(curl_exec($ch2), true);
                        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                        curl_close($ch2);
                        }

                        if ($http_code==200) {
                            $data = array('success' => 'success');
                        } else {
                            $data = array('error' => $response);
                        }

                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function memberslistAction($num, $page, $keyword, $sort, $sortto) {


        if ($keyword == 'null' || $keyword == 'undefined') {

            $offsetfinal = ($page * 10) - 10;

            $db = \Phalcon\DI::getDefault()->get('db');

            if($sortto == 'DESC'){
                $conditions = $db->prepare("SELECT * FROM members ORDER BY $sort DESC LIMIT " . $offsetfinal . ",10");
            }else {
                $conditions = $db->prepare("SELECT * FROM members ORDER BY $sort ASC LIMIT " . $offsetfinal . ",10");
            }
            
            $conditions->execute();
            $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

            $db = \Phalcon\DI::getDefault()->get('db');
            $conditions = $db->prepare("SELECT * FROM members");
            $conditions->execute();
            $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        } else {

            if($keyword == 1){

                $offsetfinal = ($page * 10) - 10;
                $keyword = 0; 

                $offsetfinal = ($page * 10) - 10;
                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE status LIKE '%" . $keyword . "%' ORDER BY userid DESC LIMIT " . $offsetfinal . ",10");
                $conditions->execute();
                $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE status LIKE '%" . $keyword . "%' ORDER BY userid DESC");
                $conditions->execute();$conditions->execute();
                $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);        

            }
            else if($keyword == 2){

                $offsetfinal = ($page * 10) - 10;
                $keyword = 1; 

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE status LIKE '%" . $keyword . "%' ORDER BY userid DESC LIMIT " . $offsetfinal . ",10");
                $conditions->execute();
                $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE status LIKE '%" . $keyword . "%' ORDER BY userid DESC");
                $conditions->execute();
                $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);
            }
            else if($keyword == 3){

                $offsetfinal = ($page * 10) - 10;
                $keyword = 2; 

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE status LIKE '%" . $keyword . "%' ORDER BY userid DESC LIMIT " . $offsetfinal . ",10");
                $conditions->execute();
                $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE status LIKE '%" . $keyword . "%' ORDER BY userid DESC");
                $conditions->execute();$conditions->execute();
                $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);
                 // echo json_encode($keyword);   
            }
            else{

                $offsetfinal = ($page * 10) - 10;

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%' OR username LIKE '%" . $keyword . "%' OR birthday LIKE '%" . $keyword . "%' OR location LIKE '%" . $keyword . "%' OR zipcode LIKE '%" . $keyword . "%' OR gender LIKE '%" . $keyword . "%' OR email LIKE '%" . $keyword . "%' OR status LIKE '%" . $keyword . "%' ORDER BY userid DESC LIMIT " . $offsetfinal . ",10");
                $conditions->execute();
                $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%' OR username LIKE '%" . $keyword . "%' OR birthday LIKE '%" . $keyword . "%' OR location LIKE '%" . $keyword . "%' OR zipcode LIKE '%" . $keyword . "%' OR gender LIKE '%" . $keyword . "%' OR email LIKE '%" . $keyword . "%' OR status LIKE '%" . $keyword . "%' ORDER BY userid DESC");
                $conditions->execute();
                $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);
            }

        }

       
        echo json_encode(array(
            'data' => $Pages, 
            'index' => $page, 
            'total_items' => count($count)
            ));
    }

    public function memberslistsearchbydateAction($num, $page, $keyword) {



        if ($keyword == 'null' || $keyword == 'undefined') {
            $offsetfinal = ($page * 10) - 10;

            $db = \Phalcon\DI::getDefault()->get('db');
            $conditions = $db->prepare("SELECT * FROM members ORDER BY userid DESC LIMIT " . $offsetfinal . ",10");
            $conditions->execute();
            $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

            $db = \Phalcon\DI::getDefault()->get('db');
            $conditions = $db->prepare("SELECT * FROM members ORDER BY userid DESC ");
            $conditions->execute();
            $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        } else {

            $offsetfinal = ($page * 10) - 10; 
            
            if($keyword <= 12){

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE birthday LIKE '" . $keyword . "%' ORDER BY userid DESC LIMIT " . $offsetfinal . ",10");
                $conditions->execute();
                $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE birthday LIKE '" . $keyword . "%' ORDER BY userid DESC");
                $conditions->execute();
                $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);    
            }
            else if($keyword >= 1900){

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE birthday LIKE '%" . $keyword . "%' ORDER BY userid DESC LIMIT " . $offsetfinal . ",10");
                $conditions->execute();
                $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE birthday LIKE '%" . $keyword . "%' ORDER BY userid DESC");
                $conditions->execute();
                $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);    
            }
            else{

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE birthday LIKE '" . $keyword . "' ORDER BY userid DESC LIMIT " . $offsetfinal . ",10");
                $conditions->execute();
                $Pages = $conditions->fetchAll(\PDO::FETCH_ASSOC);

                $db = \Phalcon\DI::getDefault()->get('db');
                $conditions = $db->prepare("SELECT * FROM members WHERE birthday LIKE '" . $keyword . "' ORDER BY userid DESC");
                $conditions->execute();
                $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);   
            }




        }

       
        echo json_encode(array(
            'data' => $Pages, 
            'index' => $page, 
            'total_items' => count($count)
            ));
    }

    public function newmemregAction($userid) {

        $pages = Members::findFirst("userid=" . $userid);
        $data = array();
        if ($pages) {
            $data = array(
                'userid' => $pages->userid,
                'email' => $pages->email,
                'password' => $pages->password
            );
        }
        echo json_encode($data);
    }

    public function checkuserAction() {
        if (isset( $_POST['email'] ) && isset($_POST['password']) ) {
            // var_dump($_POST);
            $username = $_POST['email'];
            $password = $_POST['password'];
            //$hashpass = $this->security->hash($password);
            $user = Members::findFirst("email='$username'");
            if ($user) {
                $shapass = sha1($password);
                if ($shapass == $user->password) {
                    if ($user->status == 0) {
                        $data['error'] = 'You have not yet confirmed your email address. Please check your email';
                        echo json_encode($data);
                        return;
                    } else {

                        $data['success'] = $user->firstname . ' ' . $user->lastname;

                        echo json_encode($data);
                        return;
                    }
                }
            }
        }
        $data['error'] = 'Please enter correct email and password.';
        echo json_encode($data);
        return;
    }

    public function paypalipn() {
        
    }

    public function memberscountAction() {
        $memberscount = Members::find();
        echo json_encode(array(
            'total_items' => count($memberscount)
            ));
    }

    public function userdonationAction() {
        //Executing a simple query
        // How many different areas are assigned to employees?
        $don = Donation::findFirst("id=1");
        $usercount = Donationlog::count(array("distinct" => "useremail"));       
        $members = Donationlog::sum(array("column" => "amount"));
        $heroes = Callheroesonduty::Find();
        //echo json_encode(array("user" => $don->usercount, "donations" => $don->amount));
        echo json_encode(array("user" => $usercount + $don->usercount, "donations" => number_format($members + $don->amount, 2, '.', ''),"heroes"=>count($heroes)));
    }
    public function userdonationNepalAction() {
        //Executing a simple query
        // How many different areas are assigned to employees?
        $don = Donation::findFirst("id=1");
        $usercount = Donationlogothers::count(array("distinct" => "useremail"));       
        $members = Donationlogothers::sum(array("column" => "amount"));
        //echo json_encode(array("user" => $don->usercount, "donations" => $don->amount));
        echo json_encode(array("user" => $usercount, "donations" => number_format($members, 2, '.', '')));
    }
    public function userdonationNYAction() {
        //Executing a simple query
        // How many different areas are assigned to employees?
        $don = Donation::findFirst("id=1");
        $usercount = Donationlogothers1::count(array("distinct" => "useremail"));       
        $members = Donationlogothers1::sum(array("column" => "amount"));
        //echo json_encode(array("user" => $don->usercount, "donations" => $don->amount));
        echo json_encode(array("user" => $usercount, "donations" => number_format($members, 2, '.', '')));
    }
    public function userdonationHoustonAction() {
        //Executing a simple query
        // How many different areas are assigned to employees?
       
        $usercount = Donationloghouston::count(array("distinct" => "useremail"));       
        $members = Donationloghouston::sum(array("column" => "amount"));
        //echo json_encode(array("user" => $don->usercount, "donations" => $don->amount));
        echo json_encode(array("user" => $usercount, "donations" => number_format($members, 2, '.', '')));
    }
    public function userdonationSeattleAction() {
        //Executing a simple query
        // How many different areas are assigned to employees?
       
        $usercount = Donationlogseattle::count(array("distinct" => "useremail"));       
        $members = Donationlogseattle::sum(array("column" => "amount"));
        //echo json_encode(array("user" => $don->usercount, "donations" => $don->amount));
        echo json_encode(array("user" => $usercount, "donations" => number_format($members, 2, '.', '')));
    }

    public function donationAction() {
        // Create an album
        $don = Donation::findFirst("id=1");
        //Save both records
        $don->assign(array(
            'amount' => $_POST['amount'],
            'usercount' => $_POST['users']
        ));
        if ($don->save()) {
            echo json_encode(array('amount' => $don->amount, 'users' => $don->amount));
        } else {
            echo json_encode(array('error' => "MAY ERROR"));
        }
    }

    public function sendmailAction() {
        $type = $_POST['type'];
        $email = $_POST['email'];
        if ($type == "createPassword") {
            $don = Members::findFirst("email='" . $email . "'");
            if (!$don) {
                echo json_encode(array('error' => "Email not found."));
                return;
            }
            
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
            $pass = substr( str_shuffle( $chars ), 0, 8 );
            $password = sha1($pass);
            $don->password = $password;
            //Save both records
            if ($don->save()) {
                $data['success'] = "A temporary generated password was sent in your email!";

                $dc = new CB();
                $content = $dc->resetPassword($pass,$don->firstname,$don->lastname);
                $json = json_encode(array(
                    'From' => $dc->config->postmark->signature,
                    'To' => $_POST['email'],
                    'Subject' => 'Earth Citizen Organization Password Changed.',
                    'HtmlBody' => $content
                    ));

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                    ));
                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                $response = json_decode(curl_exec($ch2), true);
                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                curl_close($ch2);

                if ($http_code!=200) {
                    $data = array('error' => $mail->ErrorInfo);
                } else {
                    $data = array('success' => 'success');
                }
            } else {
                $data = array('error' => $mail->ErrorInfo);
            }
            echo json_encode($data);
        } else if ($type == "resendActivation") {
            $don = Members::findFirst("email='" . $email . "'");
            if (!$don) {
                echo json_encode(array('error' => "Email not found."));
                return;
            }
            if ($don->status == 0) {

                $mem = Memberconfirmation::findFirst("members_id='" . $don->userid . "'");
                $confirmationCode = sha1($don->userid);
                $mem->members_code = $confirmationCode;
                //Save both records
                if ($mem->save()) {
                    $data['success'] = "A temporary generated password was sent in your email!";
                    $mail = new PHPMailer();

                    $mail->isSMTP();
                    $mail->Host = 'smtp.mandrillapp.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'efrenbautistajr@gmail.com';
                    $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                    //$mail->SMTPSecure = 'ssl';                    
                    $mail->Port = 587;

                    $mail->From = 'no-reply@eco.com';
                    $mail->FromName = 'Earth Citizen Organization';
                    $mail->addAddress($email, $don->firstname . ' ' . $don->lastname);

                    $mail->isHTML(true);


                    $mail->Subject = 'Earth Citizen Organization Confirmation Email';
                    $mail->Body = '
                            Thank you for registering with the Earth Citizens Organization! 
 <br/><br/>
                            Please click the confirmation link below to activate your account. <br/> <br/> Thanks!
                            <br/><br/>
                            Confirmation Link:
                            <br/>
                            Code: <a href="' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $don->userid . '/' . $confirmationCode . '">' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $don->userid . '/' . $confirmationCode . '   </a>

                        ';

                    if (!$mail->send()) {
                        $data = array('error' => $mail->ErrorInfo);
                    } else {
                        $data = array('success' => 'success');
                    }
                } else {
                    $data = array('error' => $mail->ErrorInfo);
                }
                echo json_encode($data);
            } else {
                $data = array('error' => 'You are already activated or you have not yet registered.');
                echo json_encode($data);
            }
        } else {
            var_dump($_POST);
        }
    }

    public function membersinfoAction($memberid) {
        $conditions = "userid=" . $memberid;
        $members = Members::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($members) {
            $bd = explode('-', $members->birthday);
            $data = array(
                'username' => $members->username,
                'email' => $members->email,
                'firstname' => $members->firstname,
                'lastname' => $members->lastname,
                'bday' => $bd[2],
                'bmonth' => $bd[1],
                'byear' => $bd[0],
                'gender' => $members->gender,
                'location' => $members->location,
                'zipcode' => $members->zipcode,
                'centername' => $members->centername,
                'howdidyoulearn' => $members->howdidyoulearn,
                'status' => $members->status,
                'refer' => $members->refer,
                'reg_inden' => $members->reg_inden
            );
        }
        echo json_encode($data);
    }

    public function memberupdateAction($memberid) {
        $conditions = "userid=" . $memberid;
        $members = Members::findFirst(array($conditions));        

        if ($members) {
            $checkusername = Members::findFirst(array("username = '" . $_POST['username'] . "' AND userid != '" . $members->userid . "'"));
            $checkemail = Members::findFirst(array("email = '" . $_POST['email'] . "' AND userid != '" . $members->userid . "'"));
            if ($checkusername) {
                $data = array('usernametaken' => 'Username already taken.');
            } else if ($checkemail) {
                $data = array('emailtaken' => 'Email address already taken.');
            } else {
                $members->username = $_POST['username'];               
                $members->birthday = $_POST['byear'] . '-' . $_POST['bmonth'] . '-' . $_POST['bday'];
                $members->gender = $_POST['gender'];
                $members->firstname = $_POST['firstname'];
                $members->lastname = $_POST['lastname'];
                if (!empty($_POST['location'])) {
                    $members->location = $_POST['location'];
                }
                $members->zipcode = $_POST['zipcode'];
                $members->centername = $_POST['centername'];
                $members->howdidyoulearn = $_POST['howdidyoulearn'];
                $members->refer = $_POST['refer'];

                //Save both records
                if ($members->save()) {
                    $data['success'] = "Your profile has been updated";  

                    $bday = $_POST['byear'] . '-' . $_POST['bmonth'] . '-' . $_POST['bday'];

                    $dc = new CB();
                    $content = $dc->memberUpdate($_POST['username'],$_POST['email'],$bday,$_POST['gender'],$_POST['firstname'],$_POST['lastname'],$_POST['location'],$_POST['zipcode'],$_POST['centername'],$_POST['howdidyoulearn'],$_POST['refer']);
                    $json = json_encode(array(
                        'From' => $dc->config->postmark->signature,
                        'To' => $_POST['email'],
                        'Subject' => 'Earth Citizen Organization Admin Updated your Profile.',
                        'HtmlBody' => $content
                        ));

                    $ch2 = curl_init();
                    curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                    curl_setopt($ch2, CURLOPT_POST, true);
                    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                        'Accept: application/json',
                        'Content-Type: application/json',
                        'X-Postmark-Server-Token: '.$dc->config->postmark->token
                        ));
                    curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                    $response = json_decode(curl_exec($ch2), true);
                    $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                    curl_close($ch2);

                    if ($http_code!=200) {
                        $data = array('error' => $mail->ErrorInfo);
                    } else {
                        $data = array('success' => 'success');
                    }                 

           
                } else {
                    $data = array('error' => 'Something went wrong.');
                }




                        //REFER
                // if (isset($_POST['refer'])) {                    

                //     $dc = new CB();
                //     $content = $dc->sendRefer($_POST['firstname'],$_POST['lastname'],$GLOBALS["baseURL"]);
                //     $json = json_encode(array(
                //         'From' => $dc->config->postmark->signature,
                //         'To' => $_POST['email'],
                //         'Subject' => 'Earth Citizen Organization Member Invitaion',
                //         'HtmlBody' => $content
                //         ));

                //     $ch2 = curl_init();
                //     curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                //     curl_setopt($ch2, CURLOPT_POST, true);
                //     curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                //     curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                //         'Accept: application/json',
                //         'Content-Type: application/json',
                //         'X-Postmark-Server-Token: '.$dc->config->postmark->token
                //         ));
                //     curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                //     $response = json_decode(curl_exec($ch2), true);
                //     $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                //     curl_close($ch2);

                //     if ($http_code!=200) {
                //         $data = array('error' => $mail->ErrorInfo);
                //     } else {
                //         $data = array('success' => 'success');
                //     } 
                    
                // }



            }
            echo json_encode($data);
        }
    }

    public function memberupdatepasswordAction($memberid) {
        $conditions = "userid=" . $memberid;
        $members = Members::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($members) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
            $pass = substr( str_shuffle( $chars ), 0, 8 );
            $password = sha1($pass);
            $members->password = $password;
            if ($members->save()) {
                $data = array('success' => 'Password Saved');

                $dc = new CB();
                $content = $dc->resetPassword($pass,$members->firstname,$members->lastname);
                $json = json_encode(array(
                    'From' => $dc->config->postmark->signature,
                    'To' => $members->email,
                    'Subject' => 'Earth Citizen Organization Admin Changed your Password.',
                    'HtmlBody' => $content
                    ));

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                    ));
                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                $response = json_decode(curl_exec($ch2), true);
                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                curl_close($ch2);

                if ($http_code!=200) {
                    $data = array('error' => $mail->ErrorInfo);
                } else {
                    $data = array('success' => 'success');
                }
                
            }
        }
        echo json_encode($data);
    }

    public function memberdeleteAction($memberid) {
        $conditions = "userid=" . $memberid;
        $members = Members::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($members) {
            if ($members->delete()) {
                $data = array('success' => 'Member Deleted');
            }
        }
        echo json_encode($data);
    }

    public function newmemberAction() {

        $conditions = "email='" . $_POST['mememail'] . "'";
        $members = Members::findFirst(array($conditions));

        if ($members) {
            $checkusername = Members::findFirst(array("username = '" . $_POST['username'] . "' AND userid != " . $members->userid));
            if ($checkusername) {
                ($checkusername == true) ? $data['usernametaken'] = "Username already taken." : '';
            } else {
                $checkemail = Members::findFirst(array("email = '" . $_POST['email'] . "' AND userid != " . $members->userid));
                if ($checkemail) {
                    ($checkemail == true) ? $data['emailtaken'] = "Email already taken." : '';
                } else {                    
                    $temppass = sha1($_POST['temppassword']);                   
                    $loc = $_POST['location']['code'].'-'.$_POST['location']['name'];
                    $checkpass = Members::findFirst(array("password = '" . $temppass . "' AND userid = " . $members->userid));

                    if(isset($_POST['membday'])){
                        if($bday = $_POST['membday']){
                           $bday = '0' . $_POST['membday'] . '-'; 
                        }else{
                            $bday = $_POST['membday'] . '-'; 
                        }
                        
                    }else{
                        $bday = "";
                    }
                    if(isset($_POST['membmonth'])){
                        if($bmonth = $_POST['membmonth']){
                           $bmonth = '0' . $_POST['membmonth'] . '-'; 
                        }else{
                            $bmonth = $_POST['membmonth'] . '-'; 
                        }
                        
                    }else{
                        $bmonth = "";
                    }
                    if(isset($_POST['centersname'])){
                        $cname = $_POST['centersname'];
                        $emailcname = "Center Name: ". $_POST['centersname'] ." <br/>";
                    }else{
                        $cname = "";
                        $emailcname = "";
                    }
                    if(isset($_POST['howdidyoulearn'])){
                        $howdidyoulearn = $_POST['howdidyoulearn'];
                        $emailhowdidyoulearn = "How did you learn fron ECO: ". $_POST['howdidyoulearn'] ." <br/>";
                    }else{
                        $howdidyoulearn = "";
                        $emailhowdidyoulearn = "";
                    }
                    if(isset($_POST['refer'])){
                        $refer = $_POST['refer'];
                        $emailrefer= "Refered friend: ". $_POST['refer'] ." <br/>";
                    }else{
                        $refer = "";
                        $emailrefer= "";
                    }
                
                    if ($checkpass) {
                        $members->username = $_POST['username'];
                        $members->password = sha1($_POST['password']);
                        $members->firstname = $_POST['memfname'];
                        $members->lastname = $_POST['memlname'];
                        $members->birthday = $bmonth . '' . $bday . '' . $_POST['membyear'];
                        $members->gender = $_POST['memgender'];
                        $members->location = $loc;
                        $members->zipcode = $_POST['memzip'];
                        $members->howdidyoulearn = $howdidyoulearn;
                        $members->centername = $cname;
                        $members->refer = $refer;
                        $members->status = 1;

                        $mail = new PHPMailer();

                        $mail->isSMTP();
                        $mail->Host = 'smtp.mandrillapp.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = 'efrenbautistajr@gmail.com';
                        $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';                    
                        $mail->Port = 587;

                        $mail->From = 'no-reply@eco.com';
                        $mail->FromName = 'Earth Citizen Organization';
                        $mail->addAddress($members->email, $members->firstname . ' ' . $members->lastname);

                        $mail->isHTML(true);


                        $mail->Subject = 'Earth Citizen Organization complete registration.';
                        $mail->Body = '
                        You have successfully completed your registration at Earth Citizen Organization! Check below:
                        <br/><br/>
                        Username: ' . $members->username . ' <br/>
                        Email: ' . $members->email . ' <br/>
                        Birthday: ' . $members->birthday . ' <br/>
                        Gender: ' . $members->gender . ' <br/>
                        Firstname: ' . $members->firstname . ' <br/>
                        Lastname: ' . $members->lastname . ' <br/>
                        Location: ' . $members->location . ' <br/>
                        Zipcode: ' . $members->zipcode . ' <br/> '.$emailhowdidyoulearn.''.$emailcname.''.$emailrefer.'
                        <br/><br/>
                        Please visit our website! Click here: <a href="' . $GLOBALS["baseURL"] . '">' . $GLOBALS["baseURL"] . ' </a>
                        ';

                        if (!$mail->send()) {
                            $data = array('error' => $mail->ErrorInfo);
                        } else {
                            $data = array('success' => 'success');





                            if (isset($_POST['refer'])) {
                                $data['success'] = "Your profile has been updated";

                                $mail2 = new PHPMailer();


                                $mail2->isSMTP();
                                $mail2->Host = 'smtp.mandrillapp.com';
                                $mail2->SMTPAuth = true;
                                $mail2->Username = 'efrenbautistajr@gmail.com';
                                $mail2->Password = '6Xy18AJ15My59aQNTHE5kA';
                    //$mail->SMTPSecure = 'ssl';                    
                                $mail2->Port = 587;

                                $mail2->From = 'no-reply@eco.com';
                                $mail2->FromName = 'Earth Citizen Organization';
                                $mail2->addAddress($members->refer);

                                $mail2->isHTML(true);
                                $mail2->Subject = 'Earth Citizen Organization Member Invitaion';
                                $mail2->Body = 'Hello friend, <br/><br/> We are pleased to let you know '
                                . $_POST['memfname'] .' '. $_POST['memlname'] .' wanted you to become a member of the Earth Citizens Organization (ECO). 
                                <br><br> ECO was established as a 501(c)(3) nonprofit organization to promote mindful living for a sustainable world. 
                                The name of the organization represents the understanding that we all are citizens of the Earth and share the responsibility 
                                for its well-being. <br><br> We all know that we need to change to make our lives on this planet sustainable. 
                                    To make the change feasible, ECO proposes to start making changes from soft simple things such as the way we breathe, 
                                the way we eat, the way we manage our stress, and the way we feel toward other people and other living beings. 
                                <br><br> These elements, together with training and practices for mindfulness and leadership, are the foundation of 
                                the training and education that ECO provides for the people who desire to initiate the change in their lives and their 
                                community. Graduates of our programs return to their communities ready to share what they have learned and experienced. 
                                The generous donations of individuals and supporting institutions make it possible for ECO to provide this invaluable 
                                education below cost. For a $10 minimum donation, you can become a part of the community of Earth Citizens. 
                                <br><br> To join ECO and become an Earth Citizen, please visit the link below. <br> <br> 
                                <a href="' . $GLOBALS["baseURL"] . '/donation"> '.$GLOBALS["baseURL"].'/donation </a> <br> <br> 
                                Thank you for your kind interest, <br> <br> ECO Staff';

                                if (!$mail2->send()) {
                                    $data = array('error' => $mail2->ErrorInfo);
                                } else {
                                    $data = array('success' => 'success');


                                }
                            }

                           
                        }

                        //Save both records
                        if ($members->save()) {
                            $data['success'] = "Your profile has been updated";
                        } else {
                            $data = array('error' => 'Something went wrong.');   
                        }
                    } else {
                        ($checkpass == false) ? $data['temppass'] = "Invalid Password." : '';
                    }
                }
            }
        } else {
            ($members == false) ? $data['emailtaken2'] = "Email Not Found." : '';
        }
        echo json_encode($data);
    }

    public function getinfoAction($memid) {

        $meminfo = Members::findFirst("userid='" . $memid . "'");
        $data = array();
        if ($meminfo) {
            $bdate = @explode("/", $meminfo->birthday);
            $data = array(
                'members_id' => $meminfo->userid,
                'mememail' => $meminfo->email,
                'memfname' => $meminfo->firstname,
                'memlname' => $meminfo->lastname,
                'memgender' => $meminfo->gender,
                'location' => $meminfo->location,
                'memzip' => $meminfo->zipcode,
                'memcname' => $meminfo->centername,
                'memhowdidyoulearn' => $meminfo->howdidyoulearn,
                'memrefer' => $meminfo->refer,
                'membday' => $bdate[1],
                'membmonth' => $bdate[0],
                'membyear' => $bdate[2]
                );
        }
        echo json_encode($data);
    }

    public function sendMandrillEmailAction(){
        $dc = new CB();

        $dc->sendMandrillEmailAction(
            'hehe',
            'efrenbautistajr@gmail.com',
            'Earth Citizen Organization Confirmation Email'
        );
    }


}
