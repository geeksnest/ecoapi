<?php

namespace Controllers;

use \Models\Toppage as Topppage;
use \Models\Toppage0 as Topppage0;
use \Models\Toppage1 as Topppage1;
use \Models\Toppage2 as Topppage2;
use \Controllers\ControllerBase as CB;

class ToppageController extends \Phalcon\Mvc\Controller {
    public function saveAction(){
        $msg = Topppage::find();
        $content= $_POST['content']; 
        $bgcolor= $_POST['bgcolor'];
        if(count($msg)==0){
            $add = new Topppage();
            $add->assign(array(
                'bgcolor' => $bgcolor,
                'content' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Topppage::findFirst(array("id=1"));
            $save->bgcolor = $bgcolor;
            $save->content    = $content;
            $save->dateupdated   = date("Y-m-d H:i:s");

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsgAction() {
       $msg = Topppage::find(array("id=1"));

        if(count($msg)!=0){
            $data=array(
                'bgcolor' => $msg[0]->bgcolor,
                'content'  => $msg[0]->content
                );
        }else{
         $data=array(
            'bgcolor' => '#ffffff',
            'content'  => ""
            );
        }
   echo json_encode($data);
    }

    public function save1Action(){
        $request = new \Phalcon\Http\Request();
        $msg = Topppage1::find();
        $content= $_POST['content'];
        $bgcolor= $_POST['bgcolor'];
        if(count($msg)==0){
            $add = new Topppage1();
            $add->assign(array(
                'bgcolor' => $bgcolor,
                'content' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Topppage1::findFirst(array("id=1"));
            $save->bgcolor = $bgcolor;
            $save->content    = $content;
            $save->dateupdated   = date("Y-m-d H:i:s");

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsg1Action() {
       $msg = Topppage1::find(array("id=1"));

        if(count($msg)!=0){

            $data=array(
                'bgcolor' => $msg[0]->bgcolor,
                'content'  => $msg[0]->content
                );
        }else{
         $data=array(
             'bgcolor' => '#ffffff',
            'content'  => ""
            );
        }
   echo json_encode($data);
    }

    public function save2Action(){
        $request = new \Phalcon\Http\Request();
        $msg = Topppage2::find();
        $content= $_POST['content'];
        $bgcolor= $_POST['bgcolor'];
        if(count($msg)==0){
            $add = new Topppage2();
            $add->assign(array(
                'bgcolor' => $bgcolor,
                'content' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Topppage2::findFirst(array("id=1"));
            $save->bgcolor = $bgcolor;
            $save->content    = $content;
            $save->dateupdated   = date("Y-m-d H:i:s");

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsg2Action() {
       $msg = Topppage2::find(array("id=1"));

        if(count($msg)!=0){

            $data=array(
                'bgcolor' => $msg[0]->bgcolor,
                'content'  => $msg[0]->content
                );
        }else{
         $data=array(
            'bgcolor' => '#ffffff',
            'content'  => ""
            );
        }
        echo json_encode($data);
    }

    public function save0Action(){
        $msg = Topppage0::find();
        if(count($msg)==0){
            $add = new Topppage0();
            $add->assign(array(
                'maintitle' => $_POST['title'],
                'bgcolor' => $_POST['bgcolor'],
                'title0' => $_POST['title0'],
                'desc0' => $_POST['desc0'],
                'image0' => $_POST['image0'],
                'count0' => $_POST['count0'],
                'title1' => $_POST['title1'],
                'desc1' => $_POST['desc1'],
                'image1' => $_POST['image1'],
                'count1' => $_POST['count1'],
                'title2' => $_POST['title2'],
                'desc2' => $_POST['desc2'],
                'image2' => $_POST['image2'],
                'count2' => $_POST['count2'],
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Topppage0::findFirst("id=1");
            $save->bgcolor = $bgcolor;
            $save->maintitle = $_POST['title'];
            $save->bgcolor = $_POST['bgcolor'];
            $save->title0 = $_POST['title0'];
            $save->desc0 = $_POST['desc0'];
            if(isset($_POST['image0'])){
                $save->image0 = $_POST['image0'];
            }
            $save->count0 = $_POST['count0'];
            $save->title1 = $_POST['title1'];
            $save->desc1 = $_POST['desc1'];

            if(isset($_POST['image1'])){
                $save->image1 = $_POST['image1'];
            }

            $save->count1 = $_POST['count1'];
            $save->title2 = $_POST['title2'];
            $save->desc2 = $_POST['desc2'];

            if(isset($_POST['image2'])){
                $save->image2 = $_POST['image2'];
            }

            $save->count2 = $_POST['count2'];
            $save->save();

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsg0Action() {
       $msg = Topppage0::find(array("id=1"));

        if(count($msg)!=0){

            $data=array(
                'bgcolor' => $msg[0]->bgcolor,
                'title'  => $msg[0]->maintitle,
                'title0' => $msg[0]->title0,
                'title1' => $msg[0]->title1,
                'title2' => $msg[0]->title2,
                'desc0' => $msg[0]->desc0,
                'desc1' => $msg[0]->desc1,
                'desc2' => $msg[0]->desc2,
                'image0' => $msg[0]->image0,
                'image1' => $msg[0]->image1,
                'image2' => $msg[0]->image2,
                'count0' => $msg[0]->count0,
                'count1' => $msg[0]->count1,
                'count2' => $msg[0]->count2
                );
        }else{
         $data=array(
            'bgcolor' => '#ffffff',
            );
        }
        echo json_encode($data);
    }      
}
