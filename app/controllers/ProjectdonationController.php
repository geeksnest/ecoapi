<?php

namespace Controllers;

use \Models\News as News;
use \Models\Projects as Projects;
use \Models\Projectauthor as Projectauthor;
use \Models\Projectsocialmedia as Projectsocialmedia;
use \Models\Projectsdonations as Projectsdonations;
use \Models\Notifications as Notifications;
use \Models\Members as Members;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class ProjectdonationController extends \Phalcon\Mvc\Controller
{

	public function creditAction()
	{
		// var_dump($_POST);
		$dc = new CB();

		$post_url = $dc->config->authorized->post_url;
		$loginname=$dc->config->authorized->apilogin;
		$transactionkey=$dc->config->authorized->transactionkey;
		$host = $dc->config->authorized->apiurlHost;
		$path = $dc->config->authorized->apiurlVer;


		$donatorfName = $_POST['billingfname'];
		$donatorlName = $_POST['billinglname'];
		$zipcode = $_POST['zipcode'];
		$location = $_POST['al1'];

		$creditTransactId = uniqid();

		$post_values = array(

        // the API Login ID and Transaction Key must be replaced with valid values
			"x_login"           => $loginname,
			"x_tran_key"        => $transactionkey,
			"x_first_name" => $donatorfName,
			"x_last_name" => $donatorlName,

			"x_version"         => "3.1",
			"x_delim_data"      => "TRUE",
			"x_delim_char"      => "|",
			"x_relay_response"  => "FALSE",
			"x_invoice_num"     => $creditTransactId,
			"x_type"            => "AUTH_CAPTURE",
			"x_method"          => "Credit Card",
			"x_card_num"        => $_POST['ccn'],
			"x_exp_date"        => sprintf("%02d", $_POST['expiremonth']).substr( $_POST['expireyear'], -2 ),

			"x_amount"          => $_POST['amount'],
			"x_description"     => $_POST['projTitle'],

        // Additional fields can be added here as outlined in the AIM integration
        // guide at: http://developer.authorize.net
			);

		$post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
		$post_values["x_city"] = $_POST['city'];
		$post_values["x_state"] = $_POST['state'];
		$post_values["x_zip"] = $_POST['zip'];
		$post_values["x_country"] = $_POST['country'];

        // This section takes the input fields and converts them to the proper format
        // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"

		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );

        // The following section provides an example of how to add line item details to
        // the post string.  Because line items may consist of multiple values with the
        // same key/name, they cannot be simply added into the above array.
        //
        // This section is commented out by default.

		$line_items = array(
			"item1<|>golf balls<|><|>2<|>18.95<|>Y",
			"item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
			"item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

		foreach( $line_items as $value )
			{ $post_string .= "&x_line_item=" . urlencode( $value ); }


        // This sample code uses the CURL library for php to establish a connection,
        // submit the post, and record the response.
        // If you receive an error, you may want to ensure that you have the curl
        // library enabled in your php configuration
        $request = curl_init($post_url); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
        $post_response = curl_exec($request); // execute curl post and store results in $post_response
        // additional options may be required depending upon your server configuration
        // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close ($request); // close curl object

        // This line takes the response and breaks it into an array using the specified delimiting character
        $response_array = explode($post_values["x_delim_char"],$post_response);

        // The results are output to the screen in the form of an html numbered list.
        if($response_array[0]==1){

        	$donate = new Projectsdonations();

        	$donate->assign(array(
        		'useremail' => $_POST['email'],
        		'transactionId' => $creditTransactId,
        		'datetimestamp' => date("Y-m-d H:i:s"),
        		'amount' => $_POST['amount'],
        		'paymentmode' => 'CreditCard',
        		'billinginfofname' => $donatorfName,
        		'billinginfolname' => $donatorlName,
        		'donatedto' => $_POST['projID'],
        		'lastccba' => substr($_POST['ccn'], -4),
        		'howdidyoulearn' => $_POST['howdidyoulearn'],
        		'cname' => $_POST['cname']
        		));
        	if($donate->save()){
        		$success['donsuccess'] = "Success.";

        		$proj = Projects::findFirst("projID='" . $_POST['projID'] ."'");        		
        		$mem = Members::findFirst('userid="'.$proj->projAuthorID.'"');

                $guid = new \Utilities\Guid\Guid();
                $noteID = $guid->GUID();
                $note = new Notifications();
                $note->assign(array(
                    'noteID' => $noteID,
					'userID' => $proj->projAuthorID,
					'readStatus' => 0,
					'content' => $proj->projTitle,
					'noteType' => 5,
					'itemID' =>  $_POST['projID'],
					'timeStarted' => time().'000',
					'date_created' => date("Y-m-d H:i:s")
					));

				if(!$note->save()) {
					$errors = array();
					foreach ($note->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					die(json_encode(array('error' => $errors)));
				}else{
					$success['notesuccess'] = "Notification saved.";
				}

        		$pay_type = 'CreditCard';
                $name = $donatorfName." ".$donatorlName;

        		$dc = new CB();
        		$content = $dc->projectReceiptTemplate($creditTransactId,$name,$_POST['amount'],$pay_type,$proj->projTitle);
        		$json = json_encode(array(
        			'From' => $dc->config->postmark->signature,
        			'To' => $_POST['email'],
        			'Subject' => 'ECO Projects Donation',
        			'HtmlBody' => $content
        			));

        		$ch2 = curl_init();
        		curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
        		curl_setopt($ch2, CURLOPT_POST, true);
        		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        		curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
        			'Accept: application/json',
        			'Content-Type: application/json',
        			'X-Postmark-Server-Token: '.$dc->config->postmark->token
        			));
        		curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        		$response = json_decode(curl_exec($ch2), true);
        		$http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        		curl_close($ch2);

        		if ($http_code!=200) {
        			$data = array('error' => $mail->ErrorInfo);
        		} else {
        			$data = array('success' => 'success');
        		}
        	}
        	echo json_encode(array('success'=> $success, 'code' => $response_array));

        }else{
        	echo json_encode(array(
        		'error'=>'Something went wrong in processing your transaction. Please check you Credit Card or verify your provider.',
        		'code' => $response_array
        		));
        }




    }

    public function echeckAction()
    {

		// var_dump($_POST);
        $dc = new CB();

    	$post_url = $dc->config->authorized->post_url;
    	$loginname=$dc->config->authorized->apilogin;
    	$transactionkey=$dc->config->authorized->transactionkey;
    	$host = $dc->config->authorized->apiurlHost;
    	$path = $dc->config->authorized->apiurlVer;

    	$donatorfName = $_POST['billingfname'];
    	$donatorlName = $_POST['billinglname'];
    	$zipcode = $_POST['zipcode'];
    	$location = $_POST['al1'];

    	$creditTransactId = uniqid();
    	$post_values = array(

        // the API Login ID and Transaction Key must be replaced with valid values
    		"x_login"           => $loginname,
    		"x_tran_key"        => $transactionkey,
    		"x_first_name" => $donatorfName,
    		"x_last_name" => $donatorlName,

    		"x_version"         => "3.1",
    		"x_delim_data"      => "TRUE",
    		"x_delim_char"      => "|",
    		"x_relay_response"  => "FALSE",
    		"x_invoice_num"     => $creditTransactId,

    		"x_method"          => "ECHECK",
    		"x_bank_aba_code"   => $_POST['bankrouting'],
    		"x_bank_acct_num"   => $_POST['bankaccountnumber'],
    		"x_bank_acct_type"  => $_POST['at'],
    		"x_bank_name"       => $_POST['bankname'],
    		"x_bank_acct_name"  => $_POST['accountname'],


    		"x_amount"          => $_POST['amount'],
            "x_description"     => $_POST['projTitle'],

        // Additional fields can be added here as outlined in the AIM integration
        // guide at: http://developer.authorize.net
    		);

		if($_POST['at'] == 'BUSINESSCHECKING'){
			$post_values["x_echeck_type"] = 'CCD';
		}else{
			$post_values["x_echeck_type"] = 'WEB';
		}

		$post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
		$post_values["x_city"] = $_POST['city'];
		$post_values["x_state"] = $_POST['state'];
		$post_values["x_zip"] = $_POST['zip'];
		$post_values["x_country"] = $_POST['country']['name'];

        // This section takes the input fields and converts them to the proper format
        // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );

        // The following section provides an example of how to add line item details to
        // the post string.  Because line items may consist of multiple values with the
        // same key/name, they cannot be simply added into the above array.
        //
        // This section is commented out by default.
        /*
        $line_items = array(
            "item1<|>golf balls<|><|>2<|>18.95<|>Y",
            "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
            "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

        foreach( $line_items as $value )
            { $post_string .= "&x_line_item=" . urlencode( $value ); }
        */

        // This sample code uses the CURL library for php to establish a connection,
        // submit the post, and record the response.
        // If you receive an error, you may want to ensure that you have the curl
        // library enabled in your php configuration
        $request = curl_init($post_url); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
        $post_response = curl_exec($request); // execute curl post and store results in $post_response
        // additional options may be required depending upon your server configuration
        // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close ($request); // close curl object

        // This line takes the response and breaks it into an array using the specified delimiting character
        $response_array = explode($post_values["x_delim_char"],$post_response);

        // The results are output to the screen in the form of an html numbered list.
        if($response_array[0]==1){

        	$donate = new Projectsdonations();

        	$donate->assign(array(
        		'useremail' => $_POST['email'],
        		'transactionId' => $creditTransactId,
        		'datetimestamp' => date("Y-m-d H:i:s"),
        		'amount' => $_POST['amount'],
        		'paymentmode' => 'e-Check',
        		'billinginfofname' => $donatorfName,
        		'billinginfolname' => $donatorlName,
        		'donatedto' => $_POST['projID'],
        		'lastccba' => substr($_POST['ccn'], -4),
        		'howdidyoulearn' => $_POST['howdidyoulearn'],
        		'cname' => $_POST['cname']
        		));
        	if($donate->save()){
        		$success['donsuccess'] = "Success.";

        		$proj = Projects::findFirst("projID='" . $_POST['projID'] ."'");        		
        		$mem = Members::findFirst('userid="'.$proj->projAuthorID.'"');

                $guid = new \Utilities\Guid\Guid();
                $noteID = $guid->GUID();
                $note = new Notifications();
                $note->assign(array(
                    'noteID' => $noteID,
					'userID' => $proj->projAuthorID,
					'readStatus' => 0,
					'content' => $proj->projTitle,
					'noteType' => 5,
					'itemID' =>  $_POST['projID'],
					'timeStarted' => time().'000',
					'date_created' => date("Y-m-d H:i:s")
					));

				if (!$note->save()) {
					$errors = array();
					foreach ($note->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					die(json_encode(array('error' => $errors)));
				} else {
					$success['notesuccess'] = "Notification saved.";
				}

        		$pay_type = 'e-Check';
        		$name = $donatorfName." ".$donatorlName;

        		$dc = new CB();
        		$content = $dc->projectReceiptTemplate($creditTransactId,$name,$_POST['amount'],$pay_type,$proj->projTitle);
        		$json = json_encode(array(
        			'From' => $dc->config->postmark->signature,
        			'To' => $_POST['email'],
        			'Subject' => 'ECO Projects Donation',
        			'HtmlBody' => $content
        			));

        		$ch2 = curl_init();
        		curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
        		curl_setopt($ch2, CURLOPT_POST, true);
        		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        		curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
        			'Accept: application/json',
        			'Content-Type: application/json',
        			'X-Postmark-Server-Token: '.$dc->config->postmark->token
        			));
        		curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        		$response = json_decode(curl_exec($ch2), true);
        		$http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        		curl_close($ch2);

        		if ($http_code!=200) {
        			$data = array('error' => $mail->ErrorInfo);
        		} else {
        			$data = array('success' => 'success');
        		}
        	}
        	echo json_encode(array('success'=> $success, 'code' => $response_array));
        }else{
        	echo json_encode(array(
        		'error'=>'Something went wrong in processing your transaction. Please refresh the page and try again.',
        		'code' => $response_array
        		));
        }



    }	

}

