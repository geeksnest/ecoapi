<?php
namespace Controllers;
use \Models\Testimonial as Testimonial;
class TestimonialsController extends \Phalcon\Mvc\Controller
{
    public function slideruploadAction(){
        var_dump($_POST);  
    }
    
    public function testimonialAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $testimonial = Testimonial::find();
        } else {
            $conditions = "name LIKE '%" . $keyword . "%'";
            $testimonial= Testimonial::find(array($conditions));
        }
        $currentPage = (int) ($page);
        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $testimonial,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();
        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id ,
                'name' => $m->name ,
                'email' => $m->email,
                'comporg' => $m->comporg,
                'message' => $m->message,
                'status' => $m->status,
                'publish' => $m ->publish
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function dltAction($id){
        $dltPhoto = Testimonial::findFirst('id='.$id.'');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Testimonial has Been deleted');
            }
        }
        echo json_encode($data);
    }

    public function infoviewAction($id) {
        $testimonial = Testimonial::findFirst("id='" .$id."'");
        $info = array();
        if ($testimonial) {
            $info = array(
                'id' => $testimonial ->id,
                'name' => $testimonial ->name,
                'email' => $testimonial ->email,
                'comporg' => $testimonial ->comporg,
                'status' => $testimonial ->status,
                'message'=> $testimonial->message,
                'publish' => $testimonial ->publish
            );
        }
        echo json_encode($info);
    }

    public function updateAction($id,$message) {
         $testimonial = Testimonial::findFirst('id='.$id.'');
         $testimonial->message = $message;
         if(!$testimonial->save()){
           $data="error";
        }else{
            $data="Success";
        }
         echo json_encode($data);
      
    }

    public function updatestatusAction($id, $status) {
        $testimonial = Testimonial::findFirst('id='.$id.'');

        if($status=="publish"){
            $stat="unpublish";
        }elseif($status=="unpublish") {
            $stat="publish";
        }
        $testimonial->publish = $stat;
        if(!$testimonial->save()){
           $data="error";
        }else{
            $data="Success";
        }
         echo json_encode($data);
    }

    /* FRONT-END DISPLAY DATA */
    public function showtestimonialAction($offset) {
        $testimonial = Testimonial::find(array("publish='publish'"," ORDER"=>"id DESC", "limit" => array("number" => 5, "offset" => $offset)));
        $msginfo = json_encode($testimonial->toArray(), JSON_NUMERIC_CHECK);
        echo $msginfo;
    }

    /* FRONT-END SAVE DATA */
    public function submitAction() {
        $data = array();
        $submitdatas = Testimonial::findFirst("email='" . $_POST['email'] . "' AND publish='unpublish'");
        if($submitdatas == true){
            ($submitdatas == true) ? $data["manen"] = "Ok." : '';
            $data = 1;
        }else{
            ($submitdatas == false) ? $data["testi"] = "Ok." : '';
            $submitdata = new Testimonial();
            $submitdata->assign(array(
                'name'  => $_POST['name'],
                'email' => $_POST['email'],
                'comporg' => $_POST['comporg'],
                'message' => $_POST['message'],
                'status'=>"stat",
                'publish'=>"unpublish"
                    ));
            if (!$submitdata->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $submitdata->getMessages()]);
            }else{                
                $data = "Successfuly Save";
            }
                echo json_encode($data);
        }
    }

}

