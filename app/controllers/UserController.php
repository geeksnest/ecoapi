<?php

namespace Controllers;

use Models\Userroles as Userroles;
use Models\Roles as Roles;
use Models\Users as Users;
use Models\Profimg as Profimg;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Controllers\ControllerBase as CB;

class UserController extends \Phalcon\Mvc\Controller {

    public function indexAction($id) {
        $dc = new CB();
        echo json_encode($data);
    }

    // ADD USER
    public function createSaveAction() {


        $data = array();

        if (isset($_POST['username'])) {

            $roles = array();
            $roles['calendarrole'] = $calendarrole = isset($_POST['calendarrole']) ? $_POST['calendarrole']:'';
            $roles['peacemarksrole'] = $peacemarksrole = isset($_POST['peacemarksrole']) ? $_POST['peacemarksrole'] : '';
            $roles['proposalesrole'] = $proposalesrole = isset($_POST['proposalesrole']) ? $_POST['proposalesrole'] : '';
            $roles['testimonialsrole'] = $testimonialsrole = isset($_POST['testimonialsrole']) ? $_POST['testimonialsrole'] : '';
            $roles['newslettersrole'] = $newslettersrole = isset($_POST['newslettersrole']) ? $_POST['newslettersrole'] : '';
            $roles['subscriberrole'] = $subscriberrole = isset($_POST['subscriberrole']) ? $_POST['subscriberrole'] : '';

            
            $roles['donationsrole'] = $donationsrole = isset($_POST['donationsrole']) ? $_POST['donationsrole'] : '';
            $roles['imagesrole'] = $imagesrole = isset($_POST['imagesrole']) ? $_POST['imagesrole'] : '';
            $roles['newsrole'] = $newsrole = isset($_POST['newsrole']) ? $_POST['newsrole'] : '';
            $roles['pagesrole'] = $pagesrole = isset($_POST['pagesrole']) ? $_POST['pagesrole'] : '';
            $roles['usersrole'] = $usersrole = isset($_POST['usersrole']) ? $_POST['usersrole'] : '';
            $roles['vendorrole'] = $vendorrole = isset($_POST['vendorrole']) ? $_POST['vendorrole'] : '';
            $roles['eventmanagerrole'] = $eventmanagerrole = isset($_POST['eventmanagerrole']) ? $_POST['eventmanagerrole'] : '';
            $roles['settingsrole'] = $settingsrole = isset($_POST['settingsrole']) ? $_POST['settingsrole'] : '';
            $roles['programrole'] = $programrole = isset($_POST['programrole']) ? $_POST['programrole'] : '';
            $roles['projectsrole'] = $projectsrole = isset($_POST['projectsrole']) ? $_POST['projectsrole'] : '';
            $roles['callheroesrole'] = $projectsrole = isset($_POST['callheroesrole']) ? $_POST['callheroesrole'] : '';
            $roles['clubrole'] = $projectsrole = isset($_POST['clubrole']) ? $_POST['clubrole'] : '';
            $roles['homepagesrole'] = $projectsrole = isset($_POST['homepagesrole']) ? $_POST['homepagesrole'] : '';
            $roles['medialibraryrole'] = $projectsrole = isset($_POST['medialibraryrole']) ? $_POST['medialibraryrole'] : '';
            $roles['menucreaterole'] = $projectsrole = isset($_POST['menucreaterole']) ? $_POST['menucreaterole'] : '';
            $roles['organizationsrole'] = $projectsrole = isset($_POST['organizationsrole']) ? $_POST['organizationsrole'] : '';
            if (empty($calendarrole) && empty($donationsrole) && empty($imagesrole) && empty($newslettersrole)  && empty($subscriberrole) && empty($newsrole) && empty($pagesrole) && empty($peacemarksrole) && empty($projectsrole) && empty($proposalesrole) && empty($testimonialsrole) && empty($usersrole) && empty($vendorrole) && empty($eventmanagerrole) && empty($settingsrole) && empty($programrole) && empty($projectsrole)) {
                $data['error'] = "<div class='label label-danger'>At least one role should be selected.</div>";
            } else {
                //if ($form->isValid($_POST) != false) {

                $bday = isset($_POST['birthday']) ? $_POST['birthday'] : "";


                $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                $dates = explode(" ", $_POST['birthday']);
                $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];


                $guid = new \Utilities\Guid\Guid();
                $userid = $guid->GUID();

                $user = new Users();
                $password = sha1($_POST['password']);
                $user->assign(array(
                    'userid' => $userid,
                    'username' => $_POST['username'],
                    'email' => $_POST['email'],
                    'password' => $password,
                    'hdyha' => $_POST['firstname'],
                    'referal' => 'superagent',
                    'firstname' => $_POST['firstname'],
                    'lastname' => $_POST['lastname'],
                    'birthday' => $d,
                    'gender' => $_POST['gender'],
                    'state' => $_POST['state'],
                    'country' => 'country',
                    'agentType' => 'Administrator',
                    'userLevel' => '2',
                    'status' => 1,
                    'img' => $_POST['image'],
                    'date_created' => date('Y-m-d'),
                    'date_updated' => date("Y-m-d H:i:s")
                    ));
                if (!$user->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "User profile has been stored.";

                    foreach ($roles as $r => $v) {
                        if ($v == "true") {
                            if($r == 'donationsrole'){
                                $userroles = new Userroles();
                                $userroles->assign(array(
                                    'userID' => $userid,
                                    'userRoles' => $r,
                                    'roleAccess' => $_POST['donationAccess'],
                                    'roleGroup' => 'donation'
                                    ));
                            }else{
                                $roles = Roles::findFirst('roleCode="'.$r.'"');
                                $userroles = new Userroles();
                                $userroles->assign(array(
                                    'userID' => $userid,
                                    'userRoles' => $r,
                                    'roleAccess' => $roles->rolePage,
                                    'roleGroup' => $roles->roleGroup
                                    ));

                            }
                            if (!$userroles->save()) {
                                $data['error'] = "Something went wrong saving the user roles, please try again.";
                            }
                        }
                    }
                }
            }

        }
        echo json_encode($data);
    }

    // MANAGE USERS
    public function manageusersAction($num, $page, $keyword, $sort, $sortto) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {            

            $conditions = "SELECT * FROM users WHERE userLevel !=1 ";            

        } else {

            $conditions = "SELECT * FROM users WHERE status LIKE '%" . $keyword . "%' OR firstname LIKE '%" . $keyword . "%'";
            $conditions .= " OR lastname LIKE '%" . $keyword . "%' OR email LIKE '%" . $keyword . "%' OR username LIKE '%" . $keyword . "%' ";

        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult, 
            'index' => $page, 
            'total_items' => count($count)
            ));

    }

    // DELETE USER
    public function deleteuserAction($userid) {
        $user = Users::findFirst(array("userid='".$userid."'"));
        $data = array('error' => 'Not Found');
        if ($user) {
            if($user->delete()){
                $data = array('success' => 'User Deleted');

                $userroles = Userroles::find(array("userID='".$userid."'"));
                if($userroles->delete()){
                    $data = array('success' => 'User Roles Deleted');
                }
            }
        }
        echo json_encode($data);
    }

    // EDIT USER INFO
    public function edituserAction($userid) {

        $user = Users::findFirst("userid='" . $userid . "'");
        $data = array();
        if ($user) {

            $access = Userroles::findFirst("userID='" . $userid . "' AND roleGroup = 'donation'");


            $data = array(
                'userid' => $user->userid,
                'username' => $user->username,
                'email' => $user->email,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'birthday' => $user->birthday,
                'birthday2' => $user->birthday,
                'bdaytemp' => $user->birthday,
                'gender' => $user->gender,
                'state' => $user->state,
                'status' => $user->status,
                'oldpwordtemp' => $user->password,
                'userLevel' => $user->userLevel,
                'imgg' => $user->img
                );

            $dc = new CB();
            $phql =   " Select * FROM Models\Userroles WHERE userID = '".$user->userid."'";
            $userroles = $dc->modelsManager->executeQuery($phql);


            $roles = array();
            foreach ($userroles as $ur) {
                $roles[] = array(
                    'roleCode' => $ur->userRoles
                );
            }
            $data['userroles'] = $roles;


            $ac['roleAccess'] = explode(',', str_replace(' ', '', $access->roleAccess));
            // $acce = array();
            foreach ($ac['roleAccess'] as $acc) {

                $data[$acc] = $acc;
            }



        }
        echo json_encode($data);
    }

  

    // UPDATE USERNAME
    public function updateusernameAction(){
        $data = array();
        if ($_POST){

            $userid = $_POST['userid'];
            $user = Users::findFirst('userid="'.$userid.'"');
            $user->username = $_POST['username'];


            if (!$user->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }
        echo json_encode($data);
    }

    // UPDATE EMAIL ADDRESS
    public function updateuseremailAction(){
        $data = array();
        if ($_POST){

            $userid = $_POST['userid'];
            $user = Users::findFirst('userid="'.$userid.'"');
            $user->email = $_POST['email'];


            if (!$user->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }
        echo json_encode($data);
    }

    // CHANGE PASSWORD
    public function changePasswordAction(){
        $data = array();
        if ($_POST){

            $password = sha1($_POST['password']);
            $userid = $_POST['userid'];
            $user = Users::findFirst('userid="'.$userid.'"');
            $user->password = $password;


            if (!$user->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }
        echo json_encode($data);
    }

    // UPDATE INFO
    public function updateuserInfoAction(){
        $data = array();
        if ($_POST){

            if($_POST['birthday2'] == 'change'){
                $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                $dates = explode(" ", $_POST['birthday']);
                $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
            }else{
                $d = $_POST['birthday'];
            }


            $userid = $_POST['userid'];
            $user = Users::findFirst('userid="'.$userid.'"');
            
            $user->firstname = $_POST['firstname'];
            $user->lastname = $_POST['lastname'];
            $user->birthday = $d;
            $user->gender = $_POST['gender'];
            $user->state = $_POST['state'];


            if (!$user->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }
        echo json_encode($data);
    }

    // UPDATE PHOTO
    public function updateProfilePhotoAction(){
        $data = array();
        if ($_POST){

            $userid = $_POST['userid'];
            $user = Users::findFirst('userid="'.$userid.'"');
            $user->img = $_POST['image'];

            if (!$user->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }
        echo json_encode($data);
    }

    // UPDATE RESTRICTIONS
    public function editRestrictionsAction(){
        $data = array();
        if ($_POST){

            $roles = array();
            $roles['calendarrole'] = $calendarrole = isset($_POST['calendarrole']) ? $_POST['calendarrole']:'';
            $roles['peacemarksrole'] = $peacemarksrole = isset($_POST['peacemarksrole']) ? $_POST['peacemarksrole'] : '';
            $roles['proposalesrole'] = $proposalesrole = isset($_POST['proposalesrole']) ? $_POST['proposalesrole'] : '';
            $roles['testimonialsrole'] = $testimonialsrole = isset($_POST['testimonialsrole']) ? $_POST['testimonialsrole'] : '';
            $roles['newslettersrole'] = $newslettersrole = isset($_POST['newslettersrole']) ? $_POST['newslettersrole'] : '';
            $roles['subscriberrole'] = $subscriberrole = isset($_POST['subscriberrole']) ? $_POST['subscriberrole'] : '';

            
            $roles['donationsrole'] = $donationsrole = isset($_POST['donationsrole']) ? $_POST['donationsrole'] : '';
            $roles['imagesrole'] = $imagesrole = isset($_POST['imagesrole']) ? $_POST['imagesrole'] : '';
            $roles['newsrole'] = $newsrole = isset($_POST['newsrole']) ? $_POST['newsrole'] : '';
            $roles['pagesrole'] = $pagesrole = isset($_POST['pagesrole']) ? $_POST['pagesrole'] : '';
            $roles['usersrole'] = $usersrole = isset($_POST['usersrole']) ? $_POST['usersrole'] : '';
            $roles['vendorrole'] = $vendorrole = isset($_POST['vendorrole']) ? $_POST['vendorrole'] : '';
            $roles['eventmanagerrole'] = $eventmanagerrole = isset($_POST['eventmanagerrole']) ? $_POST['eventmanagerrole'] : '';
            $roles['settingsrole'] = $settingsrole = isset($_POST['settingsrole']) ? $_POST['settingsrole'] : '';
            $roles['programrole'] = $programrole = isset($_POST['programrole']) ? $_POST['programrole'] : '';
            $roles['projectsrole'] = $projectsrole = isset($_POST['projectsrole']) ? $_POST['projectsrole'] : '';
            $roles['callheroesrole'] = $projectsrole = isset($_POST['callheroesrole']) ? $_POST['callheroesrole'] : '';
            $roles['clubrole'] = $projectsrole = isset($_POST['clubrole']) ? $_POST['clubrole'] : '';
            $roles['homepagesrole'] = $projectsrole = isset($_POST['homepagesrole']) ? $_POST['homepagesrole'] : '';
            $roles['medialibraryrole'] = $projectsrole = isset($_POST['medialibraryrole']) ? $_POST['medialibraryrole'] : '';
            $roles['menucreaterole'] = $projectsrole = isset($_POST['menucreaterole']) ? $_POST['menucreaterole'] : '';
            $roles['organizationsrole'] = $projectsrole = isset($_POST['organizationsrole']) ? $_POST['organizationsrole'] : '';

            $userid = $_POST['userid'];
            $dc = new CB();
            //Delete if there is existing roles
            $dc->modelsManager("DELETE FROM Models\Userroles WHERE userID = '".$userid."'");

            // Inserting using new roles
            foreach ($roles as $r => $v) {
                if ($v == "true") {
                    if($r == 'donationsrole'){
                        $userroles = new Userroles();
                        $userroles->assign(array(
                            'userID' => $userid,
                            'userRoles' => $r,
                            'roleAccess' => $_POST['donationAccess'],
                            'roleGroup' => 'donation'
                            ));
                    }else{
                        $roles = Roles::findFirst('roleCode="'.$r.'"');
                        $userroles = new Userroles();
                        $userroles->assign(array(
                            'userID' => $userid,
                            'userRoles' => $r,
                            'roleAccess' => $roles->rolePage,
                            'roleGroup' => $roles->roleGroup
                            ));

                    }
                    if (!$userroles->save()) {
                        $data['error'] = "Something went wrong saving the user roles, please try again.";
                    }
                }
            }

        }
        echo json_encode($data);
    }

    // UPDATE STATUS
    public function updatestatusAction($userid,$userLevel,$status) {
        $user = Users::findFirst('userid="'.$userid.'"');

        if($status==0){
            $status=1;
        }elseif($status==1) {
            $status=0;
        }
        $user->status = $status;
        if(!$user->save()){
           $data="error";
        }else{
            $data="Success";
        }
         echo json_encode($data);
    }

    // CHECK Username
    public function chkUsernameAction() {

        $userName = Users::findFirst("userid != '".$_POST['userid']."' AND username='" . $_POST['username'] . "'");
        if ($userName) {
            die(json_encode(array('error' => array('usernametaken' => 'Username already taken.'))));
        }
    }

    // CHECK Email
    public function chkEmailAction() {

        $userEmail = Users::findFirst("userid != '".$_POST['userid']."' AND email='" . $_POST['email'] . "'");
        if ($userEmail) {
            die(json_encode(array('error' => array('emailtaken' => 'Email Address already taken.'))));
        }
    }


    public function viewuserAction($userid) {

        $profile = Users::findFirst("userid='" . $userid ."'");
        $data = array();
        if ($profile) {
            

            $data = array(
                'userid' => $profile->userid,
                'username' => $profile->username,
                'email' => $profile->email,
                'firstname' => $profile->firstname,
                'lastname' => $profile->lastname,
                'birthday' => $profile->birthday,
                'birthday2' => $profile->birthday,
                'bdaytemp' => $profile->birthday,
                'gender' => $profile->gender,
                'state' => $profile->state,
                'oldpwordtemp' => $profile->password,
                'userLevel' => $profile->userLevel,
                'imgg' => $profile->img
                );
        }
        echo json_encode($data);
    }

    // CHANGE PASSWORD
    public function userchangePasswordAction(){
        $data = array();
        if ($_POST){

            $oldpassword = sha1($_POST['oldpassword']);
            $password = sha1($_POST['password']);
            $userid = $_POST['userid'];
            $user = Users::findFirst('userid="'.$userid.'" AND password="'.$oldpassword.'"');
            if(!$user){ 
                $data['wrongpass'] = "Incorrect password.";
            }else{
                $user->password = $password; 

                if (!$user->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";
                }
            }
            


        }
        echo json_encode($data);
    }
}
