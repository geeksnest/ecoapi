<?php

namespace Controllers;

use \Models\Menu as Menu;
use \Models\Homepage as Homepage;
use \Models\Homepagesubs as Homepagesubs;
use \Controllers\ControllerBase as CB;
use \Models\Menustructure as Menustructure;

class HomepageController extends \Phalcon\Mvc\Controller {

    public function createPageAction() {
        $data = array();
        if ($_POST) {

            $sortnumber = Homepage::maximum(array("column"=> "sortnumber"));
            $guid = new \Utilities\Guid\Guid();
            $ID = $guid->GUID();
            $page = new Homepage();
            $page->assign(array(
                'id' => $ID,
                'title' => $_POST['title'],
                'content' => $_POST['content'],
                'backgroundimg' => $_POST['banner'],
                'sortnumber' => $sortnumber+1,
                'subpagecolor' => $_POST['subpagecolor'],
                'subpage' => $_POST['subpage'],
                'datecreated' => date('Y-m-d'),
                'dateupdated' => date("Y-m-d H:i:s")
                ));
                if (!$page->save()) {
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $subpages = $_POST['image'];
                        if ($subpages) {
                         foreach ($subpages as $key => $value) {
                             $addsubs = new Homepagesubs();
                             $addsubs->assign(array(
                                'contentid'      => $ID,
                                'title'       => $value['title'],
                                'shortdesc'       => $value['desc'],
                                'image'       => $value['img'],
                                'link'       => $value['imglink'],
                                'sortnum'       => $key,
                                ));
                             if (!$addsubs->save()){
                                 $data['error'] = "Something went wrong saving the data, please try again.";
                             }else{
                                $data['success'] = "Success";
                            }
                         }

                        }else{
                            $data['success'] = "Success";
                        }
                       
                     
                }
            
        echo json_encode($data);
    } 
}

    public function sortAction(){

        $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
        if($request->isPost()){
            $id = $request->getPost('id');
            foreach ($id as $key => $value) {
             $usave = Homepage::findFirst('id="' . $value['id'] . '"');
             $usave->sortnumber         = $key;
             $usave->save();
            }
        }
    }

    public function listAction() {
        $content = Homepage::find(array("order"=>"sortnumber asc"));

        $data = array();
        foreach ($content as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'datecreated' => $m->datecreated,
                );
        }

        echo json_encode(array('data' => $data));
    }


    public function deleteAction($id){
        $dlt = Homepage::findFirst('id="' . $id . '"');
        if ($dlt) {
            if($dlt->delete()){
                $deletesubs = Homepagesubs::findFirst('contentid="' . $id . '"');
                if ($deletesubs) {
                    if($deletesubs->delete()){
                        $data = array('msg' => 'Content has been successfully Deleted!', 'type'=>'success');
                    }else{
                        $data = array('type' => 'error', 'msg' => 'Error deleting content.');
                    }
                }
                else{
                  $data = array('msg' => 'Content has been successfully Deleted!', 'type'=>'success');
                }
            }else {
                $data = array('type' => 'error', 'msg' => 'Error deleting content.');
            }
        }
       

        echo json_encode($data);
    }

    public function Infoction($id) {
        $getInfo = Homepage::findFirst('id="'. $id .'"');
        $data = array(
            'id' =>  $getInfo->id,
            'title' =>  $getInfo->title,
            'banner'=>$getInfo->backgroundimg,
            'content'=>$getInfo->content,
            'subpage'=>$getInfo->subpage,
            'subpagecolor' =>$getInfo->subpagecolor
            );
        $getsubs = Homepagesubs::find(array("contentid='".$id."'","order" => "sortnum"));
        
        foreach ($getsubs as $key => $value) {
            $data2[] = array(
             'id'   =>$value->id,
             'title'=>$value->title,
             'desc'=>$value->shortdesc,
             'img'=>$value->image,
             'imglink'=>$value->link
            ); 
        }
        echo json_encode(array("data"=>$data,"data2"=>$data2));
        // echo json_encode($data);
    }

    public function UpdateAction() {
         if ($_POST) {
            //SAVE
            $usave = Homepage::findFirst('id="' . $_POST['id'] . '"');
            $usave->assign(array(
                'title' => $_POST['title'],
                'content' => $_POST['content'],
                'backgroundimg' => $_POST['banner'],
                'subpagecolor' => ($_POST['subpagecolor'] == "" ? "#ffffff" : $_POST['subpagecolor']),
                'subpage' => $_POST['subpage'],
                'dateupdated' => date("Y-m-d H:i:s")
                ));
              if (!$usave->save()) {
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $subpages = $_POST['image'];
                        $deleteid = $_POST['deleteid'];
                        if ($deleteid) {
                            foreach ($deleteid as $val) {
                                $delid = (int)$val;
                                $delsubs = Homepagesubs::findFirst('contentid="' . $_POST['id'] . '" and id = 
                                    "'.$val.'"');
                                $delsubs->delete();
                            }
                        }
                         if ($subpages) {
                           foreach ($subpages as $key => $value) {
                                if ($value['id']) {
                                 $id = (int)$value['id'];
                                 $findsubs = Homepagesubs::findFirst('id="' . $id . '"');
                                 $findsubs->title = $value['title'];
                                 $findsubs->shortdesc = $value['desc'];
                                 $findsubs->image = $value['img'];
                                 $findsubs->link = $value['imglink'];
                                 $findsubs->sortnum = $key; 
                                }
                                else{
                                    $findsubs = new Homepagesubs();
                                    $findsubs->assign(array(
                                        'contentid'      => $_POST['id'],
                                        'title'       => $value['title'],
                                        'shortdesc'       => $value['desc'],
                                        'image'       => $value['img'],
                                        'link'       => $value['imglink'],
                                        'sortnum'       => $key,
                                        ));
                                }

                                if (!$findsubs->save()){
                                     $data['error'] = "Something went wrong saving the data, please try again.";
                                 }else{
                                    $data['success'] = "Success";
                                }
                           }
                       

                    }else{
                         $data['success'] = "Success";
                    }
                    
                }
            // $delsubs = Homepagesubs::find('contentid="' . $_POST['id'] . '"');
            // $delsubs->delete();
                 // echo json_encode($data);
        }
    }

    public function listsubmenuAction() {

        $shortCode = Menu::findFirst('shortCode="[mainmenu]"');
        $menuID = $shortCode->menuID;

        $app = new CB();
        $sql = "SELECT * FROM menu WHERE menuID = '".$menuID."'";
        $searchresult = $app->dbSelect($sql);

        $conditions = 'menuID="' . $menuID . '"';
        $menuedited = Menu::findFirst(array($conditions));
        if($menuedited->edited == 0){
            $sql2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID";
            $sql2 .= " WHERE menusubs.menuID = '".$menuID."' ORDER BY menustructure.num ASC";
            $searchresult2 = $app->dbSelect($sql2); 
        }else{
            $sql2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID";
            $sql2 .= " WHERE menustructure.child = 0 AND menusubs.menuID = '".$menuID."' ORDER BY menustructure.num ASC";
            $searchresult2 = $app->dbSelect($sql2); 
        }       
        echo json_encode(array('data' => $searchresult2, 'menuname' => $searchresult));
    }
    public function getalldataAction(){
        $app = new CB();
        $content = Homepage::find(array("order"=>"sortnumber asc"));

        foreach ($content as $deta) {
            $child1 = "SELECT * FROM homepagesubs";
            $child1 .= " WHERE contentid='".$deta->id."' ORDER BY sortnum ASC";
            $child_1 = $app->dbSelect($child1); 
            
            $data[] = array(
                'id'  => $deta->id,
                'title'   => $deta->title,
                'content'   => $deta->content,
                'backgroundimg'   => $deta->backgroundimg,
                'sortnumber'   => $deta->sortnumber,
                'subpagecolor' => $deta->subpagecolor,
                'subpage' => $child_1,
                );
        }
        echo json_encode($data);


    }
    public function getsubpageAction(){
        $app = new CB();
        $content = Homepage::find(array("order"=>"sortnumber asc"));

        foreach ($content as $deta) {

            $child1 = "SELECT * FROM homepagesubs";
            $child1 .= " WHERE contentid='".$deta->id."' ORDER BY sortnum ASC";
            $child_1 = $app->dbSelect($child1); 

            $data[] = array(
                'id'  => $deta->id,
                'title'   => $deta->title,
                'subpage' => $child_1,
                );
        }
        echo json_encode($data);

    }
}
