<?php

namespace Controllers;

class ControllerBase extends \Phalcon\Mvc\Controller {

  public function modelsManager($phql) {
    $DI = \Phalcon\DI::getDefault();
    $app = $DI->get('application');
    return $result = $app->modelsManager->executeQuery($phql);
  }

  public function dbSelect($phql) {
    $db1 = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db1->prepare($phql);
    $stmt->execute();
    $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $searchresult;
  }

  public function getConfig(){
    $DI = \Phalcon\DI::getDefault();
    $app = $DI->get('application');
    return $app->config;
  }

  public function sendMandrillEmailAction($html, $to, $subject){
    $DI = \Phalcon\DI::getDefault();
    $app = $DI->get('application');

    $postString = '{
      "key": "'.$app->config->mandrillApi->password.'",
      "message": {
        "html": "'.$html.'",
        "subject": "'.$subject.'",
        "from_email": "info@earthcitizens.org",
        "from_name": "Earth Citizens Organization",
        "to": [
        {
          "email": "'.$to.'"
        }
        ]
      },
      "async": false
    }';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $app->config->mandrillApi->sendApi);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);

    $result = curl_exec($ch);

    return $result;
  }

  public function invoiceVendorTemplate($contactperson, $orgname, $TransactId, $sepatotal, $elec, $vendortype, $electricity, $table, $totalpayment, $date, $paid){
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                ';


                if($paid == 'Unpaid'){
                  $content .= '  <p style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#374550">
                    Hello '.$contactperson.',<br><br> 
                    
                    Thank you for joining us!<br><br>Please mail your payment check to 10702 NE 68th St, Kirkland, WA 98033 and make the checks payable to ECO.
                    </p>
                    <div class=title style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;color:#374550">
                    <h2>Vendor Invoice</h2>';
                }else{
                  $content .= '  <p style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#374550">
                    Hello '.$contactperson.',<br><br> 
                  
                    We received your payment via mail. This letter serves as your transaction receipt.<br><br>Thank you for joining us.
                    </p>
                    <div class=title style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;color:#374550">
                    <h2>Vendor Receipt</h2>';
                }

                  $content .= '
                    Business/Organization<br>
                    &nbsp;&nbsp;&nbsp;Name: <strong>'.$orgname.'</strong><br>
                    &nbsp;&nbsp;&nbsp;Type: <strong>'.$vendortype.'</strong><br>
                    Transaction ID: <strong>'.$TransactId.'</strong><br>
                    Date: <strong>'.$date.'</strong><br>
                  </div>

                  <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                    <div>';




                                  
                      $content .=  '
                      <div class="line"></div>
                        <table border="1" cellpadding="0" cellspacing="0" width="100%" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;">          
                          <tbody>
                            <tr>
                              <td style="padding: 5px; text-align: left"><strong>Electricity</strong></td>
                              <td style="padding: 5px;">'.$electricity.'</td>
                              <td style="padding: 5px;text-align: right">$'.number_format($elec, 2, '.', '').'</td>
                            </tr>
                            <tr>
                              <td style="padding: 5px; text-align: left"><strong>Number of Table</strong></td>
                              <td style="padding: 5px;">'.$table.'</td>
                              <td style="padding: 5px;text-align: right">$'.number_format($sepatotal, 2, '.', '').'</td>
                            </tr>
                            <tr>
                              <td colspan="2" style="padding: 5px; text-align: left"><strong>Total</strong></td>
                              <td style="padding: 5px;text-align: right">$'.number_format($totalpayment, 2, '.', '').'</td>
                            </tr>
                            <tr>
                              <td colspan="2" style="padding: 5px; text-align: left"><strong>Payment Type</strong></td>
                              <td style="padding: 5px;">Cash</td>
                            </tr>
                            <tr>
                              <td colspan="2" style="padding: 5px; text-align: left"><strong>Remarks</strong></td>
                              <td style="padding: 5px;">'.$paid.'</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      ';
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  }

  public function ReceiptTemplate($TransactId,$date,$title,$recieptContent,$name,$amount,$pay_type){
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  ';


                
                    $first = str_replace("[name]","$name","$recieptContent");
                    $second = str_replace("[TransactId]","$TransactId","$first");
                    $third = str_replace("[pay_type]","$pay_type","$second");
                    $fourth = str_replace("[amount]","$amount","$third");
                    $fifth = str_replace("[date]","$date","$fourth");
                    $sixth = str_replace("[title]","$title","$fifth");

                    $content .= $sixth;

                  
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  } 

  public function resetPassword($pass,$firstname,$lastname){
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  ';


                
                    $content .= ' <br/> <br/>
                                  Hi '.$firstname.' '.$lastname.', <br/> <br/>
                                  We have generated a new password for you.<br/> <br/>
                                  The site does not have yet a profile feature which you 
                                  could edit your information so please use your generated password as of the moment. 
                                  <br/><br/>
                                  Thanks! <br/><br/>

                                  Generated Password: <b>' . $pass . '</b> <br/>
                         ';

                  
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  }

  public function memberUpdate($username,$email,$birthday,$gender,$firstname,$lastname,$location,$zipcode,$centername,$howdidyoulearn,$refer){
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  ';


                
                    $content .= '   <br/> <br/>
                                    Your profile has been updated! Check below:
                                    <br/><br/>
                                    Username: ' . $username . ' <br/>
                                    Email: ' . $email . ' <br/>
                                    Birthday: ' . $birthday . ' <br/>
                                    Gender: ' . $gender . ' <br/>
                                    Firstname: ' . $firstname . ' <br/>
                                    Lastname: ' . $lastname . ' <br/>
                                    Location: ' . $location . ' <br/>
                                    Zipcode: ' . $zipcode . ' <br/>
                                    Centername: ' . $centername . ' <br/>
                                    How did you learn about us?: ' . $howdidyoulearn . ' <br/>
                                    ' . ($refer == '' ? '' : 'Refered Friend: '.$refer) . ' <br/>
                        ';

                  
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  }

  public function sendRefer($firstname,$lastname,$baseURL){
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  ';


                
                    $content .= '   <br/> <br/>
                    Hello friend, <br/><br/> We are pleased to let you know '
                    . $firstname .' '. $firstname .' wanted you to become a member of the Earth Citizens Organization (ECO). 
                    <br><br> ECO was established as a 501(c)(3) nonprofit organization to promote mindful living for a sustainable world. 
                    The name of the organization represents the understanding that we all are citizens of the Earth and share the responsibility 
                    for its well-being. <br><br> We all know that we need to change to make our lives on this planet sustainable. 
                      To make the change feasible, ECO proposes to start making changes from soft simple things such as the way we breathe, 
                    the way we eat, the way we manage our stress, and the way we feel toward other people and other living beings. 
                    <br><br> These elements, together with training and practices for mindfulness and leadership, are the foundation of 
                    the training and education that ECO provides for the people who desire to initiate the change in their lives and their 
                    community. Graduates of our programs return to their communities ready to share what they have learned and experienced. 
                    The generous donations of individuals and supporting institutions make it possible for ECO to provide this invaluable 
                    education below cost. For a $10 minimum donation, you can become a part of the community of Earth Citizens. 
                    <br><br> To join ECO and become an Earth Citizen, please visit the link below. <br> <br> 
                    <a href="' . $baseURL . '/donation"> '. $baseURL .'/donation </a> <br> <br> 
                    Thank you for your kind interest, <br> <br> ECO Staff'
                        ;

                  
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  }

  public function approvedProjEmail($projTitle,$firstname,$lastname,$baseURL,$status,$msg = null){
   
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  ';

                  if($status == 1){
                    $content .= '   <br/> <br/>
                    Hello '.$firstname.' '.$lastname.', <br/><br/> We are pleased to let you know, that your project proposal <i>'.$projTitle.'</i> have been approved by the ECO Board and ready 
                    to publish.<br/><br/>';

                    $content .= 'Please login to publish your project : Click here to <a href="'.$baseURL .'/login">Login.</a>';
                  }else if($status == 3){
                    $content .= '   <br/> <br/>
                    Hello '.$firstname.' '.$lastname.', <br/><br/> We are pleased to let you know, that your project proposal <i>'.$projTitle.'</i> was declined by the ECO Board.
                    <br><br>
                    Note:
                    <br>
                    '.$msg.'
                    <br><br>
                    Please review and revise your project proposal.<br/><br/>';

                    $content .= 'Click here to <a href="'.$baseURL .'/login">Login.</a>';
                  }
                
                    

                  
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  }

   public function submitProjEmail($projTitle,$firstname,$lastname,$desc,$duration,$goal,$datecreated){
   
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                    Someone created a project<br><br>

                    Name : '.$firstname.' '.$lastname.'<br>
                    Date Created : '.$datecreated.' <br><br>

                    Project <br><br>
                    Title : '.$projTitle.'<br>
                    Short Description :<br>
                    '.$desc.'<br>
                    Duration : '.$duration.'<br>
                    Donation Goal : '.$goal.'

                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  }


  public function finishedProjEmail($projTitle,$firstname,$lastname,$baseURL,$projTotalDonations){
   
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  ';

                  
                    $content .= '   <br/> <br/>
                    Congratulations, '.$firstname.' '.$lastname.' <br/><br/> 
                    Your project <i>'.$projTitle.'</i> had collected a funds, total of $ '.$projTotalDonations.'.<br/><br/>';

                    $content .= 'Please login to check your project : Click here to <a href="'.$baseURL .'/login">Login.</a>';
                  
                
                    

                  
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  }

  public function projectReceiptTemplate($creditTransactId,$name,$amount,$pay_type,$projTitle){
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  ';


                    $content .= 'Hi '.$name.'<br/>
                                  Thank you for your donation for the "'.$projTitle.'".<br/><br/>
                                  Transact ID: '.$creditTransactId.'<br/>
                                  Amount: '.$amount.'<br/>
                                  Payment Type: '.$pay_type.'<br/><br/>';

                  
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  }

  public function approvedvideoEmail($Title,$firstname,$lastname,$baseURL,$status,$msg = null){
   
    $content = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang=en> 
    <head>
      <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <!-- So that mobile will display zoomed in -->
      <meta http-equiv=X-UA-Compatible content=IE=edge>
      <!-- enable media queries for windows phone 8 -->
      <meta name=format-detection content=telephone=no>
      <!-- disable auto telephone linking in iOS -->
      <title>Earth Citizens Organization</title>
      <style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
        table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}
        @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
        .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
      </style>
    </head> 
    <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
      <!-- 100% background wrapper (grey background) --> 
      <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
        <tr>
          <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
            <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
              <tr>
                <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
              </tr> 
              <tr> 
                <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                  <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  ';

                  if($status == 11){
                    $content .= '   <br/> <br/>
                    Hello '.$firstname.' '.$lastname.', <br/><br/> We are pleased to let you know, that your Video : <i>'.$Title.'</i> have been approved by the ECO Board<br/><br/>';

                    $content .= 'Please login to publish your project : Click here to <a href="'.$baseURL .'/login">Login.</a>';
                  }else if($status == 10){
                    $content .= '   <br/> <br/>
                    Hello '.$firstname.' '.$lastname.', <br/><br/> We are pleased to let you know, that your Video <i>'.$Title.'</i> was declined by the ECO Board.
                    <br><br>
                    Note:
                    <br>
                    '.$msg.'
                    <br><br>
                    Please review and revise your Video or its details.<br/><br/>';

                    $content .= 'Click here to <a href="'.$baseURL .'/login">Login.</a>';
                  }
                
                    

                  
                    $content .=  '
                    <br><br></div>
                  </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                  <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                  ';
                  return $content;

  } 


}
