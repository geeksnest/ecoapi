<?php
namespace Controllers;
use \Models\Eventsbanner as Eventsbanner;
use \Models\Eventslog as Eventslog;
use \Models\Eventcontents as Eventcontents;
use \Models\Eventcenternames as Eventcenternames;
use \Models\Eventamounts as Eventamounts;
use \Models\Pages as Pages;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class EventsmanagerController extends \Phalcon\Mvc\Controller
{


    public function listeventsbannerAction(){

        $getImage= Eventsbanner::find(array("order" => "id DESC"));
        foreach ($getImage as $getImages) {;
            $data[] = array(
                'id'   => $getImages->id,
                'filename'   => $getImages->filename
                );
        }
        echo json_encode($data);
    }

    public function addeventsbannerAction($filename, $type){


        $filename = $_POST['imgfilename'];
        $picture = new Eventsbanner();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        echo json_encode($data);
    }

    public function dltnewsimgAction(){
        $id = $_POST['id'];
        $dltPhoto = Eventsbanner::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }



    public function publishEventAction(){

        $guid = new \Utilities\Guid\Guid();
        $eventID = $guid->GUID();

        $event = new Eventcontents();
        $event->assign(array(
            'eventID' => $eventID,
            'title' => $_POST['title'],
            'URL' => $_POST['url'],
            'shortDesc' => $_POST['shortDesc'],
            'content' => $_POST['content'],
            'banner' => $_POST['banner'],
            'otheramount' => $_POST['otheramount'],
            'hdyla' => $_POST['hdyla'],
            'recieptSubject' => $_POST['recieptSubject'],
            'recieptContent' => $_POST['recieptContent'],
            'status' => 1,
            'dateCreated' => date('Y-m-d'),
            'dateUpdated' => date('Y-m-d')
,
            ));

        if (!$event->save()) {
            $errors = array();
            foreach ($event->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
        }else{

            $data['success_event_contents'] = "Success";
            $eventamounts = $_POST['amounts'];
            foreach($eventamounts as $eventamounts){
                $amounts = new Eventamounts();
                $amounts->assign(array(
                    'eventID' => $eventID,
                    'eventAmounts' => $eventamounts['amounts']
                    ));
                if (!$amounts->save()){
                    $errors = array();
                    foreach ($amounts->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }else{
                    $data['success_amount'] = $eventamounts." Amounts Saved.";
                }
            }
            $eventcnames = $_POST['centernames'];
            foreach($eventcnames as $eventcnames){
                $cnames = new Eventcenternames();
                $cnames->assign(array(
                    'eventID' => $eventID,
                    'cname' => $eventcnames['centernames']
                    ));
                if (!$cnames->save()){
                    $errors = array();
                    foreach ($cnames->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }else{
                    $data['success_centernames'] = $eventcnames." CenterNames Saved.";
                }
            }
        }
        echo json_encode($data);
    }


    //EVENTS LIST
    public function eventlistAction($num, $page, $keyword, $sort, $sortto) {

        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {
            
            $conditions = "SELECT * FROM eventcontents ";            

        } else {
            
            $conditions = "SELECT * FROM eventcontents WHERE title LIKE '%". $keyword ."%' OR URL LIKE '%". $keyword ."%' ";
            
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count)
            )); 
    }

    public function eventcontentsAction($eventID) {

        
        $conditions = 'eventID="'.$eventID.'"';
        $event = Eventcontents::findFirst($conditions);
        $eventamounts = Eventamounts::find($conditions);
        $eventcenternames = Eventcenternames::find($conditions);

        $dbamounts = \Phalcon\DI::getDefault()->get('db');
        $getamounts = $dbamounts->prepare("SELECT eventAmounts as amounts FROM eventamounts WHERE eventID = '" . $eventID . "' ORDER BY amounts ASC");
        $getamounts->execute();
        $amounts = $getamounts->fetchAll(\PDO::FETCH_ASSOC);

        $dbcenternames = \Phalcon\DI::getDefault()->get('db');
        $getcenternames = $dbcenternames->prepare("SELECT cname as centernames FROM eventcenternames WHERE eventID = '" . $eventID . "' ORDER BY centernames ASC");
        $getcenternames->execute();
        $centernames = $getcenternames->fetchAll(\PDO::FETCH_ASSOC);

        // if($event->paymentInfo == 'no'){
        //     $paymentInfo = false;
        // }else{
        //     $paymentInfo = true;
        // }

        // if($event->billingInfo == 'no'){
        //     $billingInfo = false;
        // }else{
        //     $billingInfo = true;
        // }

        if($event->otheramount == 'no'){
            $otheramount = false;
        }else{
            $otheramount = true;
        }
        $data = array();
        if ($event) {
            $data = array(
                'eventID' => $event->eventID,
                'title' => $event->title,
                'URL' => $event->URL,
                'shortDesc' => $event->shortDesc,
                'content' => $event->content,
                'banner' => $event->banner,
                'otheramount' => $otheramount,
                'hdyla' => $event->hdyla,
                'recieptSubject' => $event->recieptSubject,
                'recieptContent' => $event->recieptContent,
                'status' => $event->status,
                'dateCreated' => $event->dateCreated,
                'dateUpdated' => $event->dateUpdated,
                'prices' => $amounts,
                'cnames' => $centernames
                );
        }
        echo json_encode($data);
    }

    public function updateEventAction() {

        $data = array();
        if ($_POST) { 

            $eventID = $_POST['eventID'];
            $title = $_POST['title'];
            $newStr = trim($title, '"');
            $title2 = str_replace('\"', '', $newStr);  

            $eventTitle = Eventcontents::findFirst('title LIKE "' . $title2 . '"');
            $idpage = Eventcontents::findFirst('title LIKE "' . $title2 . '" AND eventID= "' . $eventID.'"');

            if ($eventTitle == true && $idpage == false ) {
                ($eventTitle == true) ? $data["eventalreadyexist"] = "The event title is already exist!" : '';
            }else{
                
                $event = Eventcontents::findFirst('eventID= "' . $eventID.'"');
                $event->title = $title;
                $event->URL = $_POST['URL'];
                $event->shortDesc = $_POST['shortDesc'];
                $event->content = $_POST['content'];

                if (!$event->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";
                } 
            }        
        }
        echo json_encode($data);
    }

    public function saveEmailInfoAction() {

        $data = array();
        if ($_POST) { 

            $eventID = $_POST['eventID'];           

            $event = Eventcontents::findFirst('eventID= "' . $eventID.'"');
            $event->recieptSubject = $_POST['recieptSubject'];
            $event->recieptContent = $_POST['recieptContent'];

            if (!$event->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            } 
        }        
        
        echo json_encode($data);
    }

    public function saveBannerAction() {

        $data = array();
        if ($_POST) { 

            $eventID = $_POST['eventID'];           

            $event = Eventcontents::findFirst('eventID= "' . $eventID.'"');
            $event->banner = $_POST['banner'];

            if (!$event->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            } 
        }        
        
        echo json_encode($data);
    }

    public function saveAmountsAction() {

        $data = array();
        if ($_POST) { 

            $eventID = $_POST['eventID'];           

            $event = Eventcontents::findFirst('eventID= "' . $eventID.'"');
            $event->otheramount = $_POST['otheramount'];

            if (!$event->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";

                $conditions = 'eventID="' . $eventID . '"';
                $delete = Eventamounts::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($delete) {
                    foreach($delete as $delete){

                        if ($delete->delete()) {
                            $data = array('success' => 'tags Deleted');
                        }
                    }
                }
                $eventamounts = $_POST['amounts'];
                foreach($eventamounts as $eventamounts){
                    $amounts = new Eventamounts();
                    $amounts->assign(array(
                        'eventID' => $eventID,
                        'eventAmounts' => $eventamounts['amounts']
                        ));
                    if (!$amounts->save()){
                        $errors = array();
                        foreach ($amounts->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    }else{
                        $data['success_amount'] = $eventamounts." Amounts Saved.";
                    }
                }
            } 
        }        
        
        echo json_encode($data);
    }   

    // public function savePaymentInfoAction() {

    //     $data = array();
    //     if ($_POST) { 

    //         $eventID = $_POST['eventID'];           

    //         $event = Eventcontents::findFirst('eventID= "' . $eventID.'"');
    //         $event->paymentInfo = $_POST['paymentInfo'];

    //         if (!$event->save()) {
    //             $data['error'] = "Something went wrong saving the data, please try again.";
    //         } else {
    //             $data['success'] = "Success";
                
    //         } 
    //     }        
        
    //     echo json_encode($data);
    // }

    // public function savebillingInfoAction() {

    //     $data = array();
    //     if ($_POST) { 

    //         $eventID = $_POST['eventID'];           

    //         $event = Eventcontents::findFirst('eventID= "' . $eventID.'"');
    //         $event->billingInfo = $_POST['billingInfo'];

    //         if (!$event->save()) {
    //             $data['error'] = "Something went wrong saving the data, please try again.";
    //         } else {
    //             $data['success'] = "Success";
                
    //         } 
    //     }        
        
    //     echo json_encode($data);
    // }

    public function savehdylaAction() {

        $data = array();
        if ($_POST) { 

            $eventID = $_POST['eventID'];           

            $event = Eventcontents::findFirst('eventID= "' . $eventID.'"');
            $event->hdyla = $_POST['hdyla'];

            if (!$event->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
                
            } 
        }        
        
        echo json_encode($data);
    }

    public function saveCnameAction() {

        $data = array();
        if ($_POST) { 

            $eventID = $_POST['eventID']; 

            $conditions = 'eventID="' . $eventID . '"';
            $delete = Eventcenternames::find(array($conditions));
            $data = array('error' => 'Not Found');
            if ($delete) {
                foreach($delete as $delete){

                    if ($delete->delete()) {
                        $data = array('success' => 'tags Deleted');
                    }
                }
            }
            $eventcnames = $_POST['centernames'];
            foreach($eventcnames as $eventcnames){
                $cnames = new Eventcenternames();
                $cnames->assign(array(
                    'eventID' => $eventID,
                    'cname' => $eventcnames['centernames']
                    ));
                if (!$cnames->save()){
                    $errors = array();
                    foreach ($cnames->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }else{
                    $data['success_amount'] = $eventcnames." Amounts Saved.";
                }
            }
        } 

        
        echo json_encode($data);
    }

    public function updateEventStatusAction($status,$eventID) {

        $data = array();
        if ($status == 1){
            $stat = 0;
        }else{
            $stat = 1;
        }
        $conditions = 'eventID="'.$eventID.'"';
        $event = Eventcontents::findFirst($conditions);
        $event->status = $stat;
        if (!$event->save()) {
            $data['error'] = "Something went wrong updating Events status, please try again.";
        } else {
            $data['success'] = "Success";
        }

        echo json_encode($data);
    }

    public function deleteEventAction($eventID) {
        $conditions = 'eventID="'.$eventID.'"';
        $event = Eventcontents::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($event) {
            if ($event->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    //Collections

    public function eventsCollectionAction($num, $page, $keyword, $eventID, $sort, $sortto) {

        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {
            
            $conditions = "SELECT * FROM eventslog WHERE donatedto LIKE '".$eventID."' ";
            
        } else {
            
            $conditions = "SELECT * FROM eventslog WHERE donatedto LIKE '".$eventID."' AND useremail LIKE '%" . $keyword . "%'";
            $conditions .= " OR transactionId LIKE '" . $keyword . "' OR billinginfofname LIKE '%" . $keyword . "%' OR billinginfolname LIKE '%" . $keyword . "%'";
            $conditions .= " OR paymentmode LIKE '%" . $keyword . "%' OR amount LIKE '" . $keyword . "' OR datetimestamp LIKE '%" . $keyword . "%'";
            $conditions .= " OR howdidyoulearn LIKE '%". $keyword ."%' OR cname LIKE '%". $keyword ."%' ";
            
        }
        $eventquery = 'eventID="'.$eventID.'"';        
        $event = Eventcontents::findFirst(array($eventquery));

        
        $conditions3 = "SELECT DISTINCT(useremail) as useremail FROM eventslog WHERE donatedto LIKE '".$eventID."'";
        

        $collects = "SELECT amount FROM eventslog WHERE donatedto LIKE '".$eventID."'"; 


        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions); 

        $totals = $app->dbSelect($conditions3); 

        $collections = $app->dbSelect($collects); 

        echo json_encode(array(
            'title' => $event->title,
            'totals' => count($totals),
            'collections' => $collections,
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count)
            ));

    }

    public function deleteEventCollectionAction($transactionId) {
        $conditions = 'transactionId="'.$transactionId.'"';
        $event = Eventslog::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($event) {
            if ($event->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    //Frontend

    public function getEventContentsAction($eventsURL) {

        
        $conditions = 'URL="'.$eventsURL.'"';
        $event = Eventcontents::findFirst($conditions);        

        $dbamounts = \Phalcon\DI::getDefault()->get('db');
        $getamounts = $dbamounts->prepare("SELECT eventAmounts as amounts FROM eventamounts WHERE eventID = '" . $event->eventID . "' ORDER BY amounts ASC");
        $getamounts->execute();
        $amounts = $getamounts->fetchAll(\PDO::FETCH_ASSOC);

        $mindbamounts = \Phalcon\DI::getDefault()->get('db');
        $mingetamounts = $mindbamounts->prepare("SELECT MIN(eventAmounts) as minamounts FROM eventamounts WHERE eventID = '" . $event->eventID . "'");
        $mingetamounts->execute();
        $minAmounts = $mingetamounts->fetchAll(\PDO::FETCH_ASSOC);

        $dbcenternames = \Phalcon\DI::getDefault()->get('db');
        $getcenternames = $dbcenternames->prepare("SELECT cname as centernames FROM eventcenternames WHERE eventID = '" . $event->eventID . "' ORDER BY centernames ASC");
        $getcenternames->execute();
        $centernames = $getcenternames->fetchAll(\PDO::FETCH_ASSOC);

        
        $data = array();
        if ($event) {
            $data = array(
                'eventID' => $event->eventID,
                'title' => $event->title,
                'URL' => $event->URL,
                'shortDesc' => $event->shortDesc,
                'content' => $event->content,
                'banner' => $event->banner,
                'paymentInfo' => $event->paymentInfo,
                'billingInfo' => $event->billingInfo,
                'otheramount' => $event->otheramount,
                'hdyla' => $event->hdyla,
                'recieptSubject' => $event->recieptSubject,
                'recieptContent' => $event->recieptContent,
                'status' => $event->status,
                'dateCreated' => $event->dateCreated,
                'dateUpdated' => $event->dateUpdated,
                'prices' => $amounts,
                'cnames' => $centernames,
                'minPrice' => $minAmounts
                );
        }
        echo json_encode($data);
    }

    public function getEventListAction($offset,$num) {
        $app = new CB();
        $conditions = "SELECT ";
        $conditions .= "eventID,title,URL,shortDesc,banner ";
        $conditions .= "FROM eventcontents ";
        $conditions .= "WHERE status=1 ";
        $conditions .= "ORDER BY num DESC";
        $limit = " LIMIT ".$offset.", ". $num; 
        $searchresult = $app->dbSelect($conditions.$limit);
        $count = $app->dbSelect($conditions);
        echo json_encode(array('data' => $searchresult, 'count' => count($count)));
    }



}

