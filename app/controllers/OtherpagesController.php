<?php

namespace Controllers;

use \Models\Otherpages as Otherpages;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class OtherpagesController extends \Phalcon\Mvc\Controller {

    public function addOtherPageAction() {
        
        if ($_POST) {

            $guid = new \Utilities\Guid\Guid();
            $pageID = $guid->GUID();

            $page = new Otherpages();
            $page->pageID = $pageID;
            $page->title = $_POST['title'];
            $page->slugs = $_POST['slugs'];
            $page->content_1 = $_POST['content_1'];
            $page->content_2 = $_POST['content_2'];
            $page->content_3 = $_POST['content_3'];
            $page->content_4 = $_POST['content_4'];
            $page->content_5 = $_POST['content_5'];
            $page->status = 1;
            $page->date_created = date("Y-m-d H:i:s");
            $page->date_updated = date("Y-m-d H:i:s");

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }

    public function checkpagetitlesAction($pagetitle) {
        $data = array();
        if($pagetitle){

            $newStr = trim($pagetitle, '"');
            $title = str_replace('\"', '', $newStr);  
            $pagetitle = Pages::findFirst('title LIKE "' . $title . '"');

            if ($pagetitle == true) {
                ($pagetitle == true) ? $data["pagealreadyexist"] = "The page title is already exist!" : '';
            }else{
                ($pagetitle == false) ? $data["pageareavailable"] = "The page title are available." : '';            
            }

            echo json_encode($data);
        }
    }

    public function pageslistAction($num, $page, $keyword) {

        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {
            
            $conditions = "SELECT * FROM otherpages ";

        } else {

            $conditions = "SELECT * FROM otherpages WHERE title LIKE '%". $keyword ."%' ";
            
        }

        $conditions .= "ORDER BY date_created DESC";

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count)
            )); 


    }

    public function getPageAction($pageID) {
        $app = new CB();
        $conditions = "SELECT * FROM otherpages WHERE pageID='".$pageID."'";
        $searchresult = $app->dbSelect($conditions); 
        echo json_encode($searchresult);        
    }

    public function updateOtherPageAction() {

        if ($_POST) { 

            $pageID = $_POST['pageID'];
            $pagetitle = $_POST['title'];
            $newStr = trim($pagetitle, '"');
            $title2 = str_replace('\"', '', $newStr);  

            $titlepage = Otherpages::findFirst('title LIKE "' . $title2 . '"');
            $idpage = Otherpages::findFirst('title LIKE "' . $title2 . '" AND pageID= "' . $pageID . '"');

            if ($titlepage == true && $idpage == false ) {
                ($titlepage == true) ? $data["pagealreadyexist"] = "The page title is already exist!" : '';
            }else{
                $page = Otherpages::findFirst('pageID="' . $pageID . '"');
                if($page){
                    $page->title = $pagetitle;
                    $page->slugs = $_POST['slugs'];
                    $page->content_1 = $_POST['content_1'];
                    $page->content_2 = $_POST['content_2'];
                    $page->content_3 = $_POST['content_3'];
                    $page->content_4 = $_POST['content_4'];
                    $page->content_5 = $_POST['content_5'];
                    $page->date_updated = date("Y-m-d H:i:s");

                    if (!$page->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $data['success'] = "Success";
                    }  
                }

            }        
        }
        echo json_encode($data);
    }

    public function pagedeleteAction($pageID) {
        $conditions = "pageID='" . $pageID . "'";
        $page = Otherpages::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if ($page->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    public function getPageprojectAction() {
        echo json_encode(Pages::findFirst(array())->toArray(), JSON_NUMERIC_CHECK);
    }


    public function updatepagestatusAction($status,$pageid) {

        $data = array();
        if ($status == 1){
            $stat = 0;
        }else{
            $stat = 1;
        }
        $conditions = 'pageid="'.$pageid.'"';
        $page = Pages::findFirst($conditions);
        $page->status = $stat;
        if (!$page->save()) {
            $data['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";
        }

        echo json_encode($data);
    }

    public function createorgAction() {
        $data = array();
        if ($_POST) { 
            $check = $_POST['agree'];
            if ($check==true) {
                $atw = new Atw();
                $atw->assign(array(
                    'orgname' => $_POST['org'],
                    'taxid' => $_POST['orgid'],
                    'phone' => $_POST['phone'],
                    'address' => $_POST['address'],
                    'created_at' => date('Y-m-d')
                    ));
                if (!$atw->save()) {
                    $errors = array();
                    foreach ($atw->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";
                    $mail = new PHPMailer();
                    $adminemail = "ecoaroundtheworld@mailinator.com";
                    $mail->isSMTP();
                    $mail->Host = 'smtp.mandrillapp.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'efrenbautistajr@gmail.com';
                    $mail->Password = '6Xy18AJ15My59aQNTHE5kA';               
                    $mail->Port = 587;

                    $mail->From = 'ecoaroundtheworld@mailinator.com';
                    $mail->FromName = 'ECO Around the World';
                    $mail->addAddress($adminemail);

                    $mail->isHTML(true);
                    $mail->Subject = 'ECO Around the World Notification';
                    $mail->Body = '
                    <h3>A new Organization has been listed to Eco Around the world </h3>
                        Organization: '.$_POST['org'].' <br/>
                        Organization Tax ID:'.$_POST['orgid'].' <br/>
                        Phone:'.$_POST['contactno'].' <br/>
                        Address:'.$_POST['address'].' <br/>
                    ';
                    if (!$mail->send()) {
                        $data['error'] = "Email not sent";
                    } else {
                        $data['email'] = "Success";
                    }
                   
                }
               echo json_encode($data);
            }
        }
        else{
             $data['error'] = "Something went wrong saving the data, please try again.";
            echo json_encode($data);
        }

    }

    public function atwlistAction($num, $page, $keyword, $sort, $sortto, $datefrom, $dateto) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {
            
            $conditions = "SELECT * FROM atw ";
        } else {
            $conditions = "SELECT * FROM atw WHERE orgname LIKE '%". $keyword ."%' OR taxid LIKE '%". $keyword ."%'
            OR phone LIKE '%". $keyword ."%' OR address LIKE '%". $keyword ."%' ";   
        }

         if($datefrom!= "null" && $dateto != "null"){
           $conditions .= "WHERE created_at >= '$datefrom' AND created_at <= '$dateto' ";
        }

         if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }


        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");
        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count)
            ));
    }

    public function atwdeleteAction($pageid) {
        $conditions = "id=" . $pageid;
        $page = Atw::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if ($page->delete()) {
                $data = array('success' => 'Successfully Deleted');
            }
        }
        echo json_encode($data);
    }


}
