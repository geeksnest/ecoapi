<?php
namespace Controllers;

use \Models\Members as Members;
use \Models\Donationlog as Donationlog;
use \Models\Donationlog1 as Donationlog1;
use \Models\Donationlogothers as Donationlogothers;
use \Models\Donationlogothers1 as Donationlogothers1;
use \Models\Donationloghouston as Donationloghouston;
use \Models\Donationlogseattle as Donationlogseattle;
use \Models\Eventslog as Eventslog;
use \Models\Eventcontents as Eventcontents;
use Controllers\ControllerBase as CB;

class DonationController extends \Phalcon\Mvc\Controller
{

    public function donationlistAction($num, $page, $keyword, $sort, $sortto, $listIn) {
        $app = new CB();
        $offsetfinal = ($page * 10) - $num;  

        if ($keyword == 'null' || $keyword == 'undefined') {

                $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email ";

        }else{           

            if($listIn == "normal"){

                $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email";
                $conditions .= " WHERE members.firstname LIKE '%" . $keyword . "%' OR members.lastname LIKE '%" . $keyword . "%'";
                $conditions .= " OR useremail LIKE '%" . $keyword . "%' OR transactionId LIKE '" . $keyword . "' OR paymentmode LIKE '%" . $keyword . "%'";
                $conditions .= " OR amount LIKE '" . $keyword . "' OR billinginfofname LIKE '%" . $keyword . "%' OR billinginfolname LIKE '%" . $keyword . "%'";
                $conditions .= " OR recurcount LIKE '" . $keyword . "' OR recurduration LIKE '" . $keyword . "' OR recurtype LIKE '" . $keyword . "'";
                $conditions .= " OR recurstartdate LIKE '" . $keyword . "' OR datetimestamp LIKE '%" . $keyword . "%' ";

            }else if($listIn == "recurr"){

                $recurr_count = $_POST["count"];
                $recurr_year = $_POST["year"];
                $recurr_month = $_POST["month"];
                $recurr_day = $_POST["day"];


                if($recurr_count !== '' && $recurr_year == '' && $recurr_month == '' && $recurr_day == ''){
                //count only
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurcount LIKE '" . $recurr_count . "' ";

                }else if($recurr_count !== '' && $recurr_year !== '' && $recurr_month == '' && $recurr_day == ''){
                //count and year
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurcount LIKE '" . $recurr_count . "' AND recurstartdate LIKE '%" . $recurr_year . "%' ";

                }else if($recurr_count !== '' && $recurr_year !== '' && $recurr_month !== '' && $recurr_day == ''){
                //count, year and month

                    $ym = $recurr_year .'-'. $recurr_month;
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurcount LIKE '" . $recurr_count . "' AND recurstartdate LIKE '%" . $ym . "%' ";

                }else if($recurr_count !== '' && $recurr_year !== '' && $recurr_month !== '' && $recurr_day !== ''){
                //count, year, month and day

                    $ym = $recurr_year .'-'. $recurr_month .'-'. $recurr_day;
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurcount LIKE '" . $recurr_count . "' AND recurstartdate LIKE '%" . $ym . "%' ";

                }else if($recurr_count !== '' && $recurr_year == '' && $recurr_month !== '' && $recurr_day == ''){
                //count and month

                    $ym = $recurr_month;
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurcount LIKE '" . $recurr_count . "' AND recurstartdate LIKE '%" . $ym . "%' ";

                }else if($recurr_count !== '' && $recurr_year == '' && $recurr_month !== '' && $recurr_day !== ''){
                //count, month and day

                    $ym = $recurr_month .'-'. $recurr_day;
                    $conditions ="SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurcount LIKE '" . $recurr_count . "' AND recurstartdate LIKE '%" . $ym . "%' ";

                }


                if($recurr_count == '' && $recurr_year !== '' && $recurr_month == '' && $recurr_day == ''){
                //year only

                    $ym = $recurr_year.'-';
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurstartdate LIKE '%" . $ym . "%' ";

                }else if($recurr_count == '' && $recurr_year !== '' && $recurr_month !== '' && $recurr_day !== ''){
                //year, month and day

                    $ym = $recurr_year .'-'. $recurr_month .'-'. $recurr_day;
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurstartdate LIKE '%" . $ym . "%' ";

                }else if($recurr_count == '' && $recurr_year == '' && $recurr_month !== '' && $recurr_day !== ''){
                //month and day

                    $ym = $recurr_month .'-'. $recurr_day;
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurstartdate LIKE '%" . $ym . "%' ";

                }else if($recurr_count == '' && $recurr_year == '' && $recurr_month !== '' && $recurr_day == ''){
                //month only

                    $ym = '-'.$recurr_month.'-';                
                    $conditions = "SELECT * FROM donationlog LEFT JOIN members ON donationlog.useremail = members.email WHERE recurstartdate LIKE '%" . $ym . "%' ";

                }

            }
        }


        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult, 
            'index' => $page, 
            'total_items' => count($count)
            ));

    }

    public function donationlistothersAction($num, $page, $keyword, $sort, $sortto, $listIn) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {

            $conditions = "SELECT * FROM donationlogothers ";            

        } else {

            $conditions = "SELECT * FROM donationlogothers WHERE useremail LIKE '%" . $keyword . "%' OR transactionId LIKE '" . $keyword . "'";
            $conditions .= " OR billinginfofname LIKE '%" . $keyword . "%' OR billinginfolname LIKE '%" . $keyword . "%' OR paymentmode LIKE '%" . $keyword . "%'";
            $conditions .= " OR amount LIKE '" . $keyword . "' OR datetimestamp LIKE '%" . $keyword . "%' OR donatedto LIKE '" . $keyword . "' ";
            
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult, 
            'index' => $page, 
            'total_items' => count($count)
            ));
        
    }

    public function donationlisthoustonAction($num, $page, $keyword, $sort, $sortto, $listIn) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {
            
            $conditions = "SELECT * FROM donationloghouston ";

        } else {
            $conditions = "SELECT * FROM donationloghouston WHERE useremail LIKE '%" . $keyword . "%' OR transactionId LIKE '" . $keyword . "'";
            $conditions .=" OR billinginfofname LIKE '%" . $keyword . "%' OR billinginfolname LIKE '%" . $keyword . "%' OR paymentmode LIKE '%" . $keyword . "%'";
            $conditions .=" OR amount LIKE '" . $keyword . "' OR datetimestamp LIKE '%" . $keyword . "%' OR donatedto LIKE '" . $keyword . "'";
            $conditions .=" OR howdidyoulearn LIKE '%". $keyword ."%' OR cname LIKE '%". $keyword ."%' ";
            
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult, 
            'index' => $page, 
            'total_items' => count($count)
            ));

    }

    public function donationlistseattleAction($num, $page, $keyword, $sort, $sortto, $listIn) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {
            
            $conditions = "SELECT * FROM donationlogseattle ";            

        } else {
            $conditions = "SELECT * FROM donationlogseattle WHERE useremail LIKE '%" . $keyword . "%' OR transactionId LIKE '" . $keyword . "'";
            $conditions .= " OR billinginfofname LIKE '%" . $keyword . "%' OR billinginfolname LIKE '%" . $keyword . "%' OR paymentmode LIKE '%" . $keyword . "%'";
            $conditions .= " OR amount LIKE '" . $keyword . "' OR datetimestamp LIKE '%" . $keyword . "%' OR donatedto LIKE '" . $keyword . "'";
            $conditions .= " OR howdidyoulearn LIKE '%". $keyword ."%' OR cname LIKE '%". $keyword ."%' ";
            
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult, 
            'index' => $page, 
            'total_items' => count($count)
            ));       

    }

    public function donationlistnyAction($num, $page, $keyword, $sort, $sortto, $listIn) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {

            $conditions = "SELECT * FROM donationlogothers1 ";
            
        } else {

            $conditions = "SELECT * FROM donationlogothers1 WHERE useremail LIKE '%" . $keyword . "%' OR transactionId LIKE '" . $keyword . "'";
            $conditions .= " OR billinginfofname LIKE '%" . $keyword . "%' OR billinginfolname LIKE '%" . $keyword . "%' OR paymentmode LIKE '%" . $keyword . "%'";
            $conditions .= " OR amount LIKE '" . $keyword . "' OR datetimestamp LIKE '%" . $keyword . "%' OR donatedto LIKE '" . $keyword . "'";
            $conditions .= " OR howdidyoulearn LIKE '%". $keyword ."%' OR cname LIKE '%". $keyword ."%' ";
            
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult, 
            'index' => $page, 
            'total_items' => count($count)
            ));

    }

   
    public function deletedonorAction($pageid) {
        $conditions = "id=" . $pageid;
        $page = Donationlog::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if($page->delete()){
                $data = array('success' => 'Donor Deleted');
            }
        }
        echo json_encode($data);
    }

    public function deleteotherdonorAction($pageid) {
        $conditions = "id=" . $pageid;
        $page = Donationlogothers::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if($page->delete()){
                $data = array('success' => 'Donor Deleted');
            }
        }
        echo json_encode($data);
    }

    public function deleteotherdonor1Action($pageid) {
        $conditions = "id=" . $pageid;
        $page = Donationlogothers1::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if($page->delete()){
                $data = array('success' => 'Donor Deleted');
            }
        }
        echo json_encode($data);
    }

    public function deletehoustondonorAction($pageid) {
        $conditions = "id=" . $pageid;
        $page = Donationloghouston::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if($page->delete()){
                $data = array('success' => 'Donor Deleted');
            }
        }
        echo json_encode($data);
    }

    public function deleteseattledonorAction($pageid) {
        $conditions = "id=" . $pageid;
        $page = Donationlogseattle::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if($page->delete()){
                $data = array('success' => 'Donor Deleted');
            }
        }
        echo json_encode($data);
    }

    

    public static function donatecreditcardAction(){
        $dc = new CB();
        //var_dump(sprintf("%02d", $_POST['expiremonth']['val']).substr( $_POST['expireyear']['val'], -2 ));
        // $post_url = "https://test.authorize.net/gateway/transact.dll";
        $post_url = $dc->config->authorized->post_url;
        $loginname=$dc->config->authorized->apilogin;
        $transactionkey=$dc->config->authorized->transactionkey;
        $host = $dc->config->authorized->apiurlHost;
        $path = $dc->config->authorized->apiurlVer;

        $donator = Members::findFirst('email="'.$_POST['email'].'"');
        // if($donator){
        //             $donatorfName = $donator->billname;
        //             $donatorlName = ''; //$donator->lastname;
        //             $zipcode = $donator->zipcode;
        //             $location = $donator->location;
        // }else{
                    $donatorfName = $_POST['billingfname'];
                    $donatorlName = $_POST['billinglname'];
                    $zipcode = $_POST['zipcode'];
                    $location = $_POST['al1'];
        //}
            if($_POST['recurpayment'] == 'true'){
                function parse_return($content)
                {
                    $refId = substring_between($content,'<refId>','</refId>');
                    $resultCode = substring_between($content,'<resultCode>','</resultCode>');
                    $code = substring_between($content,'<code>','</code>');
                    $text = substring_between($content,'<text>','</text>');
                    $subscriptionId = substring_between($content,'<subscriptionId>','</subscriptionId>');
                    return array ('refif' => $refId, 'resultcode' =>$resultCode, "code" => $code, "text" => $text, "subscriptionid" => $subscriptionId);
                }
                function substring_between($haystack,$start,$end)
                {
                    if (strpos($haystack,$start) === false || strpos($haystack,$end) === false)
                    {
                        return false;
                    }
                    else
                    {
                        $start_position = strpos($haystack,$start)+strlen($start);
                        $end_position = strpos($haystack,$end);
                        return substr($haystack,$start_position,$end_position-$start_position);
                    }
                }


                    $creditTransactId = uniqid();

                    $content =
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
                    "<ARBCreateSubscriptionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
                    "<merchantAuthentication>".
                    "<name>" . $loginname . "</name>".
                    "<transactionKey>" . $transactionkey . "</transactionKey>".
                    "</merchantAuthentication>".
                    "<refId>" . $creditTransactId . "</refId>".
                    "<subscription>".
                    "<name>Earth Citizen Organization Recur Donation</name>".
                    "<paymentSchedule>".
                    "<interval>".
                    "<length>". $_POST['reccur_length'] ."</length>".
                    "<unit>". $_POST['reccur_unit']['name'] ."</unit>".
                    "</interval>".
                    "<startDate>" . date('Y') .'-'.sprintf("%02d", date('m')). '-'.sprintf("%02d", date('d')) . "</startDate>".
                    "<totalOccurrences>". $_POST['reccur_count'] . "</totalOccurrences>".
                    "</paymentSchedule>".
                    "<amount>". $_POST['amount'] ."</amount>".
                    "<payment>".
                    "<creditCard>".
                    "<cardNumber>" . $_POST['ccn'] . "</cardNumber>".
                    "<expirationDate>" . sprintf("%02d", $_POST['expiremonth']).substr( $_POST['expireyear'], -2 ) . "</expirationDate>".
                    "</creditCard>".
                    "</payment>".
                    "<order>".
                    "<invoiceNumber>".$creditTransactId."</invoiceNumber>".
                    "<description>Earth Citizens Reoccuring Donation through Credit Card</description>".
                    "</order>".
                    "<customer>".
                    "<email>".$_POST['email']."</email>".
                    "</customer>".
                    "<billTo>".
                    "<firstName>". $donatorfName . "</firstName>".
                    "<lastName>" . $donatorlName . "</lastName>";

                        $content .= "<address>".$_POST['al1'] . $_POST['al2']."</address>";
                        $content .= "<city>".$_POST['city']."</city>";
                        $content .= "<state>".$_POST['state']."</state>";
                        $content .= "<zip>".$_POST['zip']."</zip>";
                        $content .= "<country>".$_POST['country']."</country>";


                    $content .=
                    "</billTo>".
                    "</subscription>".
                    "</ARBCreateSubscriptionRequest>";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://" . $host . $path);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
                    curl_setopt($ch, CURLOPT_HEADER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    $response = curl_exec($ch);
                    $res = parse_return($response);
                    if ($response)
                    {

                        if($res['code']=='I00001'){
                            $donate = new Donationlog();

                            $donate->assign(array(
                                'useremail' => $_POST['email'],
                                'transactionId' => $creditTransactId,
                                'datetimestamp' => date("Y-m-d H:i:s"),
                                'amount' => $_POST['amount'],
                                'paymentmode' => 'CreditCard',
                                'billinginfofname' => $donatorfName,
                                'billinginfolname' => $donatorlName,
                                'recurcount' => $_POST['reccur_count'],
                                'recurduration' => $_POST['reccur_length'],
                                'recurtype' => $_POST['reccur_unit']['name'],
                                'recurstartdate' => date('Y') .'-'.sprintf("%02d", date('m')). '-'.sprintf("%02d", date('d')),
                                'lastccba' => substr($_POST['ccn'], -4)
                                ));

                            if($donate->save()){
                                $dc = new CB();
                                $json = json_encode(array(
                                    'From' => $dc->config->postmark->signature,
                                    'To' => $_POST['email'],
                                    'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                    'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to EARTH CITIZEN ORGANIZATIONS. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: CreditCard <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                                    ));

                                $ch2 = curl_init();
                                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                                curl_setopt($ch2, CURLOPT_POST, true);
                                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                    'Accept: application/json',
                                    'Content-Type: application/json',
                                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                                    ));
                                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                                $response = json_decode(curl_exec($ch2), true);
                                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                                curl_close($ch2);

                                if ($http_code!=200) {
                                    $data = array('error' => $mail->ErrorInfo);
                                } else {
                                    $data = array('success' => 'success');
                                }
                            }
                        }
                        echo json_encode($res);
                    }
            }else{
                $creditTransactId = uniqid();

                $post_values = array(

                    // the API Login ID and Transaction Key must be replaced with valid values
                    "x_login"			=> $loginname,
                    "x_tran_key"		=> $transactionkey,
                    "x_first_name" => $donatorfName,
                    "x_last_name" => $donatorlName,

                    "x_version"			=> "3.1",
                    "x_delim_data"		=> "TRUE",
                    "x_delim_char"		=> "|",
                    "x_relay_response"	=> "FALSE",
                    "x_invoice_num"     => $creditTransactId,
                    "x_type"			=> "AUTH_CAPTURE",
                    "x_method"			=> "CC",
                    "x_card_num"		=> $_POST['ccn'],
                    "x_exp_date"		=> sprintf("%02d", $_POST['expiremonth']).substr( $_POST['expireyear'], -2 ),

                    "x_amount"			=> $_POST['amount'],
                    "x_description"		=> "Earth Citizen Organization Donation through credit card.",

                    // Additional fields can be added here as outlined in the AIM integration
                    // guide at: http://developer.authorize.net
                    );

                    $post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
                    $post_values["x_city"] = $_POST['city'];
                    $post_values["x_state"] = $_POST['state'];
                    $post_values["x_zip"] = $_POST['zip'];
                    $post_values["x_country"] = $_POST['country'];

                // This section takes the input fields and converts them to the proper format
                // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
                $post_string = "";
                foreach( $post_values as $key => $value )
                    { $post_string .= "$key=" . urlencode( $value ) . "&"; }
                $post_string = rtrim( $post_string, "& " );

                // The following section provides an example of how to add line item details to
                // the post string.  Because line items may consist of multiple values with the
                // same key/name, they cannot be simply added into the above array.
                //
                // This section is commented out by default.
                /*
                $line_items = array(
                    "item1<|>golf balls<|><|>2<|>18.95<|>Y",
                    "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
                    "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

                foreach( $line_items as $value )
                    { $post_string .= "&x_line_item=" . urlencode( $value ); }
                */

                // This sample code uses the CURL library for php to establish a connection,
                // submit the post, and record the response.
                // If you receive an error, you may want to ensure that you have the curl
                // library enabled in your php configuration
                $request = curl_init($post_url); // initiate curl object
                curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
                curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
                curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
                curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
                $post_response = curl_exec($request); // execute curl post and store results in $post_response
                // additional options may be required depending upon your server configuration
                // you can find documentation on curl options at http://www.php.net/curl_setopt
                curl_close ($request); // close curl object

                // This line takes the response and breaks it into an array using the specified delimiting character
                $response_array = explode($post_values["x_delim_char"],$post_response);

                // The results are output to the screen in the form of an html numbered list.
                if($response_array[0]==1){

                        $donate = new Donationlog();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'CreditCard',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'lastccba' => substr($_POST['ccn'], -4)
                            ));
                        if($donate->save()){
                                $dc = new CB();
                                $json = json_encode(array(
                                    'From' => $dc->config->postmark->signature,
                                    'To' => $_POST['email'],
                                    'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                    'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to EARTH CITIZEN ORGANIZATIONS. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: CreditCard <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                                    ));

                                $ch2 = curl_init();
                                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                                curl_setopt($ch2, CURLOPT_POST, true);
                                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                    'Accept: application/json',
                                    'Content-Type: application/json',
                                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                                    ));
                                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                                $response = json_decode(curl_exec($ch2), true);
                                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                                curl_close($ch2);

                                if ($http_code!=200) {
                                    $data = array('error' => $mail->ErrorInfo);
                                } else {
                                    $data = array('success' => 'success');
                                }
                            }
                    echo json_encode(array('success'=>'Success.', 'code' => $response_array));
                }else{
                    echo json_encode(array(
                            'error'=>'Something went wrong in processing your transaction. Please check you Credit Card or verify your provider.',
                            'code' => $response_array
                            ));
                }
            }

    }

    public static function echeckdonateAction(){
        
        $dc = new CB();

        $post_url = $dc->config->authorized->post_url;
        $loginname=$dc->config->authorized->apilogin;
        $transactionkey=$dc->config->authorized->transactionkey;
        $host = $dc->config->authorized->apiurlHost;
        $path = $dc->config->authorized->apiurlVer;

        $donator = Members::findFirst('email="'.$_POST['email'].'"');
        
                    $donatorfName = $_POST['billingfname'];
                    $donatorlName = $_POST['billinglname'];
                    $zipcode = $_POST['zipcode'];
                    $location = $_POST['al1'];
        
            if($_POST['recurpayment2'] == 'true'){
                function parse_return($content)
                {
                    $refId = substring_between($content,'<refId>','</refId>');
                    $resultCode = substring_between($content,'<resultCode>','</resultCode>');
                    $code = substring_between($content,'<code>','</code>');
                    $text = substring_between($content,'<text>','</text>');
                    $subscriptionId = substring_between($content,'<subscriptionId>','</subscriptionId>');
                    return array ('refif' => $refId, 'resultcode' =>$resultCode, "code" => $code, "text" => $text, "subscriptionid" => $subscriptionId);
                }
                function substring_between($haystack,$start,$end)
                {
                    if (strpos($haystack,$start) === false || strpos($haystack,$end) === false)
                    {
                        return false;
                    }
                    else
                    {
                        $start_position = strpos($haystack,$start)+strlen($start);
                        $end_position = strpos($haystack,$end);
                        return substr($haystack,$start_position,$end_position-$start_position);
                    }
                }

                    $creditTransactId = uniqid();

                    $at = '';
                    if($_POST['at'] == 'CHECKING'){
                        $at = 'checking';
                    }elseif($_POST['at'] == 'BUSINESSCHECKING'){
                        $at = 'businessChecking';
                    }elseif($_POST['at'] == 'SAVINGS'){
                        $at = 'savings';
                    }

                    $content =
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
                    "<ARBCreateSubscriptionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
                    "<merchantAuthentication>".
                    "<name>" . $loginname . "</name>".
                    "<transactionKey>" . $transactionkey . "</transactionKey>".
                    "</merchantAuthentication>".
                    "<refId>" . $creditTransactId . "</refId>".
                    "<subscription>".
                    "<name>Earth Citizen Organization Recur Donation</name>".
                    "<paymentSchedule>".
                    "<interval>".
                    "<length>". $_POST['reccur_length'] ."</length>".
                    "<unit>". $_POST['reccur_unit']['name'] ."</unit>".
                    "</interval>".
                    "<startDate>" . date('Y') .'-'.sprintf("%02d", date('m')). '-'.sprintf("%02d", date('d')) . "</startDate>".
                    "<totalOccurrences>". $_POST['reccur_count'] . "</totalOccurrences>".
                    "</paymentSchedule>".
                    "<amount>". $_POST['amount'] ."</amount>".
                    "<payment>".
                    "<bankAccount>".
                    "<accountType>" . $at. "</accountType>".
                    "<routingNumber>" . $_POST['bankrouting'] . "</routingNumber>".
                    "<accountNumber>" . $_POST['bankaccountnumber'] . "</accountNumber>".
                    "<nameOnAccount>" . $_POST['accountname'] . "</nameOnAccount>";

                    if($_POST['at'] == 'BUSINESSCHECKING'){
                        $content .= "<echeckType>CCD</echeckType>";
                    }else{
                        $content .= "<echeckType>WEB</echeckType>";
                    }

                    $content .= "<bankName>" . $_POST['bankname'] . "</bankName>".
                    "</bankAccount>".
                    "</payment>".
                    "<order>".
                    "<invoiceNumber>".$creditTransactId."</invoiceNumber>".
                    "<description>Earth Citizens Reoccuring Donation through E-Check</description>".
                    "</order>".
                    "<customer>".
                    "<email>".$_POST['email']."</email>".
                    "</customer>".
                    "<billTo>".
                    "<firstName>". $donatorfName . "</firstName>".
                    "<lastName>" . $donatorlName . "</lastName>";

                        $content .= "<address>".$_POST['al1'] . $_POST['al2']."</address>";
                        $content .= "<city>".$_POST['city']."</city>";
                        $content .= "<state>".$_POST['state']."</state>";
                        $content .= "<zip>".$_POST['zip']."</zip>";
                        $content .= "<country>".$_POST['country']."</country>";

                    $content .=
                    "</billTo>".
                    "</subscription>".
                    "</ARBCreateSubscriptionRequest>";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://" . $host . $path);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
                    curl_setopt($ch, CURLOPT_HEADER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                    $response = curl_exec($ch);
                    $res = parse_return($response);
                    if ($response)
                    {
                        if($res['code']=='I00001'){
                            $donate = new Donationlog();

                            $donate->assign(array(
                                'useremail' => $_POST['email'],
                                'transactionId' => $creditTransactId,
                                'datetimestamp' => date("Y-m-d H:i:s"),
                                'amount' => $_POST['amount'],
                                'paymentmode' => 'eCheck',
                                'billinginfofname' => $donatorfName,
                                'billinginfolname' => $donatorlName,
                                'recurcount' => $_POST['reccur_count'],
                                'recurduration' => $_POST['reccur_length'],
                                'recurtype' => $_POST['reccur_unit']['name'],
                                'recurstartdate' => date('Y') .'-'.sprintf("%02d", date('m')). '-'.sprintf("%02d", date('d')),
                                'lastccba' => substr($_POST['bankaccountnumber'], -4)
                                ));

                            if($donate->save($res)){
                                $dc = new CB();
                                $json = json_encode(array(
                                    'From' => $dc->config->postmark->signature,
                                    'To' => $_POST['email'],
                                    'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                    'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to EARTH CITIZEN ORGANIZATIONS. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: eCheck <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                                    ));

                                $ch2 = curl_init();
                                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                                curl_setopt($ch2, CURLOPT_POST, true);
                                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                    'Accept: application/json',
                                    'Content-Type: application/json',
                                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                                    ));
                                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                                $response = json_decode(curl_exec($ch2), true);
                                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                                curl_close($ch2);

                                if ($http_code!=200) {
                                    $data = array('error' => $mail->ErrorInfo);
                                } else {
                                    $data = array('success' => 'success');
                                }
                            }
                        }
                        echo json_encode(parse_return($response));
                    }
            }else{

                $creditTransactId = uniqid();
                $post_values = array(

                    // the API Login ID and Transaction Key must be replaced with valid values
                    "x_login"           => $loginname,
                    "x_tran_key"        => $transactionkey,
                    "x_first_name" => $donatorfName,
                    "x_last_name" => $donatorlName,

                    "x_version"         => "3.1",
                    "x_delim_data"      => "TRUE",
                    "x_delim_char"      => "|",
                    "x_relay_response"  => "FALSE",
                    "x_invoice_num"     => $creditTransactId,

                    "x_method"          => "ECHECK",
                    "x_bank_aba_code"   => $_POST['bankrouting'],
                    "x_bank_acct_num"   => $_POST['bankaccountnumber'],
                    "x_bank_acct_type"  => $_POST['at'],
                    "x_bank_name"       => $_POST['bankname'],
                    "x_bank_acct_name"  => $_POST['accountname'],


                    "x_amount"          => $_POST['amount'],
                    "x_description"     => "Earth Citizen Organization Donation through E-Check",

                    // Additional fields can be added here as outlined in the AIM integration
                    // guide at: http://developer.authorize.net
                    );

                    if($_POST['at'] == 'BUSINESSCHECKING'){
                        $post_values["x_echeck_type"] = 'CCD';
                    }else{
                        $post_values["x_echeck_type"] = 'WEB';
                    }

                    $post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
                    $post_values["x_city"] = $_POST['city'];
                    $post_values["x_state"] = $_POST['state'];
                    $post_values["x_zip"] = $_POST['zip'];
                    $post_values["x_country"] = $_POST['country']['name'];

                // This section takes the input fields and converts them to the proper format
                // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
                $post_string = "";
                foreach( $post_values as $key => $value )
                    { $post_string .= "$key=" . urlencode( $value ) . "&"; }
                $post_string = rtrim( $post_string, "& " );

                // The following section provides an example of how to add line item details to
                // the post string.  Because line items may consist of multiple values with the
                // same key/name, they cannot be simply added into the above array.
                //
                // This section is commented out by default.
                /*
                $line_items = array(
                    "item1<|>golf balls<|><|>2<|>18.95<|>Y",
                    "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
                    "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

                foreach( $line_items as $value )
                    { $post_string .= "&x_line_item=" . urlencode( $value ); }
                */

                // This sample code uses the CURL library for php to establish a connection,
                // submit the post, and record the response.
                // If you receive an error, you may want to ensure that you have the curl
                // library enabled in your php configuration
                $request = curl_init($post_url); // initiate curl object
                curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
                curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
                curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
                curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
                $post_response = curl_exec($request); // execute curl post and store results in $post_response
                // additional options may be required depending upon your server configuration
                // you can find documentation on curl options at http://www.php.net/curl_setopt
                curl_close ($request); // close curl object

                // This line takes the response and breaks it into an array using the specified delimiting character
                $response_array = explode($post_values["x_delim_char"],$post_response);

                // The results are output to the screen in the form of an html numbered list.
                if($response_array[0]==1){

                        $donate = new Donationlog();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'eCheck',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'lastccba' => substr($_POST['bankaccountnumber'], -4)
                            ));
                        if($donate->save()){
                                $dc = new CB();
                                $json = json_encode(array(
                                    'From' => $dc->config->postmark->signature,
                                    'To' => $_POST['email'],
                                    'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                    'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to EARTH CITIZEN ORGANIZATIONS. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: eCheck <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                                    ));

                                $ch2 = curl_init();
                                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                                curl_setopt($ch2, CURLOPT_POST, true);
                                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                    'Accept: application/json',
                                    'Content-Type: application/json',
                                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                                    ));
                                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                                $response = json_decode(curl_exec($ch2), true);
                                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                                curl_close($ch2);

                                if ($http_code!=200) {
                                    $data = array('error' => $mail->ErrorInfo);
                                } else {
                                    $data = array('success' => 'success');
                                }
                            }
                    echo json_encode(array('success'=>$response_array[3], 'code'=>$response_array));
                }else{
                    echo json_encode(array(
                            'error'=>$response_array[3],
                            'code' => $response_array
                            ));
                }
            }

    }

    public static function donatecreditcardotherAction(){

        $dc = new CB();
        //var_dump(sprintf("%02d", $_POST['expiremonth']['val']).substr( $_POST['expireyear']['val'], -2 ));
        // $post_url = "https://test.authorize.net/gateway/transact.dll";
        $post_url = $dc->config->authorized->post_url;
        $loginname=$dc->config->authorized->apilogin;
        $transactionkey=$dc->config->authorized->transactionkey;
        $host = $dc->config->authorized->apiurlHost;
        $path = $dc->config->authorized->apiurlVer;

        $donatorfName = $_POST['billingfname'];
        $donatorlName = $_POST['billinglname'];
        $zipcode = $_POST['zipcode'];
        $location = $_POST['al1'];



            $creditTransactId = uniqid();

            $post_values = array(

                // the API Login ID and Transaction Key must be replaced with valid values
                "x_login"			=> $loginname,
                "x_tran_key"		=> $transactionkey,
                "x_first_name" => $donatorfName,
                "x_last_name" => $donatorlName,

                "x_version"			=> "3.1",
                "x_delim_data"		=> "TRUE",
                "x_delim_char"		=> "|",
                "x_relay_response"	=> "FALSE",
                "x_invoice_num"     => $creditTransactId,
                "x_type"			=> "AUTH_CAPTURE",
                "x_method"			=> "CC",
                "x_card_num"		=> $_POST['ccn'],
                "x_exp_date"		=> sprintf("%02d", $_POST['expiremonth']).substr( $_POST['expireyear'], -2 ),

                "x_amount"			=> $_POST['amount'],
                "x_description"		=> "Donation through credit card.",

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
            );

            if(isset($_POST['target'])){
                if($_POST['target']=='other'){
                    $post_values["x_description"] = 'Other Donation through credit card.';
                }else if ($_POST['target']=='nepal'){
                    $post_values["x_description"] = 'ECO Nepal Relief Donation through credit card.';
                }else if ($_POST['target']=='houston'){
                    $post_values["x_description"] = 'Earth Citizens Walk in Houston Donation through credit card.';
                }
            }

            $post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
            $post_values["x_city"] = $_POST['city'];
            $post_values["x_state"] = $_POST['state'];
            $post_values["x_zip"] = $_POST['zip'];
            $post_values["x_country"] = $_POST['country'];

            // This section takes the input fields and converts them to the proper format
            // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
            $post_string = "";
            foreach( $post_values as $key => $value )
            { $post_string .= "$key=" . urlencode( $value ) . "&"; }
            $post_string = rtrim( $post_string, "& " );

            // The following section provides an example of how to add line item details to
            // the post string.  Because line items may consist of multiple values with the
            // same key/name, they cannot be simply added into the above array.
            //
            // This section is commented out by default.
            /*
            $line_items = array(
                "item1<|>golf balls<|><|>2<|>18.95<|>Y",
                "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
                "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

            foreach( $line_items as $value )
                { $post_string .= "&x_line_item=" . urlencode( $value ); }
            */

            // This sample code uses the CURL library for php to establish a connection,
            // submit the post, and record the response.
            // If you receive an error, you may want to ensure that you have the curl
            // library enabled in your php configuration
            $request = curl_init($post_url); // initiate curl object
            curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
            $post_response = curl_exec($request); // execute curl post and store results in $post_response
            // additional options may be required depending upon your server configuration
            // you can find documentation on curl options at http://www.php.net/curl_setopt
            curl_close ($request); // close curl object

            // This line takes the response and breaks it into an array using the specified delimiting character
            $response_array = explode($post_values["x_delim_char"],$post_response);

            // The results are output to the screen in the form of an html numbered list.
            if($response_array[0]==1){
                if(isset($_POST['target'])){
                    if($_POST['target']=='other'){
                        $donate = new Donationlogothers1();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'CreditCard',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => 'Other',
                            'lastccba' => substr($_POST['ccn'], -4),
                            'howdidyoulearn' => $_POST['howdidyoulearn'],
                            'cname' => $_POST['cname']
                        ));
                        if($donate->save()){
                            $dc = new CB();
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br> <br> Thank you for your contribution to NY Citizens Walk. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: CreditCard <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                            ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                            }
                        }




                    }else if ($_POST['target']=='nepal'){
                        $donate = new Donationlogothers();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'CreditCard',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => 'Nepal',
                            'lastccba' => substr($_POST['ccn'], -4)
                        ));
                        $donate->save();
                    }else if ($_POST['target']=='houston'){
                        $donate = new Donationloghouston();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'CreditCard',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => 'Earth Citizens Walk in Houston',
                            'lastccba' => substr($_POST['ccn'], -4),
                            'howdidyoulearn' => $_POST['howdidyoulearn'],
                            'cname' => $_POST['cname']
                        ));
                        if($donate->save()){
                            $dc = new CB();
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br> <br> Thank you for your contribution to Houston Citizens Walk. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: CreditCard <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                            ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                            }
                        }



                    }else if ($_POST['target']=='seattle'){
                        $donate = new Donationlogseattle();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'CreditCard',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => 'Natural Healing Expo 2015 in SEATTLE',
                            'lastccba' => substr($_POST['ccn'], -4),
                            'howdidyoulearn' => $_POST['howdidyoulearn'],
                            'cname' => $_POST['cname']
                        ));
                        if($donate->save()){
                            $dc = new CB();
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to Natural Healing Expo 2015 in SEATTLE, WA. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: CreditCard <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                            ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                            }
                        }



                    }else if ($_POST['target']=='events'){
                        $donate = new Eventslog();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'CreditCard',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => $_POST['donatedto'],
                            'lastccba' => substr($_POST['ccn'], -4),
                            'howdidyoulearn' => $_POST['howdidyoulearn'],
                            'cname' => $_POST['cname']
                        ));
                        if($donate->save()){
                            
                            $conditions = 'eventID="'.$_POST['donatedto'].'"';
                            $event = Eventcontents::findFirst($conditions);

                            $pay_type = 'CreditCard';
                            $name = $donatorfName." ".$donatorlName;

                            $dc = new CB();
                            $content = $dc->ReceiptTemplate($creditTransactId,date("Y-m-d H:i:s"),$event->title,$event->recieptContent,$name,$_POST['amount'],$pay_type);
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => $event->recieptSubject,
                                'HtmlBody' => $content
                                ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                            }
                        }



                    }
                }

                echo json_encode(array('success'=>'Success.', 'code' => $response_array));
            }else {
                echo json_encode(array(
                    'error' => 'Something went wrong in processing your transaction. Please check you Credit Card or verify your provider.',
                    'code' => $response_array
                ));
            }
    }

    public static function echeckdonateotherAction(){
        //var_dump(sprintf("%02d", $_POST['expiremonth']['val']).substr( $_POST['expireyear']['val'], -2 ));

        $dc = new CB();
        // $post_url = "https://test.authorize.net/gateway/transact.dll";
        $post_url = $dc->config->authorized->post_url;
        $loginname=$dc->config->authorized->apilogin;
        $transactionkey=$dc->config->authorized->transactionkey;
        $host = $dc->config->authorized->apiurlHost;
        $path = $dc->config->authorized->apiurlVer;

        $donatorfName = $_POST['billingfname'];
        $donatorlName = $_POST['billinglname'];
        $zipcode = $_POST['zipcode'];
        $location = $_POST['al1'];


            $creditTransactId = uniqid();
            $post_values = array(

                // the API Login ID and Transaction Key must be replaced with valid values
                "x_login"           => $loginname,
                "x_tran_key"        => $transactionkey,
                "x_first_name"      => $donatorfName,
                "x_last_name"       => $donatorlName,

                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",
                "x_invoice_num"     => $creditTransactId,

                "x_method"          => "ECHECK",
                "x_bank_aba_code"   => $_POST['bankrouting'],
                "x_bank_acct_num"   => $_POST['bankaccountnumber'],
                "x_bank_acct_type"  => $_POST['at'],
                "x_bank_name"       => $_POST['bankname'],
                "x_bank_acct_name"  => $_POST['accountname'],


                "x_amount"          => $_POST['amount'],
                "x_description"     => "Donation through E-Check.",

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
            );

            if($_POST['at'] == 'BUSINESSCHECKING'){
                $post_values["x_echeck_type"] = 'CCD';
            }else{
                $post_values["x_echeck_type"] = 'WEB';
            }

            if(isset($_POST['target'])){
                if($_POST['target']=='other'){
                    $post_values["x_description"] = 'Other Donation through credit card.';
                }elseif ($_POST['target']=='nepal'){
                    $post_values["x_description"] = 'ECO Nepal Relief Donation through credit card.';
                }elseif ($_POST['target']=='houston'){
                    $post_values["x_description"] = 'Earth Citizens Walk in Houston Donation through E-Check.';
                }
            }

            $post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
            $post_values["x_city"] = $_POST['city'];
            $post_values["x_state"] = $_POST['state'];
            $post_values["x_zip"] = $_POST['zip'];
            $post_values["x_country"] = $_POST['country']['name'];

            // This section takes the input fields and converts them to the proper format
            // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
            $post_string = "";
            foreach( $post_values as $key => $value )
            { $post_string .= "$key=" . urlencode( $value ) . "&"; }
            $post_string = rtrim( $post_string, "& " );

            // The following section provides an example of how to add line item details to
            // the post string.  Because line items may consist of multiple values with the
            // same key/name, they cannot be simply added into the above array.
            //
            // This section is commented out by default.
            /*
            $line_items = array(
                "item1<|>golf balls<|><|>2<|>18.95<|>Y",
                "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
                "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

            foreach( $line_items as $value )
                { $post_string .= "&x_line_item=" . urlencode( $value ); }
            */

            // This sample code uses the CURL library for php to establish a connection,
            // submit the post, and record the response.
            // If you receive an error, you may want to ensure that you have the curl
            // library enabled in your php configuration
            $request = curl_init($post_url); // initiate curl object
            curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
            $post_response = curl_exec($request); // execute curl post and store results in $post_response
            // additional options may be required depending upon your server configuration
            // you can find documentation on curl options at http://www.php.net/curl_setopt
            curl_close ($request); // close curl object

            // This line takes the response and breaks it into an array using the specified delimiting character
            $response_array = explode($post_values["x_delim_char"],$post_response);

            // The results are output to the screen in the form of an html numbered list.
            if($response_array[0]==1){
                if(isset($_POST['target'])){
                    if($_POST['target']=='other'){
                        $donate = new Donationlogothers1();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'eCheck',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => 'other',
                            'lastccba' => substr($_POST['bankaccountnumber'], -4),
                            'howdidyoulearn' => $_POST['howdidyoulearn'],
                            'cname' => $_POST['cname']
                        ));

                        if($donate->save()){
                            $dc = new CB();
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br> <br> Thank you for your contribution to NY Citizens Walk. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: e-check <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                            ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                            }
                        }



                    }elseif ($_POST['target']=='nepal'){
                        $donate = new Donationlogothers();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'eCheck',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => 'Nepal',
                            'lastccba' => substr($_POST['bankaccountnumber'], -4)
                        ));
                        $donate->save();
                    }elseif ($_POST['target']=='houston'){
                        $donate = new Donationloghouston();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'eCheck',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => 'Earth Citizens Walk in Houston',
                            'lastccba' => substr($_POST['bankaccountnumber'], -4)
                        ));
                        if($donate->save()){
                            $dc = new CB();
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br> <br> Thank you for your contribution to Houston Citizens Walk. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: e-check <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                            ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                            }
                        }
                    }elseif ($_POST['target']=='seattle'){
                        $donate = new Donationlogseattle();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'eCheck',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => 'Natural Healing Expo 2015 in SEATTLE',
                            'lastccba' => substr($_POST['bankaccountnumber'], -4)
                        ));
                        if($donate->save()){
                            $dc = new CB();
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => 'Earth Citizen Organizations Donation Receipt',
                                'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to Natural Healing Expo 2015 in SEATTLE, WA. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: e-check <br>Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                            ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                            }
                        }
                    }elseif ($_POST['target']=='events'){
                        $donate = new Eventslog();

                        $donate->assign(array(
                            'useremail' => $_POST['email'],
                            'transactionId' => $creditTransactId,
                            'datetimestamp' => date("Y-m-d H:i:s"),
                            'amount' => $_POST['amount'],
                            'paymentmode' => 'eCheck',
                            'billinginfofname' => $donatorfName,
                            'billinginfolname' => $donatorlName,
                            'donatedto' => $_POST['donatedto'],
                            'lastccba' => substr($_POST['bankaccountnumber'], -4)
                        ));
                        if($donate->save()){
                            // $dc = new CB();
                            // $json = json_encode(array(
                            //     'From' => $dc->config->postmark->signature,
                            //     'To' => $_POST['email'],
                            //     'Subject' => 'Earth Citizen Organizations Donation Receipt',
                            //     'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to '.$_POST['title'].'. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: e-check <br>Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                            // ));
                            $conditions = 'eventID="'.$_POST['donatedto'].'"';
                            $event = Eventcontents::findFirst($conditions);

                            $pay_type = 'eCheck';
                            $name = $donatorfName." ".$donatorlName;

                            $dc = new CB();
                            $content = $dc->ReceiptTemplate($creditTransactId,date("Y-m-d H:i:s"),$event->title,$event->recieptContent,$name,$_POST['amount'],$pay_type);
                            $json = json_encode(array(
                                'From' => $dc->config->postmark->signature,
                                'To' => $_POST['email'],
                                'Subject' => $event->recieptSubject,
                                'HtmlBody' => $content
                                ));

                            $ch2 = curl_init();
                            curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                            curl_setopt($ch2, CURLOPT_POST, true);
                            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json',
                                'Content-Type: application/json',
                                'X-Postmark-Server-Token: '.$dc->config->postmark->token
                            ));
                            curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                            $response = json_decode(curl_exec($ch2), true);
                            $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                            curl_close($ch2);

                            if ($http_code!=200) {
                                $data = array('error' => $mail->ErrorInfo);
                            } else {
                                $data = array('success' => 'success');
                            }
                        }
                    }
                }
                echo json_encode(array('success'=>$response_array[3], 'code'=>$post_response));
            }else{
                echo json_encode(array(
                    'error'=>$response_array[3],
                    'code' => $response_array
                ));
            }

    }

    public static function donatecreditcardworegAction(){
        if($_POST['email']){
            $dc = new CB();
        //var_dump(sprintf("%02d", $_POST['expiremonth']['val']).substr( $_POST['expireyear']['val'], -2 ));
            $post_url = "https://test.authorize.net/gateway/transact.dll";
        // $post_url = $dc->config->authorized->post_url;
            $loginname=$dc->config->authorized->apilogin;
            $transactionkey=$dc->config->authorized->transactionkey;
            $host = $dc->config->authorized->apiurlHost;
            $path = $dc->config->authorized->apiurlVer;

            $donatorfName = $_POST['billingfname'];
            $donatorlName = $_POST['billinglname'];
            $zipcode = $_POST['zipcode'];
            $location = $_POST['al1'];

            $creditTransactId = uniqid();

            $post_values = array(

                // the API Login ID and Transaction Key must be replaced with valid values
                "x_login"           => $loginname,
                "x_tran_key"        => $transactionkey,
                "x_first_name" => $donatorfName,
                "x_last_name" => $donatorlName,

                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",
                "x_invoice_num"     => $creditTransactId,
                "x_type"            => "AUTH_CAPTURE",
                "x_method"          => "CC",
                "x_card_num"        => $_POST['ccn'],
                "x_exp_date"        => sprintf("%02d", $_POST['expiremonth']).substr( $_POST['expireyear'], -2 ),

                "x_amount"          => $_POST['amount'],
                "x_description"     => "Earth Citizen Organization Donation through credit card.",

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
                );


                $post_values["x_description"] = 'Earth Citizen Organization Donation through credit card.';
                $post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
                $post_values["x_city"] = $_POST['city'];
                $post_values["x_state"] = $_POST['state'];
                $post_values["x_zip"] = $_POST['zip'];
                $post_values["x_country"] = $_POST['country'];

            // This section takes the input fields and converts them to the proper format
            // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
                $post_string = "";
                foreach( $post_values as $key => $value )
                    { $post_string .= "$key=" . urlencode( $value ) . "&"; }
                $post_string = rtrim( $post_string, "& " );

            // The following section provides an example of how to add line item details to
            // the post string.  Because line items may consist of multiple values with the
            // same key/name, they cannot be simply added into the above array.
            //
            // This section is commented out by default.
            /*
            $line_items = array(
                "item1<|>golf balls<|><|>2<|>18.95<|>Y",
                "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
                "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

            foreach( $line_items as $value )
                { $post_string .= "&x_line_item=" . urlencode( $value ); }
            */

            // This sample code uses the CURL library for php to establish a connection,
            // submit the post, and record the response.
            // If you receive an error, you may want to ensure that you have the curl
            // library enabled in your php configuration
            $request = curl_init($post_url); // initiate curl object
            curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
            $post_response = curl_exec($request); // execute curl post and store results in $post_response
            // additional options may be required depending upon your server configuration
            // you can find documentation on curl options at http://www.php.net/curl_setopt
            curl_close ($request); // close curl object

            // This line takes the response and breaks it into an array using the specified delimiting character
            $response_array = explode($post_values["x_delim_char"],$post_response);

            // The results are output to the screen in the form of an html numbered list.
            if($response_array[0]==1){

                $donate = new Donationlog();

                $donate->assign(array(
                    'useremail' => $_POST['email'],
                    'transactionId' => $creditTransactId,
                    'datetimestamp' => date("Y-m-d H:i:s"),
                    'amount' => $_POST['amount'],
                    'paymentmode' => 'CreditCard',
                    'billinginfofname' => $_POST['billingfname'],
                    'billinginfolname' => $_POST['billinglname'],
                    'lastccba' => substr($_POST['ccn'], -4),
                    'howdidyoulearn' => $_POST['howdidyoulearn'],
                    'cname' => $_POST['cname']
                    ));

                if($donate->save()){
                    $dc = new CB();
                    $json = json_encode(array(
                        'From' => $dc->config->postmark->signature,
                        'To' => $_POST['email'],
                        'Subject' => 'Earth Citizen Organizations Donation Receipt',
                        'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to EARTH CITIZEN ORGANIZATIONS. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: CreditCard <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                        ));

                    $ch2 = curl_init();
                    curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                    curl_setopt($ch2, CURLOPT_POST, true);
                    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                        'Accept: application/json',
                        'Content-Type: application/json',
                        'X-Postmark-Server-Token: '.$dc->config->postmark->token
                        ));
                    curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                    $response = json_decode(curl_exec($ch2), true);
                    $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                    curl_close($ch2);

                    if ($http_code!=200) {
                        $data = array('error' => $mail->ErrorInfo);
                    } else {
                        $data = array('success' => 'success');
                    }
                }



                echo json_encode(array('success'=>'Success.', 'code' => $response_array));
            }else {
                echo json_encode(array(
                    'error' => 'Something went wrong in processing your transaction. Please check you Credit Card or verify your provider.',
                    'code' => $response_array
                    ));
            }

        }else {
            echo json_encode(array(
                'error' => 'Something went wrong in processing your transaction. Please refersh the page and try again.',
                'code' => $response_array
                ));
        }
    }

    public static function echeckdonateworegAction(){
        if($_POST['email']){
            $dc = new CB();
            $post_url = "https://test.authorize.net/gateway/transact.dll";
        // $post_url = $dc->config->authorized->post_url;
            $loginname=$dc->config->authorized->apilogin;
            $transactionkey=$dc->config->authorized->transactionkey;
            $host = $dc->config->authorized->apiurlHost;
            $path = $dc->config->authorized->apiurlVer;

            $donatorfName = $_POST['billingfname'];
            $donatorlName = $_POST['billinglname'];
            $zipcode = $_POST['zipcode'];
            $location = $_POST['al1'];

            $creditTransactId = uniqid();
            $post_values = array(

                // the API Login ID and Transaction Key must be replaced with valid values
                "x_login"           => $loginname,
                "x_tran_key"        => $transactionkey,
                "x_first_name" => $donatorfName,
                "x_last_name" => $donatorlName,

                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",
                "x_invoice_num"     => $creditTransactId,

                "x_method"          => "ECHECK",
                "x_bank_aba_code"   => $_POST['bankrouting'],
                "x_bank_acct_num"   => $_POST['bankaccountnumber'],
                "x_bank_acct_type"  => $_POST['at'],
                "x_bank_name"       => $_POST['bankname'],
                "x_bank_acct_name"  => $_POST['accountname'],


                "x_amount"          => $_POST['amount'],
                "x_description"     => "Earth Citizen Organization Donation through E-Check",

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
                );

                if($_POST['at'] == 'BUSINESSCHECKING'){
                    $post_values["x_echeck_type"] = 'CCD';
                }else{
                    $post_values["x_echeck_type"] = 'WEB';
                }


                $post_values["x_description"] = 'Earth Citizen Organization Donation through E-Check';
                $post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
                $post_values["x_city"] = $_POST['city'];
                $post_values["x_state"] = $_POST['state'];
                $post_values["x_zip"] = $_POST['zip'];
                $post_values["x_country"] = $_POST['country']['name'];

            // This section takes the input fields and converts them to the proper format
            // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
                $post_string = "";
                foreach( $post_values as $key => $value )
                    { $post_string .= "$key=" . urlencode( $value ) . "&"; }
                $post_string = rtrim( $post_string, "& " );

            // The following section provides an example of how to add line item details to
            // the post string.  Because line items may consist of multiple values with the
            // same key/name, they cannot be simply added into the above array.
            //
            // This section is commented out by default.
            /*
            $line_items = array(
                "item1<|>golf balls<|><|>2<|>18.95<|>Y",
                "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
                "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

            foreach( $line_items as $value )
                { $post_string .= "&x_line_item=" . urlencode( $value ); }
            */

            // This sample code uses the CURL library for php to establish a connection,
            // submit the post, and record the response.
            // If you receive an error, you may want to ensure that you have the curl
            // library enabled in your php configuration
            $request = curl_init($post_url); // initiate curl object
            curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
            $post_response = curl_exec($request); // execute curl post and store results in $post_response
            // additional options may be required depending upon your server configuration
            // you can find documentation on curl options at http://www.php.net/curl_setopt
            curl_close ($request); // close curl object

            // This line takes the response and breaks it into an array using the specified delimiting character
            $response_array = explode($post_values["x_delim_char"],$post_response);

            // The results are output to the screen in the form of an html numbered list.
            if($response_array[0]==1){

                $donate = new Donationlog();

                $donate->assign(array(
                    'useremail' => $_POST['email'],
                    'transactionId' => $creditTransactId,
                    'datetimestamp' => date("Y-m-d H:i:s"),
                    'amount' => $_POST['amount'],
                    'paymentmode' => 'eCheck',
                    'billinginfofname' => $_POST['billingfname'],
                    'billinginfolname' => $_POST['billinglname'],
                    'lastccba' => substr($_POST['bankaccountnumber'], -4),
                    'howdidyoulearn' => $_POST['howdidyoulearn'],
                    'cname' => $_POST['cname']
                    ));

                if($donate->save()){
                    $dc = new CB();
                    $json = json_encode(array(
                        'From' => $dc->config->postmark->signature,
                        'To' => $_POST['email'],
                        'Subject' => 'Earth Citizen Organizations Donation Receipt',
                        'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for your contribution to EARTH CITIZEN ORGANIZATIONS. This letter serves as your transaction receipt. <br> <br> Here’s your contribution info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: eCheck <br> Amount: '.$_POST['amount'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                        ));

                    $ch2 = curl_init();
                    curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                    curl_setopt($ch2, CURLOPT_POST, true);
                    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                        'Accept: application/json',
                        'Content-Type: application/json',
                        'X-Postmark-Server-Token: '.$dc->config->postmark->token
                        ));
                    curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                    $response = json_decode(curl_exec($ch2), true);
                    $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                    curl_close($ch2);

                    if ($http_code!=200) {
                        $data = array('error' => $mail->ErrorInfo);
                    } else {
                        $data = array('success' => 'success');
                    }
                }

                echo json_encode(array('success'=>$response_array[3], 'code'=>$post_response));
            }else{
                echo json_encode(array(
                    'error'=>$response_array[3],
                    'code' => $response_array
                    ));
            }

        }else {
            echo json_encode(array(
                'error' => 'Something went wrong in processing your transaction. Please refersh the page and try again.',
                'code' => $response_array
                ));
        }

    }

}