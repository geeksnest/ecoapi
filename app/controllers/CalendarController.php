<?php
namespace Controllers;

use \Models\Calendar as Calendar;
use \Models\Featuredphoto as Featuredphoto;

class CalendarController extends \Phalcon\Mvc\Controller
{
    // Upload Image
    public function slideruploadAction(){
        var_dump($_POST);
    }

    public function ajaxfileuploaderAction($filename){
        $newpicname = 0;
        $getType=explode('.', $filename);
        // New Image Name
        $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);
        $generateid=md5(uniqid(rand(), true));

        if(is_file('../public/server/php/files/'.$filename)){
            rename('../public/server/php/files/'.$filename, '../public/images/featurebanner/'.$newfileName);
        }

        if(is_file('../public/server/php/files/thumbnail/'.$filename)){
            rename('../public/server/php/files/thumbnail/'.$filename, '../public/images/featurebanner/thumbnail/'.$newfileName);
        }

        $imgUpload = new Featuredphoto();
        $imgUpload->assign(array('path' => $newfileName, 'title' => 'Title Here'));
        if (!$imgUpload->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
            echo json_encode(["error" => $imgUpload->getMessages()]);
        }else{
            $getImage= Featuredphoto::find(array("order"=>"id DESC"));
            foreach ($getImage as $getImages) {;
                $data[] = array('id'=>$getImages->id, 'imgpath'=>$getImages->path, 'imgtitle'=>$getImages->title);
            }
            echo json_encode($data);
        }
    }

    // List Uploaded Images
    public function imagelistAction(){
        $getImage= Featuredphoto::find(array("order" => "id DESC"));
        foreach ($getImage as $getImages) {;
            $data[] = array(
                'id'=>$getImages->id,
                'imgpath'=>$getImages->path,
                'imgtitle'=>$getImages->title
                );
        }
        echo json_encode($data);
    }

    // Delete Uploaded Images
    public function dltphotoAction(){
        $id = $_POST['id'];
        $dltPhoto = Featuredphoto::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }

    // Delete Uploaded Images
    public function dltactphotoAction(){
        $id = $_POST['id'];
        $dltPhoto = Featuredphoto::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }

    // Insert activity data to dbase
    public function addactAction(){
        $data = array();

        $mont0 = array('Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12);
        $datef = explode(" ", $_POST['dfrom']);
        if($_POST['dto'] == ""){
            $datet = explode(" ", $_POST['dfrom']);
        }else{
            $datet = explode(" ", $_POST['dto']);
        }

        $ft = array('00' => 12, '01' => 1, '02' => 2, '03' => 3, '04' => 4, '05' => 5, '06' => 6, '07' => 7, '08' => 8, '09' => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 1, 14 => 2, 15 => 3, 16 => 4, 17 => 5, 18 => 6, 19 => 7, 20 => 8, 21 => 9, 22 => 10, 23 => 11);
        if($_POST['mytime'] == ""){
            $mytime = "";
        }else{
            $mt2 = explode(" ", $_POST['mytime']);
            $mt = explode(":", $mt2[4]);

            if($mt[0] > 12){
                $tx = 'PM';
            }else{
                $tx = 'AM';
            }
            $mytime = $ft[$mt[0]].':'.$mt[1].' '.$tx;
        }

        $datef1 = $datef[3].'-'.$mont0[$datef[1]].'-'.$datef[2];
        $datet1 = $datet[3].'-'.$mont0[$datet[1]].'-'.$datet[2];

        $start = strtotime($datef[3].'-'.$mont0[$datef[1]].'-'.$datef[2]);
        $end = strtotime($datet[3].'-'.$mont0[$datet[1]].'-'.$datet[2]);
        $days_between = ($end - $start) / 86400;

        if ($days_between >= 0){
            $acti = new Calendar();
            $acti->assign(array(
                'acttitle' => $_POST['title'],
                'actloc' => $_POST['loc'],
                'df' => $datef1,
                'dt' => $datet1,
                'mt' => $mytime,
                'actdesc' => $_POST['body'],
                'actinfo' => $_POST['shortdesc'],
                'actbanner' => $_POST['banner']
                ));

            if (!$acti->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }else{
            $data = array('ErrDate' => 'Invalid Date.');
        }
        echo json_encode($data);
    }

    // view activities
    public function viewcalendarAction() {
        $calac = Calendar::find();
        foreach ($calac as $c) {;

            $df = explode("-", $c->df);
            $dt = explode("-", $c->dt);
            $mt = explode(":", $c->mt);
            $mt2 = explode(" ", $mt[1]);
            $fm = array(
                1 => 'January', 2 => 'February',
                3 => 'March', 4 => 'April',
                5 => 'May', 6 => 'June',
                7 => 'July', 8 => 'August',
                9 => 'September', 10 => 'October',
                11 => 'November', 12 => 'December'
                );

            $data[] = array(
                'title' => $c->acttitle,
                'loc' => $c->actloc,
                'fyea' => $df[0], 'fmon' => $df[1], 'fday' => $df[2],
                'tyea' => $dt[0], 'tmon' => $dt[1], 'tday' => $dt[2],
                'fyea2' => $df[0], 'fmon2' => $fm[$df[1]], 'fday2' => $df[2],
                'tyea2' => $dt[0], 'tmon2' => $fm[$dt[1]], 'tday2' => $dt[2],
                'hhr' => $mt[0], 'hmin' => $mt2[0], 'hap' => $mt2[1],
                'body' => $c->actdesc,
                'info' => $c->actinfo,
                'banner' => $c->actbanner,
                'datef' => $c->df,
                'datet' => $c->dt
            );
        }
        echo json_encode($data);
    }

    // FRONT-END THIS
    public function listviewAction() {
        $viewcal = Calendar::find(array("order" => "df DESC"));
        $viewcalendar = json_encode($viewcal->toArray(), JSON_NUMERIC_CHECK);
        echo $viewcalendar;
    }

    // Update activity
    public function activityUpdateAction(){
        $data = array();

        $mont0 = array('Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12);
        $datef = explode(" ", $_POST['dfrom']);
        $datet = explode(" ", $_POST['dto']);

        $ft = array('00' => 12, '01' => 1, '02' => 2, '03' => 3, '04' => 4, '05' => 5, '06' => 6, '07' => 7, '08' => 8, '09' => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 1, 14 => 2, 15 => 3, 16 => 4, 17 => 5, 18 => 6, 19 => 7, 20 => 8, 21 => 9, 22 => 10, 23 => 11);
        if($_POST['mytime'] == ""){
            $mytime = "";
        }else{
            $mt2 = explode(" ", $_POST['mytime']);
            $mt = explode(":", $mt2[4]);

            if($mt[0] > 12){
                $tx = 'PM';
            }else{
                $tx = 'AM';
            }
            $mytime = $ft[$mt[0]].':'.$mt[1].' '.$tx;
        }

        $datef1 = $datef[3].'-'.$mont0[$datef[1]].'-'.$datef[2];
        $datet1 = $datet[3].'-'.$mont0[$datet[1]].'-'.$datet[2];

        $start = strtotime($datef[3].'-'.$mont0[$datef[1]].'-'.$datef[2]);
        $end = strtotime($datet[3].'-'.$mont0[$datet[1]].'-'.$datet[2]);
        $days_between = ($end - $start) / 86400;

        if ($days_between >= 0){
            $actid = $_POST['actid'];
            $ca = Calendar::findFirst("actid=" . $actid);

            $ca->acttitle = $_POST['title'];
            $ca->actloc = $_POST['loc'];
            $ca->df = $datef1;
            $ca->dt = $datet1;
            $ca->mt = $mytime;
            $ca->actdesc = $_POST['body'];
            $ca->actinfo = $_POST['shortdesc'];
            $ca->actbanner = $_POST['banner'];

            if (!$ca->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }else{
            $data = array('ErrDate' => 'Invalid Date.');
            $data = 1;
        }
        echo json_encode($data);
    }

    // view edit layout
    public function activityinfoAction($actid) {
        $ca = Calendar::findFirst("actid=" . $actid);
        $data = array();
        if ($ca) {

            $mt = explode(" ", $ca->mt);
            $mt2 = explode(":", $mt[0]);

            if ($mt[1] == 'PM') {
                $hrr = $mt2[0] + 12;
                $mytime2 = $hrr.':'.$mt2[1].':00';
            }else{
                $mytime2 = date('Y-m-d').' '.$mt[0].':00';
            }
            $df = explode("-", $ca->df);
            $dt = explode("-", $ca->dt);
            $fm = array(
                1 => 'January', 2 => 'February',
                3 => 'March', 4 => 'April',
                5 => 'May', 6 => 'June',
                7 => 'July', 8 => 'August',
                9 => 'September', 10 => 'October',
                11 => 'November', 12 => 'December'
                );

            $data = array(
                'actid' => $ca->actid,
                'title' => $ca->acttitle,
                'loc' => $ca->actloc,
                'dfrom' => $df[2].'-'.$fm[$df[1]].'-'.$df[0],
                'dfrom2' => $df[2].'-'.$fm[$df[1]].'-'.$df[0],
                'dto' => $dt[2].'-'.$fm[$dt[1]].'-'.$dt[0],
                'dto2' => $dt[2].'-'.$fm[$dt[1]].'-'.$dt[0],
                'mytime2' => $mytime2,
                'body' => $ca->actdesc,
                'shortdesc' => $ca->actinfo,
                'banner' => $ca->actbanner
                );
        }
        echo json_encode($data);
    }

    public function activitydeleteAction($actid) {
        $conditions = "actid=" . $actid;
        $page = Calendar::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if($page->delete()){
                $data = array('success' => 'Activity Deleted');
            }
        }
        echo json_encode($data);
    }
}