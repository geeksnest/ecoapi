<?php

namespace Controllers;

use \Models\Donationlog as Donationlog;
use \Models\Donation as Donation;
use \Models\Memberconfirmation as Memberconfirmation;
use MailChimp as MailChimp;
use PHPMailer as PHPMailer;
use \Models\Pages as Pages;
use \Models\Atw as Atw;
use \Models\Friends as Friends;
use \Controllers\ControllerBase as CB;

class AtwController extends \Phalcon\Mvc\Controller{
    public function createorgAction(){
        $data = array();
        if($_POST){ 
            $atw = new Atw();

            function slugify ($string) {
                $string = utf8_encode($string);
                $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);   
                $string = preg_replace('/[^a-z0-9- ]/i', '', $string);
                $string = str_replace(' ', '-', $string);
                $string = trim($string, '-');
                $string = strtolower($string);

                if (empty($string)) {
                    return 'n-a';
                }

                return $string;
            }

            $slugs = slugify($_POST['org']);

            $atw->assign(array(
                'orgname'       => $_POST['org'],
                'slugs'         => $slugs,
                'taxid'         => "",
                'syear'         => $_POST['syear'],
                'phone'         => $_POST['contactnumber'],
                'address'       => $_POST['address1'],
                'address2'      => $_POST['address2'],
                'city'          => $_POST['city'],
                'state'         => $_POST['state'],
                'country'       => $_POST['country'],
                'zip'           => $_POST['zip'],
                'statement'     => $_POST['mission'],
                'activities'    => $_POST['activities'],
                'news'          => $_POST['news'],
                'website'       => $_POST['website'],
                'facebook'      => $_POST['facebook'],
                'vid'           => $_POST['vid'],
                'email'         => $_POST['email'],
                'logo'          => $_POST['image'],
                'created_at'    => date('Y-m-d')
                ));
            if(!$atw->save()){
                $errors = array();
                foreach ($atw->getMessages() as $message){
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }else{
                $data['success'] = "Success";
                $content ='
                <!doctype html>
                <html class=no-js lang=en data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
                <head>
                    <meta charset="utf-8" />
                    <meta name=viewport content="width=device-width, initial-scale=1.0" />
                    <title>Earth Citizens | Newsletter</title>
                    <meta name=description content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more." />
                    <meta name=author content="ZURB, inc. ZURB network also includes zurb.com" />
                    <meta name=copyright content="ZURB, inc. Copyright (c) 2015" />
                    <link rel=stylesheet type=text/css href="https://earthcitizens.org/css/fr/font-awesome.css" />
                    <link rel=stylesheet type=text/css href="https://earthcitizens.org/css/fr/font-awesome.min.css" />
                </head>
                <body style="background:#fff;color:#222;padding:0;margin:0;font-family:" Helvetica Neue ", Helvetica, Roboto, Arial, sans-serif; font-weight: normal;font-style: normal;line-height: 1.5;position: relative;cursor: auto; font-size: 100%;">
                    <div style=width:100%;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0;max-width:62.5rem>
                        <div style=width:100%>
                            <div style=padding-top:25px;padding-bottom:15px;overflow:hidden>
                                <div style=width:25%;float:left>
                                    <div style=width:200px;margin-left:auto;margin-right:auto;margin-bottom:25px><img src=https://earthcitizens.org/images/template_images/ecologo1.png style=width:100%></div>
                                </div>
                            </div>
                            <hr style="border 2px solid #000" />
                            <div style=width:25%;float:left;height:35px;padding-top:15px;color:#555;font:Arial,sans-serif>Request Date: <b>'.date("Y-m-d").'</b></div>
                            <div style=width:75%;float:right>
                                <div style=overflow:hidden;width:300px;height:50px;float:right>
                                    <ul style=margin-left:auto;margin-right:auto;padding:0;list-style:none>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=https://www.facebook.com/EarthCitizensOrganization>LIKE <img style=width:20px;height:20px src=https://earthcitizens.org/images/facebook.png></a></li>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=https://twitter.com/goearthcitizens>FOLLOW <img style=width:20px;height:20px src=https://earthcitizens.org/images/twitter.png></a></li>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=#>REFER <img style=width:20px;height:20px src=https://earthcitizens.org/images/email.png></a></li>
                                    </ul>
                                </div>
                            </div>
                            <hr/>
                        </div>
                    </div>
                    <div style=width:100%;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0;max-width:62.5rem>
                        <h3>A new organization has been listed to Eco Around the world </h3>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Name of Organization:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['org'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Year of Establishment:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['syear'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Address 1:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['address1'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Address 2:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['address2'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>City:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['city'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>State:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['state'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Zip/Postal:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['zip'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Country:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['country'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Organization Mission Statement:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['mission'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Description of Activities:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['activities'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>News and Announcements:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['news'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Facebook Page:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['facebook'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Public Video:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['vid'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Contact Phone Number:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['contactnumber'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Contact Email:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['email'].'</p>
                    </div>
                </body>
                </html>
                ';
                $mail = new PHPMailer();
                $mail->isSMTP();
                $adminemail         = "infoeco@mailinator.com";
                $mail->Host         = 'smtp.mandrillapp.com';
                $mail->SMTPAuth     = true;
                $mail->Username     = 'efrenbautistajr@gmail.com';
                $mail->Password     = '6Xy18AJ15My59aQNTHE5kA';               
                $mail->Port         = 587;

                $mail->From         = 'infoeco@mailinator.com';
                $mail->FromName     = 'ECO Around the World';
                $mail->addAddress($adminemail);

                $mail->isHTML(true);
                $mail->Subject      = 'ECO Around the World Request';
                $mail->Body         = $content;

                if(!$mail->send()){
                    $data['error'] = "Email not sent";
                } else {
                    $data['email'] = "Success";
                }
            }
            echo json_encode($data);
        }
        else{
           $data['error'] = "Something went wrong saving the data, please try again.";
           echo json_encode($data);
       }
   }

   public function atwlistAction($num, $page, $keyword, $sort, $sortto, $datefrom, $dateto) {
    $app = new CB();
    $offsetfinal = ($page * 10) - 10;

    if($keyword == 'null' || $keyword == 'undefined') {
        $conditions = "SELECT * FROM atw ";
    }else{
        $conditions = "SELECT * FROM atw WHERE 
        orgname LIKE '%". $keyword ."%' 
        OR taxid LIKE '%". $keyword ."%'
        OR phone LIKE '%". $keyword ."%' 
        OR address LIKE '%". $keyword ."%' ";   
    }

    if($datefrom!= "null" && $dateto != "null"){
        $conditions .= "WHERE created_at >= '$datefrom' AND created_at <= '$dateto' ";
    }

    if($sortto == 'DESC'){
        $sortby = "ORDER BY $sort DESC";
    }else{
        $sortby = "ORDER BY $sort ASC";
    }

    $conditions .= $sortby;
    $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");
    echo json_encode(array('data' => $searchresult,'index' => $page,'total_items' => count($app->dbSelect($conditions))));
}

public function atwdeleteAction($pageid){
    $page = Atw::findFirst(array("id=" . $pageid));
    $data = array('error' => 'Not Found');
    if($page){
        if ($page->delete()) {
            $data = array('success' => 'Successfully Deleted');
        }
    }
    echo json_encode($data);
}

public function atwReviewAction($id){
    $app = new CB();
    $searchresult = $app->dbSelect("SELECT * FROM atw WHERE id='".$id."'");
    echo json_encode($searchresult); 
}

public function feReviewAction($slugs){
  $app = new CB();
  $conditions = "SELECT atw.*,countryflags.flag,countryflags.country 
  FROM atw LEFT JOIN countryflags ON atw.country = countryflags.country where slugs='".$slugs."'";
  $searchresult = $app->dbSelect($conditions);
  echo json_encode($searchresult[0], JSON_NUMERIC_CHECK);
}

public function fasaveAction() {
    $data = array();
    if ($_POST) { 
        $check = $_POST['agree'];
        if ($check==true) {
            $atw = new Friends();
            $atw->assign(array(
                'organization' => $_POST['organization'],
                'add1' => $_POST['add1'],
                'add2' => $_POST['add2'],
                'city' => $_POST['city'],
                'state' => $_POST['state'],
                'zip' => $_POST['zip'],
                'country' => $_POST['country'],
                'category' => $_POST['cat'],
                'year' => $_POST['year'],
                'web' => $_POST['web'],
                'mission' => $_POST['mission'],
                'contactperson' => $_POST['contactperson'],
                'email' => $_POST['email'],
                'reason' => $_POST['reason'],
                'phone' => $_POST['phone'],
                'image' => $_POST['image'],
                'date' => date('Y-m-d H:i:s')
                ));
            if (!$atw->save()) {
                $errors = array();
                foreach ($atw->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
            else {
                     // $data['success'] = "Success";
                $content ='
                <!doctype html>
                <html class=no-js lang=en data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
                <head>
                    <meta charset="utf-8" />
                    <meta name=viewport content="width=device-width, initial-scale=1.0" />
                    <title>Earth Citizens | Newsletter</title>
                    <meta name=description content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more." />
                    <meta name=author content="ZURB, inc. ZURB network also includes zurb.com" />
                    <meta name=copyright content="ZURB, inc. Copyright (c) 2015" />
                    <link rel=stylesheet type=text/css href="https://earthcitizens.org/css/fr/font-awesome.css" />
                    <link rel=stylesheet type=text/css href="https://earthcitizens.org/css/fr/font-awesome.min.css" />
                </head>
                <body style="background:#fff;color:#222;padding:0;margin:0;font-family:" Helvetica Neue ", Helvetica, Roboto, Arial, sans-serif; font-weight: normal;font-style: normal;line-height: 1.5;position: relative;cursor: auto; font-size: 100%;">
                    <div style=width:100%;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0;max-width:62.5rem>
                        <div style=width:100%>
                            <div style=padding-top:25px;padding-bottom:15px;overflow:hidden>
                                <div style=width:100%>
                                    <div style=width:200px;margin-left:auto;margin-right:auto;margin-bottom:25px><img src=https://earthcitizens.org/images/template_images/ecologo1.png style=width:100%></div>
                                </div>
                            </div>
                            <hr style="border 2px solid #000" />
                            <div style=width:25%;float:left;height:35px;padding-top:15px;color:#555;font:Arial,sans-serif>Request Date: <b>'.date("Y-m-d").'</b></div>
                            <div style=width:75%;float:right>
                                <div style=overflow:hidden;width:300px;height:50px;float:right>
                                    <ul style=margin-left:auto;margin-right:auto;padding:0;list-style:none>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=https://www.facebook.com/EarthCitizensOrganization>LIKE <img style=width:20px;height:20px src=https://earthcitizens.org/images/facebook.png></a></li>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=https://twitter.com/goearthcitizens>FOLLOW <img style=width:20px;height:20px src=https://earthcitizens.org/images/twitter.png></a></li>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=#>REFER <img style=width:20px;height:20px src=https://earthcitizens.org/images/email.png></a></li>
                                    </ul>
                                </div>
                            </div>
                            <hr/>
                        </div>
                    </div>
                    <div style=width:100%;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0;max-width:62.5rem>
                        <h3>A new organization has been listed to ECO Friends and Allies</h3>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Name of Organization:</b></label> <br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['organization'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Address 1:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['add1'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Address 2:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['add2'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>City:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['city'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>State:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['state'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Zip:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['zip'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Country:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['country'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Organization Category:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['cat'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Year:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['year'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Organization Website Address:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['web'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Organization Mission Statement:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['mission'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Contact Person:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['contactperson'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Contact Phone Number:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['phone'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Contact Email:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['email'].'</span>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 16px"><b>Reason for Partnership:</b></label><br>
                        <span style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;">'.$_POST['reason'].'</span>

                    </div>

                </body>
                </html>
                ';

                $mail = new PHPMailer();
                // $adminemail = "info@earthcitizens.org";
                $adminemail = "infoeco@mailinator.com";
                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';               
                $mail->Port = 587;

                // $mail->From = 'info@earthcitizens.org';
                $mail->From = "infoeco@mailinator.com";
                $mail->FromName = 'ECO Friends and Allies';
                $mail->addAddress($adminemail);

                $mail->isHTML(true);
                $mail->Subject = 'ECO Friends and Allies Request';
                $mail->Body = $content;
                if (!$mail->send()) {
                    $data['error'] = "Email not sent";
                } else {
                    $data['success'] = "Success";
                }
                echo json_encode($data);

            }
               // echo json_encode($data);
        }
    }
    else{
       $data['error'] = "Something went wrong saving the data, please try again.";
       echo json_encode($data);
   }

}

public function falistAction($num, $page, $keyword, $sort, $sortto, $datefrom, $dateto) {
    $app = new CB();
    $offsetfinal = ($page * 10) - 10;

    if ($keyword == 'null' || $keyword == 'undefined') {

        $conditions = "SELECT * FROM friends ";
    } else {
        $conditions = "SELECT * FROM friends WHERE organization LIKE '%". $keyword ."%' OR add1 LIKE '%". $keyword ."%'
        OR category LIKE '%". $keyword ."%' OR contactperson LIKE '%". $keyword ."%' OR phone LIKE '%". $keyword ."%'
        OR email LIKE '%". $keyword ."%' ";   
    }

    if($datefrom!= "null" && $dateto != "null"){
     $conditions .= "WHERE date(date) >= '$datefrom' AND date(date) <= '$dateto' ";
 }

 if($sortto == 'DESC'){
    $sortby = "ORDER BY $sort DESC";
}else{
    $sortby = "ORDER BY $sort ASC";
}


$conditions .= $sortby;

$searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");
$count = $app->dbSelect($conditions);

echo json_encode(array(
    'data' => $searchresult,
    'index' => $page,
    'total_items' => count($count)
    ));
}

public function fainfoAction($pageid) {
    $app = new CB();
    $sql = "SELECT * FROM friends  where id ='".$pageid."' ";
    $searchresult = $app->dbSelect($sql);
    echo json_encode($searchresult);
}

public function fadeleteAction($pageid) {
    $conditions = "id=" . $pageid;
    $page = Friends::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($page) {
        if ($page->delete()) {
            $data = array('success' => 'Successfully Deleted');
        }
    }
    echo json_encode($data);
}


}
