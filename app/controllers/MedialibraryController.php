<?php

namespace Controllers;

use \Models\Medialibrary as Medialibrary;

use \Models\Tagsml as Tagsml;
use \Models\Tagslistml as Tagslistml;
use \Models\Categoryml as Categoryml;
use \Models\Categoriesml as Categoriesml;
use \Models\Notifications as Notifications;
use \Models\Members as Members;
use \Models\Videolikes as Videolikes;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class MedialibraryController extends \Phalcon\Mvc\Controller
{

	public function addAction()
	{
		if($_POST){

			$guid = new \Utilities\Guid\Guid();
			$mlID = $guid->GUID();

			$media = new Medialibrary();
			$media->id = $mlID;
			$media->status = "Submitted";
			$media->title = $_POST['title'];
			$media->slugs = $_POST['slugs'];
			$media->shortdesc = $_POST['shortdesc'];
			$media->description = $_POST['description'];
			$media->datecreated = date('Y-m-d');
			$media->dateupdated = date("Y-m-d H:i:s");
			$media->author = $_POST['authorid'];
			$media->showvideo = "Enabled";
			$media->name = $_POST['name'];
			$media->phone = $_POST['phone'];
			$media->email = $_POST['email'];
			$media->web = $_POST['web'];
			$media->fb = $_POST['fb'];
			$media->videotype = $_POST['type'];
			if($_POST['type'] == "embed"){
			  $media->embed = $_POST['embed'];
			}else{
			  $media->video = $_POST['video'];
			  $media->videothumb = $_POST['videothumb'];
			}		

			if (!$media->save()) {
				$errors = array();
				foreach ($media->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array('error' => $errors));
			} else {
				$data['success'] = "Video saved.";
				echo json_encode($data);
				$mltags = $_POST['tag'];
				foreach($mltags as $nt){
					$gettags = Tagsml::findFirst('tags="'.$nt.'"');
					if(!$gettags){
						$mltags = new Tagsml();
						$mltags->assign(array(
							'tags' => $nt,
							'slugs' => str_replace("-", " ", $nt)
							));

						if (!$mltags->save())
						{
							$data['error'] = "Something went wrong saving the tags, please try again.";
						}
						else
						{
							$data['success'] = "Success Tags";
							$get = Tagsml::findFirst('tags="'.$nt.'"');
							$tagsofml = new Tagslistml();
							$tagsofml->assign(array(
								'id' => $mlID,
								'tagid' => $get->id
								));
							if (!$tagsofml->save())
							{
								$data['error'] = "Something went wrong saving the tags, please try again.";
							}
							else
							{
								$data['success'] = "Success Tags";
							}
						}

					}else{
						$data['success'] = "Success";
						$tagsofml = new Tagslistml();
						$tagsofml->assign(array(
							'id' => $mlID,
							'tagid' => $gettags->id
							));
						if (!$tagsofml->save())
						{
							$data['error'] = "Something went wrong saving the tags, please try again.";
						}
						else
						{
							$data['success'] = "Success tags else";
						}
					}

				}
				$mlcat = $_POST['category'];
				foreach($mlcat as $mlcat){
					$catofml = new Categoriesml();
					if(isset($mlcat['categoryid'])){
						$catofml->assign(array(
							'id' => $mlID,
							'categoryid' => $mlcat['categoryid']
							));
					}
					else{
						$catofml->assign(array(
							'id' => $mlID,
							'categoryid' => $mlcat
							));
					}

					if (!$catofml->save()){
						$errors = array();
						foreach ($catofml->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						echo json_encode(array('error' => $errors));
					}
					else {
						$data['success'] = "Success Category";
					}
				}
			}
		}
	}

	public function updatevideoAction(){
		$mlID = $_POST['id'];
		$getvideo = Medialibrary::findFirst("id='" . $mlID ."'");
		if($getvideo){
			$getvideo->title = $_POST['title'];
			$getvideo->shortdesc = $_POST['shortdesc'];
			$getvideo->description = $_POST['description'];
			$getvideo->slugs = $_POST['slugs'];
			$getvideo->name = $_POST['name'];
			$getvideo->phone = $_POST['phone'];
			$getvideo->email = $_POST['email'];
			$getvideo->web = $_POST['web'];
			$getvideo->fb = $_POST['fb'];
			if($_POST['videothumb']){
				$getvideo->videothumb = $_POST['videothumb'];
			}
			  
			if($_POST['stat'] == '2'){
				$getvideo->status = "Resubmitted";
			}
			if (!$getvideo->save()) {
				$errors = array();
				foreach ($getvideo->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array('error' => $errors));
			} else {
				$data['success'] = "Updated";
				$conditions = 'id="' . $mlID . '"';
				$deletetags = Tagslistml::find(array($conditions));
				$data = array('error' => 'Not Found');
				if ($deletetags) {
					foreach($deletetags as $deletetags){

						if ($deletetags->delete()) {
							$data = array('success' => 'tags Deleted');
						}
					}
				}
				

				$mltags = $_POST['tag'];
				foreach($mltags as $nt){
					$gettags = Tagsml::findFirst('tags="'.$nt.'"');
					if(!$gettags){
						$mltags = new Tagsml();
						$mltags->assign(array(
							'tags' => $nt,
							'slugs' => str_replace("-", " ", $nt)
							));

						if (!$mltags->save())
						{
							$data['error'] = "Something went wrong saving the tags, please try again.";
						}
						else
						{
							$data['success'] = "Success Tags";
							$get = Tagsml::findFirst('tags="'.$nt.'"');
							$tagsofml = new Tagslistml();
							$tagsofml->assign(array(
								'id' => $mlID,
								'tagid' => $get->id
								));
							if (!$tagsofml->save())
							{
								$data['error'] = "Something went wrong saving the tags, please try again.";
							}
							else
							{
								$data['success'] = "Success Tags";
							}
						}

					}else{
						$data['success'] = "Success";
						$tagsofml = new Tagslistml();
						$tagsofml->assign(array(
							'id' => $mlID,
							'tagid' => $gettags->id
							));
						if (!$tagsofml->save())
						{
							$data['error'] = "Something went wrong saving the tags, please try again.";
						}
						else
						{
							$data['success'] = "Success tags else";
						}
					}

				}

				$conditionscat = 'id="' . $mlID . '"';
				$deletecat = Categoriesml::find(array($conditionscat));
				$data = array('error' => 'Not Found');
				if ($deletecat) {
					foreach($deletecat as $deletecat){

						if ($deletecat->delete()) {
							$data = array('success' => 'tags Deleted');
						}
					}
				}


				$mlcat = $_POST['category'];
				foreach($mlcat as $mlcat){
					$catofml = new Categoriesml();
					if(isset($mlcat['categoryid'])){
						$catofml->assign(array(
							'id' => $mlID,
							'categoryid' => $mlcat['categoryid']
							));
					}
					else{
						$catofml->assign(array(
							'id' => $mlID,
							'categoryid' => $mlcat
							));
					}

					if (!$catofml->save()){
						$errors = array();
						foreach ($catofml->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						echo json_encode(array('error' => $errors));
					}
					else {
						$data['success'] = "Success Category";
					}
				}
			}
		}
	}

	public function femylistmoreAction($offset, $num , $userid)
	{
		$app = new CB();

		$conditions = "SELECT * FROM medialibrary where author='".$userid."'"; 
		$limit = "ORDER BY datecreated LIMIT ".$offset.", ". $num .";                                                                                                                                                                        ";
		$searchresult = $app->dbSelect($conditions.$limit);
		foreach($searchresult as $sr=>$val){
			$countlikes = count(Videolikes::find("videoid='".$val['id']."'"));
			$searchresult[$sr]['likes'] = $countlikes;
		}
		$count = $app->dbSelect($conditions);
		echo json_encode(array('data' => $searchresult, 'count' => count($count)));
	}

	public function showvideoAction($id,$stat){
		$getInfo = Medialibrary::findFirst('id="'. $id .'"');
		$getInfo->showvideo = $stat;
		$getInfo->save();
		$data=array('success' => $stat);
		echo json_encode($data);
	}

	public function fedeleteAction($id){
		$getInfo = Medialibrary::findFirst('id="'. $id .'"');
		if ($getInfo) {
			if ($getInfo->delete()) {
				$data = array('success' => 'Video Deleted');
			}
		}	
	}

	public function editvideoAction($slugs){
		$app = new CB();
		$conditions = "SELECT medialibrary.title,medialibrary.author,medialibrary.description,medialibrary.embed,medialibrary.id,
		 medialibrary.shortdesc,medialibrary.videotype,medialibrary.video,medialibrary.videothumb,members.userid,members.lastname,members.firstname,
		 medialibrary.datepublished,medialibrary.status,medialibrary.disapprovemsg,medialibrary.featured,medialibrary.viewscount,medialibrary.name,medialibrary.phone
		 ,medialibrary.email,medialibrary.fb,medialibrary.web
		 FROM medialibrary LEFT JOIN members ON medialibrary.author = members.userid where slugs='".$slugs."'";
		$searchresult = $app->dbSelect($conditions);

		foreach($searchresult as $sr=>$val){
			$countlikes = count(Videolikes::find("videoid='".$val['id']."'"));
			$categories = $app->dbSelect("SELECT * FROM categoryml INNER JOIN categoriesml ON categoryml.categoryid = categoriesml.categoryid WHERE categoriesml.id ='".$val['id']."' ");
			$tags = $app->dbSelect("SELECT * FROM tagsml INNER JOIN tagslistml ON tagsml.id = tagslistml.tagid WHERE tagslistml.id ='".$val['id']."' ");
			$searchresult[$sr]['category'] = $categories;
			$searchresult[$sr]['tag'] = $tags;
			$searchresult[$sr]['likes'] = $countlikes;
		}
		if($searchresult){ echo json_encode($searchresult); }
		else{ echo json_encode("NODATA"); }
	}

	public function setfeaturedAction($id){
		$getvideo = Medialibrary::findFirst("featured='true'");
		if($getvideo){
			$getvideo->featured = Null;
			if ($getvideo->save()) {
				$setfeatured = Medialibrary::findFirst("id='".$id."'");
				$setfeatured->featured = 'true';
				$setfeatured->save();
			}
		}
		else{
			$setfeatured = Medialibrary::findFirst("id='".$id."'");
			$setfeatured->featured = 'true';
			$setfeatured->save();
		}
	}

	public function viewsaddAction($id){
		$getvideo = Medialibrary::findFirst("id='".$id."'");
		if($getvideo){
			$getvideo->viewscount = $getvideo->viewscount + 1;
			if ($getvideo->save()) {
				echo json_encode($getvideo->viewscount);
			}
		}
	}

	public function mediaindexAction(){
		$app = new CB();
		$conditions = "SELECT medialibrary.*,
		members.firstname,
		members.lastname FROM medialibrary";
		$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
		$conditions .= " WHERE medialibrary.featured='true'";
		$featured = $app->dbSelect($conditions);
		foreach($featured as $sr=>$val){
			date_default_timezone_set('UTC');
			$daystogo = strtotime($val['datepublished']); //convert days(ex. 30 days) or dates(ex. 01/20/2016 15:30:00) to Epoch Time
			
			$featured[$sr]['timeStarted'] = $daystogo * 1000; //add 3 zeros to Epoch Time = milliseconds
		}
		echo json_encode(array(
			'featured' => $featured,
			));
	}

	public function mostpopularlistAction()
	{
		$app = new CB();
		$conditions = "SELECT medialibrary.*,
		members.firstname,
		members.lastname FROM medialibrary";
		$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
		$conditions .= " WHERE medialibrary.status='Approved' AND medialibrary.showvideo='Enabled'";
		$pupular = $app->dbSelect($conditions);
		foreach($pupular as $sr=>$val){
			$countlikes = count(Videolikes::find("videoid='".$val['id']."'"));
			$pupular[$sr]['likes'] = $countlikes;
		}

		//SORT ARRAY BY LIKES
		function array_sort_by_column($arr, $col, $dir = SORT_DESC) {
			$sort_col = array();
			foreach ($arr as $key=> $row) {
				$sort_col[$key] = $row[$col];
			}

			array_multisort($sort_col, $dir, $arr);
			return $arr;
		}
		$final = array();
		$final = array_sort_by_column($pupular, 'likes');
		$stack = array();
		array_push($stack, $final[0], $final[1], $final[2]);


		echo json_encode(array(
			'data' => $stack
			));
	}

	public function mediaindexlistAction($offset, $num)
	{
		$app = new CB();

		$conditions = "SELECT medialibrary.*,
		members.firstname,
		members.lastname FROM medialibrary";
		$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
		$conditions .= " WHERE medialibrary.status='Approved' AND medialibrary.showvideo='Enabled'";
		$limit = "ORDER BY datepublished LIMIT ".$offset.", ". $num .";                                                                                                                                                                        ";
		$searchresult = $app->dbSelect($conditions.$limit);
		$count = $app->dbSelect($conditions);
		echo json_encode(array('data' => $searchresult, 'count' => count($count)));
	}


	public function tagslistAction($offset, $num, $tag){

		$app = new CB();

		$conditions = "SELECT medialibrary.*, 
		tagslistml.*, tagsml.slugs as tagslugs, tagsml.tags,
		members.firstname,
		members.lastname FROM";
		$conditions .= " tagslistml LEFT JOIN tagsml ON tagslistml.tagid = tagsml.id";
		$conditions .= " LEFT JOIN medialibrary ON tagslistml.id = medialibrary.id";
		$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
		$conditions .= " WHERE tagsml.slugs='$tag'";
		$conditions .= " AND medialibrary.status='Approved' AND medialibrary.showvideo='Enabled'";
		$limit = " ORDER BY datepublished LIMIT ".$offset.", ". $num .";                                                                                                                                                                        ";
		$searchresult = $app->dbSelect($conditions.$limit);
		$count = $app->dbSelect($conditions);
		$gettagname = "SELECT tagsml.tags FROM tagsml WHERE slugs = '$tag'";
		$tagsname = $app->dbSelect($gettagname);

		echo json_encode(array('data' => $searchresult, 'tagsname' => $tagsname, 'count' => count($count)));

	}


	public function displayarchivelistAction($offset, $num, $archive){

		$app = new CB();
		$dates = explode("-", $archive);
		$month = $dates[0];
		$year = $dates[1];
    	$searchdate = date("Y-m-d", strtotime($month." ".$year));
    	$betweendate = date("Y-m-d", strtotime("+1 months -1 day", strtotime($searchdate)));
		$conditions = "SELECT * FROM medialibrary";
		$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
		$conditions .= " WHERE";
		$conditions .= " medialibrary.datepublished >= '$searchdate' AND medialibrary.datepublished <= '$betweendate'";
		$conditions .= " AND medialibrary.status='Approved' AND medialibrary.showvideo='Enabled'";
		$limit = " ORDER BY medialibrary.datepublished LIMIT ".$offset.", ". $num .";                                                                                                                                                                        ";
		$searchresult = $app->dbSelect($conditions.$limit);
		$count = $app->dbSelect($conditions);
		$gettagname = "SELECT tagsml.tags FROM tagsml WHERE slugs = '$tag'";
		$tagsname = $app->dbSelect($gettagname);

		echo json_encode(array('data' => $searchresult, 'count' => count($count)));

	}


	public function categorylistAction($offset, $num, $category){

		$app = new CB();

		$conditions = "SELECT medialibrary.*, categoriesml.*,
		categoryml.categoryslugs as catslugs, categoryml.categoryname,
		members.firstname,
		members.lastname FROM";
		$conditions .= " categoriesml LEFT JOIN categoryml ON categoriesml.categoryid = categoryml.categoryid";
		$conditions .= " LEFT JOIN medialibrary ON categoriesml.id = medialibrary.id";
		$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
		$conditions .= " WHERE categoryml.categoryslugs='$category'";
		$conditions .= " AND medialibrary.status='Approved' AND medialibrary.showvideo='Enabled'";
		$limit = " ORDER BY datepublished LIMIT ".$offset.", ". $num .";                                                                                                                                                                        ";
		$searchresult = $app->dbSelect($conditions.$limit);
		$count = $app->dbSelect($conditions);
		$getcatname = "SELECT categoryml.categoryname FROM categoryml WHERE categoryslugs = '$category'";
		$catname = $app->dbSelect($getcatname);

		echo json_encode(array('data' => $searchresult, 'catname' => $catname, 'count' => count($count)));

	}


	public function viewvideoAction($slugs){
		$app = new CB();
		$conditions = "SELECT medialibrary.title,medialibrary.author,medialibrary.description,medialibrary.embed,medialibrary.id,
		 medialibrary.shortdesc,medialibrary.videotype,medialibrary.video,medialibrary.videothumb,members.userid,members.lastname,members.firstname,
		 medialibrary.datepublished,medialibrary.status,medialibrary.disapprovemsg,medialibrary.featured,medialibrary.viewscount
		 FROM medialibrary LEFT JOIN members ON medialibrary.author = members.userid where slugs='".$slugs."'";
		$searchresult = $app->dbSelect($conditions);

		foreach($searchresult as $sr=>$val){
			$categories = $app->dbSelect("SELECT * FROM categoryml INNER JOIN categoriesml ON categoryml.categoryid = categoriesml.categoryid WHERE categoriesml.id ='".$val['id']."' ");
			$tags = $app->dbSelect("SELECT * FROM tagsml INNER JOIN tagslistml ON tagsml.id = tagslistml.tagid WHERE tagslistml.id ='".$val['id']."' ");
			$searchresult[$sr]['category'] = $categories;
			$searchresult[$sr]['tag'] = $tags;
		}
		if($searchresult){ echo json_encode($searchresult); }
		else{ echo json_encode("NODATA"); }
	}



	// Backend part

	public function belistAction($num, $page, $keyword, $status, $sort, $sortto) {
		$app = new CB();
		$offsetfinal = ($page * 10) - 10;

		if ($keyword == 'undefined' && $status == 'undefined') {
			$conditions = "SELECT medialibrary.status,
								  medialibrary.author,
								  medialibrary.title,
								  medialibrary.slugs,
								  medialibrary.id,
								  medialibrary.datecreated,
								  medialibrary.datepublished,
								  medialibrary.featured,
								  members.firstname,
								  members.lastname 
								   FROM medialibrary";
			$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid ";
		}
		elseif($keyword != 'undefined' && $status == 'undefined') {
			$conditions = "SELECT medialibrary.status,
								  medialibrary.author,
								  medialibrary.title,
								  medialibrary.slugs,
								  medialibrary.id,
								  medialibrary.datecreated,
								  medialibrary.datepublished,
								  medialibrary.featured,
								  members.firstname,
								  members.lastname FROM medialibrary";
			$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
			$conditions .= " WHERE title LIKE '%" . $keyword . "%' OR datepublished LIKE '%" . $keyword . "%' OR datecreated LIKE '%" . $keyword . "%' OR firstname LIKE '%" . $keyword . "%'  OR lastname LIKE '%" . $keyword . "%' ";
		}
		elseif($keyword == 'undefined' && $status != 'undefined') {
			$conditions = "SELECT medialibrary.status,
								  medialibrary.author,
								  medialibrary.title,
								  medialibrary.slugs,
								  medialibrary.id,
								  medialibrary.datecreated,
								  medialibrary.datepublished,
								  medialibrary.featured,
								  members.firstname,
								  members.lastname FROM medialibrary";
			$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
			$conditions .= " WHERE medialibrary.status='" . $status . "'";
		}
		else {
			$conditions = "SELECT medialibrary.status,
								  medialibrary.author,
								  medialibrary.title,
								  medialibrary.slugs,
								  medialibrary.id,
								  medialibrary.datecreated,
								  medialibrary.datepublished,
								  medialibrary.featured,
								  members.firstname,
								  members.lastname FROM medialibrary";
			$conditions .= " LEFT JOIN members ON medialibrary.author = members.userid";
			$conditions .= " WHERE medialibrary.status='" . $status . "' AND title LIKE '%" . $keyword . "%' 
			OR medialibrary.status='" . $status . "' AND datecreated LIKE '%" . $keyword . "%' 
			OR medialibrary.status='" . $status . "' AND  firstname LIKE '%" . $keyword . "%'  
			OR medialibrary.status='" . $status . "' AND  lastname LIKE '%" . $keyword . "%' ";
		}

		if($sortto == 'DESC'){
			$sortby = "ORDER BY $sort DESC";
		}else{
			$sortby = "ORDER BY $sort ASC";
		}

		$conditions .= $sortby;

		$searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

		$count = $app->dbSelect($conditions);

		echo json_encode(array(
			'data' => $searchresult,
			'index' => $page,
			'total_items' => count($count)
			));
	}

	public function resolutionAction($id,$status)
	{
		$vid = Medialibrary::findFirst("id='" . $id ."'");
		if($vid){
			if($status == "Approved"){
				$notifystatus = 11;
				$vid->datepublished = date("Y-m-d H:i:s");
			}else{
				$notifystatus = 10;
				$vid->disapprovemsg = $_POST['msg'];
			}
			$vid->status = $status;
			$vid->dateupdated = date("Y-m-d H:i:s");

			if (!$vid->save()) {
				$errors = array();
				foreach ($vid->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				die(json_encode(array('error' => $errors)));
			} else {
				$data['success'] = "Status Updated.";
				$guid = new \Utilities\Guid\Guid();
				$noteID = $guid->GUID();
				$note = new Notifications();
				$note->assign(array(
					'noteID' => $noteID,
					'userID' => $vid->author,
					'readStatus' => 0,
					'content' => $vid->title,
					'noteType' => $notifystatus,
					'itemID' =>  $id,
					'timeStarted' => time().'000',
					'date_created' => date("Y-m-d H:i:s")
					));

				if (!$note->save()) {
					$errors = array();
					foreach ($note->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					die(json_encode(array('error' => $errors)));
				} else {
					$data['notesuccess'] = "Notification saved.";
				}

				
					$member = Members::findFirst("userid='" . $vid->author ."'");

					$dc = new CB();
					$content = $dc->approvedvideoEmail($vid->title,$member->firstname,$member->lastname,$dc->config->application->baseURL,$notifystatus,$_POST['msg']);
					$json = json_encode(array(
						'From' => $dc->config->postmark->signature,
						'To' => $member->email,
						'Subject' => "ECO Media library",
						'HtmlBody' => $content
						));

					$ch2 = curl_init();
					curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
					curl_setopt($ch2, CURLOPT_POST, true);
					curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
						'Accept: application/json',
						'Content-Type: application/json',
						'X-Postmark-Server-Token: '.$dc->config->postmark->token
						));
					curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
					$response = json_decode(curl_exec($ch2), true);
					$http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
					curl_close($ch2);

					if ($http_code!=200) {
						$data = array('error' => $mail->ErrorInfo);
					} else {
						$data = array('success' => 'success');
					}
				
			}
		}
		echo json_encode($data);
	}



	public function listcategoryAction()
	{
		$app = new CB();
		$sql = "SELECT categoriesml.id as t_id, categoriesml.categoryid,medialibrary.id,medialibrary.status
		FROM categoriesml LEFT JOIN medialibrary on categoriesml.id = medialibrary.id
		where (categoriesml.id is not null or categoriesml.id != '') and (categoriesml.categoryid is not null or categoriesml.categoryid != '') 
		and medialibrary.status = 'Approved' Group by categoriesml.categoryid";
		$searchresult = $app->dbSelect($sql);

		foreach ($searchresult as $key => $gettags) {
			$tags[]=array(
				'categoryid'=>$searchresult[$key]['categoryid']
				);
		}
		foreach ($tags as $value) {
			$gettags="SELECT * from categoryml where categoryid = '".$value['categoryid']."' ";
			$searchtags = $app->dbSelect($gettags);
			foreach ($searchtags as $val) {
				$tagsml[]=array(
				'categoryid'=>$val['categoryid'],
				'categoryname'=> $val['categoryname'],
				'categoryslugs'=> $val['categoryslugs']
				);
			}
			
		}
		echo json_encode($tagsml);
		
	}
	public function listcategoryallAction()
	{	
		$app = new CB();
		$get= Categoryml::find(array("order" => "categoryid DESC"));
		$sql = "SELECT categoriesml.id as t_id, categoriesml.categoryid,medialibrary.id,medialibrary.status
		FROM categoriesml LEFT JOIN medialibrary on categoriesml.id = medialibrary.id
		where (categoriesml.id is not null or categoriesml.id != '') and (categoriesml.categoryid is not null or categoriesml.categoryid != '') 
		and medialibrary.status = 'Approved' Group by categoriesml.categoryid";
		$searchresult = $app->dbSelect($sql);
		foreach ($searchresult as $key => $gettags) {
			$tags[]=array(
				'categoryid'=>$searchresult[$key]['categoryid']
				);
		}
		foreach ($tags as $value) {
			$gettags="SELECT * from categoryml where categoryid = '".$value['categoryid']."' ";
			$searchtags = $app->dbSelect($gettags);
			foreach ($searchtags as $val) {
				$tagsml[]=$val['categoryid'];
			}
		}
		foreach ($get as $get) {
			$tagsmlall[] = $get->categoryid;
		}
		$array4 = array_merge($tagsml,$tagsmlall);
			foreach ($array4 as $value) {
				$gets="SELECT * from categoryml where categoryid = '".$value."' ";
				$result = $app->dbSelect($gets);
				foreach ($result as $search) {
					$tag[]=$search['categoryid'];	
				}
			}
			$unique = array_unique($tag);
			
			foreach ($unique as $un) {
				$final="SELECT * from categoryml where categoryid = '".$un."' ";
				$res = $app->dbSelect($final);
				foreach ($res as $res) {
					$finaltag[]=array(
						'categoryid'=>$res['categoryid'],
						'categoryname'=> $res['categoryname'],
						'categoryslugs'=> $res['categoryslugs']
						);	
				}
			}
			
			echo json_encode($finaltag);
		// $getcategory= Categoryml::find(array("order" => "categoryid DESC"));
		// foreach ($getcategory as $getcategory) {;
		// 	$data[] = array(
		// 		'categoryid'=>$getcategory->categoryid,
		// 		'categoryname'=>$getcategory->categoryname,
		// 		'categoryslugs'=>$getcategory->categoryslugs
		// 		);
		// }
		// echo json_encode($data);
	}

	public function listtagsallAction()
	{
		$app = new CB();
		$get=Tagsml::find(array("order" => "id DESC"));
		$sql = "SELECT tagslistml.id as t_id, tagslistml.tagid,medialibrary.id,medialibrary.status
		FROM tagslistml LEFT JOIN medialibrary on tagslistml.id = medialibrary.id
		where (tagslistml.id is not null or tagslistml.id != '') and (tagslistml.tagid is not null or tagslistml.tagid != '') 
		and medialibrary.status = 'Approved' Group by tagslistml.tagid";
		$searchresult = $app->dbSelect($sql);
		foreach ($searchresult as $key => $gettags) {
			$tags[]=array(
				'tagid'=>$searchresult[$key]['tagid']
				);
		}
		foreach ($tags as $value) {
			$gettags="SELECT * from tagsml where id = '".$value['tagid']."' ";
			$searchtags = $app->dbSelect($gettags);
			foreach ($searchtags as $val) {
				$tagsml[]=$val['id'];	
			}
		}
		foreach ($get as $get) {
			$tagsmlall[] = $get->id;
		}
		$array4 = array_merge($tagsml,$tagsmlall);
			foreach ($array4 as $value) {
				$gets="SELECT * from tagsml where id = '".$value."' ";
				$result = $app->dbSelect($gets);
				foreach ($result as $search) {
					$tag[]=$search['id'];	
				}
			}
			$unique = array_unique($tag);
			foreach ($unique as $un) {
				$final="SELECT * from tagsml where id = '".$un."' ";
				$res = $app->dbSelect($final);
				foreach ($res as $res) {
					$finaltag[]=array(
						'id'=>$res['id'],
						'tags'=> $res['tags'],
						'slugs'=> $res['slugs']
						);	
				}
			}
		echo json_encode($finaltag);
	}
	public function listtagsAction()
	{
		$app = new CB();
		$sql = "SELECT tagslistml.id as t_id, tagslistml.tagid,medialibrary.id,medialibrary.status
		FROM tagslistml LEFT JOIN medialibrary on tagslistml.id = medialibrary.id
		where (tagslistml.id is not null or tagslistml.id != '') and (tagslistml.tagid is not null or tagslistml.tagid != '') 
		and medialibrary.status = 'Approved' Group by tagslistml.tagid";
		$searchresult = $app->dbSelect($sql);

		foreach ($searchresult as $key => $gettags) {
			$tags[]=array(
				'tagid'=>$searchresult[$key]['tagid']
				);
		}
		foreach ($tags as $value) {
			$gettags="SELECT * from tagsml where id = '".$value['tagid']."' ";
			$searchtags = $app->dbSelect($gettags);
			foreach ($searchtags as $val) {
				$tagsml[]=array(
				'tags'=> $val['tags'],
				'slugs'=> $val['slugs']
				);
			}
			
		}
		echo json_encode($tagsml);

	}

	public function ArchiveListAction() {

		$app = new CB();

		$conditions = "SELECT * FROM medialibrary ";
		$conditions .= "WHERE status = 'Approved' ";
		$conditions .= "ORDER BY datepublished DESC";
		$searchresult = $app->dbSelect($conditions);
		$dates = [];
		foreach($searchresult as $sr=>$val){
			if(!in_array(date('F Y', strtotime($val['datepublished'])), $dates)){

				$data[] = array(
					'month' => date('F', strtotime($val['datepublished'])),
					'year' => date('Y', strtotime($val['datepublished'])),
					'datepublished' => $val['datepublished']
					);
            	array_push($dates, date('F Y', strtotime($val['datepublished'])));
			}
		}
		echo json_encode($data);
	}

	public function likeunlikeAction($videoid,$userid){
		$getlikes = Videolikes::findFirst("usrid='".$userid."' AND videoid='".$videoid."'");
		if($getlikes){
			if ($getlikes->delete()) {
				$data = "false";
			}
		}else{
			$like = new Videolikes();
			$like->usrid = $userid;
			$like->videoid = $videoid;
			if ($like->save()){
				$data = "true";
			}
		}
		echo json_encode($data);
	}
	public function countlikesAction($videoid,$usrid){
		$getlikes = count(Videolikes::find("videoid='".$videoid."'"));
		$likes = Videolikes::findFirst("usrid='".$usrid."' AND videoid='".$videoid."'");
		if($likes){
			$usrlike = 'true';
		}else{
			$usrlike = 'false';
		}
		echo json_encode(array("getlikes"=>$getlikes,"usrlike"=>$usrlike));
	}

	public function managetagsAction($num, $page, $keyword)
	{

		if ($keyword == 'null' || $keyword == 'undefined') {
			$tag = Tagsml::find();
		} else {
			$conditions = "tags LIKE '%" . $keyword . "%'";
			$tag = Tagsml::find(array($conditions));
		}

		$currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator = new \Phalcon\Paginator\Adapter\Model(
			array(
				"data" => $tag,
				"limit" => 10,
				"page" => $currentPage
				)
			);

        // Get the paginated results
		$page = $paginator->getPaginate();

		$data = array();
		foreach ($page->items as $m) {
			$data[] = array(
				'id' => $m->id,
				'tags' => $m->tags,
				'slugs' => $m->slugs,
				);
		}
		$p = array();
		for ($x = 1; $x <= $page->total_pages; $x++) {
			$p[] = array('num' => $x, 'link' => 'page');
		}
		echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

	}
	public function updatetagsAction(){
		if($_POST){
			$id = $_POST['id'];

			$find = Tagsml::findFirst('id!=' . $id . ' AND tags = "'.$_POST['tags'].'"');
			if($find){
				$data['error'] = "Your data already exist.";
				echo json_encode($data);
			}else {
				$data = array();
				$gettag = Tagsml::findFirst('id=' . $id . ' ');
				$gettag->tags = $_POST['tags'];
				$gettag->slugs = $_POST['slugs'];

				if (!$gettag->save()) {
					$data['error'] = "Something went wrong saving the data, please try again.";
				} else {
					$data['success'] = "Success";
				}
				echo json_encode($data);
			}
		}
	}

	public function tagsdeleteAction($id) {
		
		$gettag = Tagsml::findFirst(array("id=" . $id));
		$data = array('error' => 'Not Found');
		if ($gettag) {
			if ($gettag->delete()) {
				$data = array('success' => 'Tag Deleted');
			}
		}
		$gettags = Tagslistml::findFirst(array("tagid=" . $id));
		if ($gettags) {
			if ($gettags->delete()) {
				$data = array('success2' => 'Tags Deleted');
			}
		}
		echo json_encode($data);
	}

	public function checktagstitlesAction($tagstitle) {
		$data = array();
		$titletag = Tagsml::findFirst("tags LIKE '" . $tagstitle . "'");
		if ($titletag == true) {
			($titletag == true) ? $data["tagsexist"] = "The news tag is already exist!" : '';
		}else{
			($titletag == false) ? $data["tagsavailable"] = "The news tag are available." : '';
		}
		echo json_encode($data);
	}
	
	public function createtagsAction(){

		$data = array();

		$tagsnames = new Tagsml();
		$tagsnames->assign(array(
			'tags' => $_POST['tags'],
			'slugs' => $_POST['slugs'],
			));
		if (!$tagsnames->save()){
			$errors = array();
			foreach ($tagsnames->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			echo json_encode(array('error' => $errors));
		}
		else{
			$data['success'] = "Success";
		}
		echo json_encode($data);
	}


	/////CATEGORY
	public function managecategoryAction($num, $page, $keyword)
	{

		if ($keyword == 'null' || $keyword == 'undefined') {
			$Categoryml = Categoryml::find();
		} else {
			$conditions = "categoryname LIKE '%" . $keyword . "%'";
			$Categoryml = Categoryml::find(array($conditions));
		}

		$currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator = new \Phalcon\Paginator\Adapter\Model(
			array(
				"data" => $Categoryml,
				"limit" => 10,
				"page" => $currentPage
				)
			);

        // Get the paginated results
		$page = $paginator->getPaginate();

		$data = array();
		foreach ($page->items as $m) {
			$data[] = array(
				'id' => $m->categoryid,
				'categoryname' => $m->categoryname,
				'slugs' => $m->categoryslugs,
				);
		}
		$p = array();
		for ($x = 1; $x <= $page->total_pages; $x++) {
			$p[] = array('num' => $x, 'link' => 'page');
		}
		echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

	}
	public function updatecategoryAction(){
		if($_POST){
			$id = $_POST['id'];

			$find = Categoryml::findFirst('categoryid!=' . $id . ' AND categoryname = "'.$_POST['categoryname'].'"');
			if($find){
				$data['error'] = "Your data already exist.";
				echo json_encode($data);
			}else {
				$data = array();
				$gettag = Categoryml::findFirst('categoryid=' . $id . ' ');
				$gettag->categoryname = $_POST['categoryname'];
				$gettag->categoryslugs = $_POST['categoryslugs'];

				if (!$gettag->save()) {
					$data['error'] = "Something went wrong saving the data, please try again.";
				} else {
					$data['success'] = "Success";
				}
				echo json_encode($data);
			}
		}
	}

	public function categorydeleteAction($id) {
		
		$gettag = Categoryml::findFirst(array("categoryid=" . $id));
		$data = array('error' => 'Not Found');
		if ($gettag) {
			if ($gettag->delete()) {
				$data = array('success' => 'Category Deleted');
			}
		}
		$gettags = Categoriesml::findFirst(array("categoryid=" . $id));
		if ($gettags) {
			if ($gettags->delete()) {
				$data = array('success2' => 'Category Deleted');
			}
		}
		echo json_encode($data);
	}

	public function checkcategorytitlesAction($categoryname) {
		$data = array();
		$titlecateg = Categoryml::findFirst("categoryname LIKE '" . $categoryname . "'");
		if ($titlecateg == true) {
			($titlecateg == true) ? $data["Categoryexist"] = "The Category is already exist!" : '';
		}else{
			($titlecateg == false) ? $data["Categoryavailable"] = "The Category is available." : '';
		}
		echo json_encode($data);
	}
	
	public function createcategoryAction(){

		$data = array();

		$catnames = new Categoryml();
		$catnames->assign(array(
			'categoryname' => $_POST['catnames'],
			'categoryslugs' => $_POST['categoryslugs'],
			));
		if (!$catnames->save()){
			$errors = array();
			foreach ($catnames->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			echo json_encode(array('error' => $errors));
		}
		else{
			$data['success'] = "Success";
		}
		echo json_encode($data);
	}

	public function categoryattemptdeleteAction() {
			$db = \Phalcon\DI::getDefault()->get('db');
			$request = new Request();
			if($request->isPost()) {
				$page = $request->getPost("page");
				$categoryid = $request->getPost("categoryid");
				$items = 10; //static
				$finaloffset = ($page * $items) - $items;

				$find_this_category = Categoriesml::find("categoryid = '".$categoryid."'");
				if($find_this_category == true) { //USED category
					$editable = array();
					foreach($find_this_category as $category) {
						$id = $category->id; //get the id
						$find_this = Categoriesml::find("categoryid = '".$id."'");
						if(count($find_this) == 1) { //it will replace first its ONLY CATEGORY
							$conditions = "id = '".$id."'";
							$getit = Medialibrary::findFirst(array('conditions'=>$conditions, 'columns'=>"id, title"));
							array_push($editable, $getit);
						}
					}
				}

			$data['totalitems'] = count($editable);
			if($page > ceil(count($editable)/$items) && $page > 1) {
				$page = $page - 1;
				$finaloffset = $finaloffset - $items;
			}

				//pagination -> array_slice
			$data['editable'] = array_slice($editable, $finaloffset, $items);

			$categorylist = Categoryml::find("id != '".$categoryid."'");
			$data['categorylist'] = $categorylist->toArray();
			$data['index'] = $page;
		}
		echo json_encode($data);
	}

}

