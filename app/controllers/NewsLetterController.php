<?php

namespace Controllers;

use \Models\Members as Members;
use \Models\Newsletter as Newsletter;
use \Models\Subscribers as Subscribers;
use \Models\Sentlogs as Sentlogs;
use PHPMailer as PHPMailer;
use Controllers\ControllerBase as CB;
use \Models\Donationlog as Donationlog;
use \Models\Donation as Donation;

class NewsLetterController extends \Phalcon\Mvc\Controller {

 public function createNewsLetterAction()
 {


   $newsletter = new Newsletter();
   $newsletter->assign(array(
    'title' => $_POST['title'],
    'body' => $_POST['body'],
    'date' => date('Y-m-d')
    ));
   if (!$newsletter->save()) 
   {
    $data['error'] = "Something went wrong saving the data, please try again.";
  } 
  else 
  {
   $data['success'] = "Success";
 }

 echo json_encode($data);
}



public function managenewsletterAction($num, $page, $keyword) {

  if ($keyword == 'null' || $keyword == 'undefined') {
    $Pages = Newsletter::find();
  } else {
    $conditions = "firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%'";
    $Pages = Newsletter::find(array($conditions));
  }

  $currentPage = (int) ($page);

            // Create a Model paginator, show 10 rows by page starting from $currentPage
  $paginator = new \Phalcon\Paginator\Adapter\Model(
    array(
      "data" => $Pages,
      "limit" => 10,
      "page" => $currentPage
      )
    );

            // Get the paginated results
  $page = $paginator->getPaginate();

  $data = array();
  foreach ($page->items as $m) {
    $data[] = array(
      'newsletterid' => $m->newsletterid,
      'title' => $m->title,
      );
  }
  $p = array();
  for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
  }
  echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}


public function newsletterdeleteAction($newsletterid) {
  $conditions = "newsletterid=" . $newsletterid;
  $newsletter = Newsletter::findFirst(array($conditions));
  $data = array('error' => 'Not Found');
  if ($newsletter) {
    if ($newsletter->delete()) {
      $data = array('success' => 'News Letter Deleted');
    }
  }
  echo json_encode($data);
}


public function newsletterinfoAction($newsletterid) {

  $newsletter = Newsletter::findFirst("newsletterid=" . $newsletterid);
  $data = array();
  if ($newsletter) {
    $data = array(
      'newsletterid' => $newsletter->newsletterid,
      'title' => $newsletter->title,
      'body' => $newsletter->body,
      'date' => $newsletter->date
      );
  }
  echo json_encode($data);
}


public function newsletterUpdateAction() 
{

  $data = array();
  $newsletterid = $_POST['newsletterid'];
  $newsletter = Newsletter::findFirst('newsletterid=' . $newsletterid . ' ');
  $newsletter->title = $_POST['title'];
  $newsletter->body = $_POST['body'];

  if (!$newsletter->save()) {
    $data['error'] = "Something went wrong saving the data, please try again.";
  } else {

   $data['success'] = "Success";
 }
 echo json_encode($data);
}



public function subscriberslistAction($newsletterid) {

  $Pages = Subscribers::find();
  

  $data = array();
  foreach ($Pages as $m) {

    $sents = Sentlogs::findFirst('email="' . $m->NMSemail . '" and newsletterid ='. $newsletterid );
    if(!$sents)
    {
     $data[] = array(
      'NMSid' => $m->NMSid,
      'NMSemail' => $m->NMSemail,
      );
   }

   
 }

 echo json_encode(array('data' => $data));

}

public function memberlistAction($newsletterid) {

  $Pages = Members::find();
  

  $data = array();
  foreach ($Pages as $m) {

    $sents = Sentlogs::findFirst('email="' . $m->email. '" and newsletterid ='. $newsletterid );
    if(!$sents)
    {
     $data[] = array(
      'userid' => $m->userid,
      'email' => $m->email,
      );
   }

   
 }

 echo json_encode(array('memdata' => $data));

}




public function sendnewsletterAction($NSemail) {

     $don = Donation::findFirst("id=1");
          $usercount = Donationlog::count(array("distinct" => "useremail"));
          $members = Donationlog::sum(array("column" => "amount"));
          //echo json_encode(array("user" => $don->usercount, "donations" => $don->amount));
         
          $totaluser = $usercount + $don->usercount;

                    $day = substr($_POST["date"],-2);
                 

                    $month = '';
                    if (substr($_POST["date"], 5, -3) == 01)
                    {
                      $month = 'Jan';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 02)
                    {
                      $month = 'Feb';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 03)
                    {
                      $month = 'Mar';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 04)
                    {
                      $month = 'Apr';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 05)
                    {
                      $month = 'May';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 06)
                    {
                      $month = 'Jun';
                    }elseif (substr($_POST["date"], 5, -3) == 07)
                    {
                      $month = 'Jul';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 08)
                    {
                      $month = 'Aug';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 09)
                    {
                      $month = 'Sep';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 10)
                    {
                      $month = 'Oct';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 11)
                    {
                      $month = 'Nov';
                    }
                    elseif (substr($_POST["date"], 5, -3) == 12)
                    {
                      $month = 'Dec';
                    }
                    

              
                $year = substr($_POST["date"], 0, -6);

  $dc = new CB();
  $json = json_encode(array(
    'From' => $dc->config->postmark->signature,
    'To' => $NSemail,
    'Subject' => 'Earth Citizen Organization NewsLetter',
    'HtmlBody' => '

    <!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Earth Citizens | Newsletter</title>
  <meta name="description" content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more."/>
  <meta name="author" content="ZURB, inc. ZURB network also includes zurb.com"/>
  <meta name="copyright" content="ZURB, inc. Copyright (c) 2015"/>
  
  <link rel="stylesheet" type="text/css" href="https://earthcitizens.org/css/fr/font-awesome.css"/>
  <link rel="stylesheet" type="text/css" href="https://earthcitizens.org/css/fr/font-awesome.min.css"/>
</head>
<body style="background: #fff;color: #222;padding: 0;margin: 0;font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-weight: normal;font-style: normal;line-height: 1.5;position: relative;cursor: auto; font-size: 100%;">

  <div style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">
    <div style="width: 100%;">
      <div style="padding-top: 25px;padding-bottom: 15px;overflow: hidden;">
        <div style="width: 25%;float:left">
          <div style="width: 200px;margin-left: auto;margin-right: auto;margin-bottom: 25px;">
          <img src="https://earthcitizens.org/images/template_images/ecologo1.png" style="width:100%;">
          </div>
        </div>
        <div style="width:50%;float:right">
          <div class="top-contact" style="text-align:center;">
            <span style="color: #5386C1;  " class="ng-binding"><i class="fa fa-user"> </i> Earth Citizens Count: ' .  $totaluser . '</span>
            <span style="color: #5386C1;  " class="join"><a style="line-height: 15px;text-align: center;padding: 3px 10px;color:#fff;font-size: 12px;text-decoration:none;color:#008CBA;background-color: rgba(255, 157, 15, 0.7);border: 2px solid #fff;border-radius:5px;" href="https://earthcitizens.org/donation##new-account">Join</a></span>
          </div>
          <p style="text-align: center;color: #555;">
            What makes a person a hero is not a mask, cape or a special kind of armor. It is the willingness to widely benefit all beyond personal benefits and interests.
          </p>
        </div>
      </div>
      <hr style="border 2px solid #000" />
      <div style="width: 25%;float:left;height:35px;padding-top: 15px;color: #555;">
      ' . $month.' '. $day .', '. $year . '
      </div>
      <div style="width: 75%;float:right;">

      <div style="overflow: hidden;width: 300px;height:50px;float:right;">
          <ul style="margin-left: auto;margin-right: auto;padding: 0; list-style: none;">
            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA:" href="https://www.facebook.com/EarthCitizensOrganization">LIKE <img style="width:20px;height:20px" src="https://earthcitizens.org/images/facebook.png"></a></li>
            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA" href="https://twitter.com/goearthcitizens">FOLLOW <img style="width:20px;height:20px" src="https://earthcitizens.org/images/twitter.png"></a></li>
            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA" href="#">REFER <img style="width:20px;height:20px" src="https://earthcitizens.org/images/email.png"></a></li>
          </ul>
        </div>


      </div>
      <hr/>
      <!-- <img src="https://earthcitizens.org/images/Picture2.png" style="width:100%;"> -->
    </div>
  </div>
  <div style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">

 
            <p style="font:18px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 21px">

              ' . $_POST["body"] . '
            
            </p>

  </div>


  <footer style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">
    <div style="width: 100%;">
      <hr/>
      <div style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">

        <div style="margin-left: auto;margin-right: auto;overflow: hidden;width: 400px;">
          <ul style="margin-left: auto;margin-right: auto;padding: 0; list-style: none;">
            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA" href="#">contact</a></li>
            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA" href="#">archive</a></li>
            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA" href="#">forward to a friend</a></li>
            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA" href="#">unsubscribe</a></li>
          </ul>
        </div>
      </div>
      <hr/>
      <p style="text-align: center;color: #555;">340 Jordan Rd. Sedona AZ 86336</p>
    </div>
  </footer>
</body>
</html>'
          ));

$ch2 = curl_init();
curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
curl_setopt($ch2, CURLOPT_POST, true);
curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
  'Accept: application/json',
  'Content-Type: application/json',
  'X-Postmark-Server-Token: '.$dc->config->postmark->token
  ));
curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
$response = json_decode(curl_exec($ch2), true);
$http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
curl_close($ch2);

if ($http_code!=200) 
{
  $data = array('error' => $mail->ErrorInfo);
} else 
{

   $sentlogs = new Sentlogs();
      $sentlogs->assign(array(
        'email' => $NSemail,
        'newsletterid' => $_POST['newsletterid'],
        'newslettertitle' => $_POST['title']
        ));
      if (!$sentlogs->save()) 
      {
        $data['error'] = "Something went wrong saving the data, please try again.";
      } 
      else 
      {
       $data['success'] = "Success";
     }

  }

}


}

