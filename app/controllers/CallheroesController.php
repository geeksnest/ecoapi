<?php

namespace Controllers;
use \Controllers\ControllerBase as CB;
use PHPMailer as PHPMailer;
use \Models\Callheroesonduty as Callheroesonduty;
use \Models\Callheroes as Callheroes;

class CallheroesController extends \Phalcon\Mvc\Controller {
    public function SaveHeroesonDutyAction() {
        $data = array();
        if ($_POST) {
            $page = new Callheroesonduty();
            $page->assign(array(
                'name'      => $_POST['name'],
                'email'     => $_POST['email'],
                'phone'     => $_POST['phone'],
                'gender'    => $_POST['gender'],
                'bday'      => $_POST['byear'].'-'.$_POST['bmonth'].'-'.$_POST['bday'],
                'leadtrain' => $_POST['leadtrain'],
                'skill'     => $_POST['skill'],
                'service'   => $_POST['service'],
                'commit'    => $_POST['commit'],
                'status'    => 0,
                'datesent'  => date("Y-m-d")
                ));
            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data['error'] = "Something went wrong saving the data, please try again.";
            }else {
                 $dc = new CB();
                $content ='
                 <html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
                <head>
                  <meta charset="utf-8"/>
                  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                  <title>Earth Citizens | Newsletter</title>
                  <meta name="description" content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more."/>
                  <meta name="author" content="ZURB, inc. ZURB network also includes zurb.com"/>
                  <meta name="copyright" content="ZURB, inc. Copyright (c) 2015"/>
                  <link rel="stylesheet" type="text/css" href="https://earthcitizens.org/css/fr/font-awesome.css"/>
                  <link rel="stylesheet" type="text/css" href="https://earthcitizens.org/css/fr/font-awesome.min.css"/>
                </head>
                <body style="background: #fff;color: #222;padding: 0;margin: 0;font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-weight: normal;font-style: normal;line-height: 1.5;position: relative;cursor: auto; font-size: 100%;">
                  <div style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">
                    <div style="width: 100%;">
                      <div style="padding-top: 25px;padding-bottom: 15px;overflow: hidden;">
                        <div style="width: 25%;float:left">
                          <div style="width: 200px;margin-left: auto;margin-right: auto;margin-bottom: 25px;">
                            <img src="https://earthcitizens.org/images/template_images/ecologo1.png" style="width:100%;">
                          </div>
                        </div>
                        <div style="width:50%;float:right">
                          <h2 style="font:20px/25px Helvetica,Arial,sans-serif;color:#717171;margin-bottom: 0px;">HEROES ON DUTY Request</h2>
                          <p style="text-align: justify;color: #555; margin-top: 0px;font:Arial,sans-serif;">
                           If you are a graduate of the HEROES program and desire to develop leadership and reinforce what you learned through the training, please click “HEROES on Duty” to apply for opportunities to help other and serve your community.
                          </p>
                        </div>
                      </div>
                      <hr style="border 2px solid #000" />
                      <div style=width:25%;float:left;height:35px;padding-top:15px;color:#555;font:Arial,sans-serif>Request Date: <b>'.date("Y-m-d").'</b></div>
                      <div style="width: 75%;float:right;">
                        <div style="overflow: hidden;width: 300px;height:50px;float:right;">
                          <ul style="margin-left: auto;margin-right: auto;padding: 0; list-style: none;">
                            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA;font:Arial,sans-serif;" href="https://www.facebook.com/EarthCitizensOrganization">LIKE <img style="width:20px;height:20px" src="https://earthcitizens.org/images/facebook.png"></a></li>
                            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA;font:Arial,sans-serif;" href="https://twitter.com/goearthcitizens">FOLLOW <img style="width:20px;height:20px" src="https://earthcitizens.org/images/twitter.png"></a></li>
                            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA;font:Arial,sans-serif;" href="#">REFER <img style="width:20px;height:20px" src="https://earthcitizens.org/images/email.png"></a></li>
                          </ul>
                        </div>
                      </div>
                      <hr/>
                    </div>
                  </div>
                  <div style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Name:</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                       '.$_POST['name'].'
                    </p>
                    <hr/>
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Email Address</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['email'].'
                    </p>
                    <hr/>
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Contact Phone Number:</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['phone'].'
                    </p>
                    <hr/>
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Gender</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['gender'].'
                    </p>
                    <hr/>
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Date of Birth</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['byear'].'-'.$_POST['bmonth'].'-'.$_POST['bday'].'
                    </p>
                    <hr/>
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>When did you take the HEROES leadership training?</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['leadtrain'].'
                    </p>
                    <hr/>
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Your skills or expertise</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['skill'].'
                    </p>
                    <hr/>
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>How many hours can you service for a week?</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['service'].'
                    </p>
                    <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>For how many weeks can you be committed?</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['commit'].'
                    </p>
                  </div>
                  <footer style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">
                    <div style="width: 100%;">
                      <hr/>
                      <p style="text-align: center;color: #555;">Heroes On Duty Request</p>
                    </div>
                  </footer>
                </body>
                </html>
                ';
                $mail = new PHPMailer();
                $adminemail = "info@earthcitizens.org";
                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';               
                $mail->Port = 587;

                $mail->From = 'info@earthcitizens.org';
                $mail->FromName = 'ECO Heroes on Duty';
                $mail->addAddress($adminemail);

                $mail->isHTML(true);
                $mail->Subject = 'ECO Heroes on Duty Request';
                $mail->Body = $content;
                if (!$mail->send()) {
                    $data['error'] = "Email not sent";
                } else {
                    $data['success'] = "Success";
                }
                
            }
        }
 
        echo json_encode($data);
       
    }


    public function SaveHeroesCallAction() {
        $data = array();
        if ($_POST) {
            $page = new Callheroes();
            $page->assign(array(
                'organization'  => $_POST['organization'],
                'taxid'         => $_POST['taxid'],
                'phone'         => $_POST['phone'],
                'address'       => $_POST['address'],
                'helplocaiton'  => $_POST['helplocaiton'],
                'reasonhelp'    => $_POST['reasonhelp'],
                'whyhelp'       => $_POST['whyhelp'],
                'qualification' => $_POST['qualification'],
                'status'        => 0,
                'datesent'      => date("Y-m-d")
                ));
            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $dc = new CB();
                $content ='
                <!doctype html>
                <html class=no-js lang=en data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
                <head>
                    <meta charset="utf-8" />
                    <meta name=viewport content="width=device-width, initial-scale=1.0" />
                    <title>Earth Citizens | Newsletter</title>
                    <meta name=description content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more." />
                    <meta name=author content="ZURB, inc. ZURB network also includes zurb.com" />
                    <meta name=copyright content="ZURB, inc. Copyright (c) 2015" />
                    <link rel=stylesheet type=text/css href="https://earthcitizens.org/css/fr/font-awesome.css" />
                    <link rel=stylesheet type=text/css href="https://earthcitizens.org/css/fr/font-awesome.min.css" />
                </head>
                <body style="background:#fff;color:#222;padding:0;margin:0;font-family:" Helvetica Neue ", Helvetica, Roboto, Arial, sans-serif; font-weight: normal;font-style: normal;line-height: 1.5;position: relative;cursor: auto; font-size: 100%;">
                    <div style=width:100%;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0;max-width:62.5rem>
                        <div style=width:100%>
                            <div style=padding-top:25px;padding-bottom:15px;overflow:hidden>
                                <div style=width:25%;float:left>
                                    <div style=width:200px;margin-left:auto;margin-right:auto;margin-bottom:25px><img src=https://earthcitizens.org/images/template_images/ecologo1.png style=width:100%></div>
                                </div>
                                <div style=width:50%;float:right>
                                    <h2 style="font:20px/25px Helvetica,Arial,sans-serif;color:#717171;margin-bottom:0">Call a Hero Request</h2>
                                    <p style=text-align:justify;color:#555;margin-top:0;font:Arial,sans-serif>If you are a non-profit organization and need HEROES’ help, please click the “Call HEROES” button below to submit your information, and we will try to find HEROES in your area to help.</p>
                                </div>
                            </div>
                            <hr style="border 2px solid #000" />
                            <div style=width:25%;float:left;height:35px;padding-top:15px;color:#555;font:Arial,sans-serif>Request Date: <b>'.date("Y-m-d").'</b></div>
                            <div style=width:75%;float:right>
                                <div style=overflow:hidden;width:300px;height:50px;float:right>
                                    <ul style=margin-left:auto;margin-right:auto;padding:0;list-style:none>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=https://www.facebook.com/EarthCitizensOrganization>LIKE <img style=width:20px;height:20px src=https://earthcitizens.org/images/facebook.png></a></li>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=https://twitter.com/goearthcitizens>FOLLOW <img style=width:20px;height:20px src=https://earthcitizens.org/images/twitter.png></a></li>
                                        <li style=list-style:none;float:left;margin-left:1.375rem;display:block><a style=text-decoration:none;color:#008CBA;font:Arial,sans-serif href=#>REFER <img style=width:20px;height:20px src=https://earthcitizens.org/images/email.png></a></li>
                                    </ul>
                                </div>
                            </div>
                            <hr/>
                        </div>
                    </div>
                    <div style=width:100%;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0;max-width:62.5rem>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Name of Organization:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['organization'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Organization’s Tax ID:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['taxid'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Contact Phone Number:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['phone'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Official Address:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['address'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Help Location:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['helplocaiton'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Reasons for needing help:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['reasonhelp'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>When you need help:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['whyhelp'].'</p>
                        <hr/>
                        <label style="font:16px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Desired qualification for helpers:</b></label>
                        <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0">'.$_POST['qualification'].'</p>
                    </div>
                    <footer style=width:100%;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0;max-width:62.5rem>
                        <div style=width:100%>
                            <hr/>
                            <p style=text-align:center;color:#555>Call a Hero Request</p>
                        </div>
                    </footer>
                </body>
                </html>
                ';
                $mail = new PHPMailer();
                $adminemail = "info@earthcitizens.org";
                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';               
                $mail->Port = 587;

                $mail->From = 'info@earthcitizens.org';
                $mail->FromName = 'ECO Heroes On Call';
                $mail->addAddress($adminemail);

                $mail->isHTML(true);
                $mail->Subject = 'Heroes On Call Request';
                $mail->Body = $content;
                if (!$mail->send()) {
                    $data['error'] = "Email not sent";
                } else {
                    $data['success'] = "Success";
                }
                // $json = json_encode(array(
                //     'From' => $dc->config->postmark->signature,
                //     'To' => 'heroesonduty@mailinator.com',
                //     'Subject' => 'Heroes On Call Request',
                //     'HtmlBody' => $content
                //     ));
                // $ch2 = curl_init();
                // curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                // curl_setopt($ch2, CURLOPT_POST, true);
                // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                // curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                //     'Accept: application/json',
                //     'Content-Type: application/json',
                //     'X-Postmark-Server-Token: '.$dc->config->postmark->token
                //     ));
                // curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                // $response = json_decode(curl_exec($ch2), true);
                // $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                // curl_close($ch2);
                // if ($http_code!=200) {
                //     $data['error'] = $mail->ErrorInfo;
                // } else {
                //    $data['success'] = "Success";
                // }

            }
        }
        echo json_encode($data);
    }




    public function manageHeroesCallAction($num, $page, $keyword, $sort, $sortto, $datefrom, $dateto) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;
        if ($keyword == 'null' || $keyword == 'undefined') {
            $conditions = "SELECT * FROM callheroes ";
        }else {
            $conditions = "SELECT * FROM callheroes WHERE 
            organization LIKE '%". $keyword ."%' 
            or taxid LIKE '%". $keyword ."%'
            or phone LIKE '%". $keyword ."%'";
        }
        if($datefrom!= "null" && $dateto != "null"){
           $conditions .= "WHERE datesent >= '$datefrom' AND datesent <= '$dateto' ";
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;
        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");
        $count = $app->dbSelect($conditions);
        echo json_encode(array(
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count)
            )); 
    }

    public function getinfoHeroesCallAction($id) {
        $app = new CB();
        $conditions = "SELECT * FROM callheroes WHERE id='".$id."' ";
        $searchresult = $app->dbSelect($conditions);
        //UPDATE Status review
        $_update = Callheroes::findFirst('id=' . $id . ' ');
        $_update->status = 1;
        if (!$_update->save()) {
            $data['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";

        }
        echo json_encode($searchresult); 
    }

     public function deleteHeroesCallAction($id) {
        $pages =  Callheroes::findFirst('id=' . $id . ' ');
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }



    public function manageHeroesonDutyAction($num, $page, $keyword, $sort, $sortto, $datefrom, $dateto) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;
        if ($keyword == 'null' || $keyword == 'undefined') {
            $conditions = "SELECT * FROM callheroesonduty ";

        } else {
            $conditions = "SELECT * FROM callheroesonduty WHERE 
            name LIKE '%". $keyword ."%' 
            or  email LIKE '%". $keyword ."%'
            or  phone LIKE '%". $keyword ."%'";
        }


        if($datefrom!= "null" && $dateto != "null"){
           $conditions .= "WHERE datesent >= '$datefrom' AND datesent <= '$dateto' ";
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;
        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");
        $count = $app->dbSelect($conditions);
        echo json_encode(array(
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count)
            )); 
    }


    public function getinfoheroesoncallAction($id){
        $app = new CB();
        $conditions = "SELECT * FROM callheroesonduty WHERE id='".$id."' ";
        $searchresult = $app->dbSelect($conditions);
        //UPDATE Status review
        $_update = Callheroesonduty::findFirst('id='.$id .'');
        $_update->status = 1;
        if (!$_update->save()) {
            $data['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";

        }
       echo json_encode($searchresult); 
    }

    public function deleteheroesoncallAction($id) {
        $pages =  Callheroesonduty::findFirst('id=' . $id . ' ');
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

}
