<?php

namespace Controllers;

use \Models\Centername as Centername;
use PHPMailer as PHPMailer;
use MailChimp as MailChimp;
use \Controllers\ControllerBase as CB;

class CentersController extends \Phalcon\Mvc\Controller {

    public function createCenterAction() {
        $data = array();
        if ($_POST) {

            $cn = new Centername();
            $cn->centername = $_POST['centername'];
            $cn->date_created = date("Y-m-d H:i:s");
            $cn->date_updated = date("Y-m-d H:i:s");

            if (!$cn->save()) {
                $errors = array();
                foreach ($cn->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }   

    public function centerListAction($page, $keyword, $sortto) {

        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {

            $conditions = "SELECT * FROM centername ";

        } else {

            $conditions = "SELECT * FROM centername WHERE centername LIKE '%". $keyword ."%' ";
            
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY centername DESC";
        }else{
            $sortby = "ORDER BY centername ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count)
            )); 
    }

    public function getCenterAction($id) {
        $app = new CB();
        $conditions = "SELECT * FROM centername WHERE id='".$id."'";
        $data = $app->dbSelect($conditions);
        echo json_encode($data);
    }

    public function updateCenterAction() {
        $data = array();
        if ($_POST) {
            $conditions = 'id="'.$_POST['id'].'"';
            $cn = Centername::findFirst($conditions);
            $cn->centername = $_POST['centername'];
            $cn->date_updated = date("Y-m-d H:i:s");

            if (!$cn->save()) {
                $errors = array();
                foreach ($cn->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }

    public function deleteCenterAction($id) {
        $conditions = "id=" . $id;
        $delete = Centername::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($delete) {
            if ($delete->delete()) {
                $data = array('success' => 'Center Deleted');
            }
        }
        echo json_encode($data);
    }

    public function centernamesAction() {
        $app = new CB();
        $conditions = "SELECT * FROM centername ORDER BY centername ASC";
        $data = $app->dbSelect($conditions);
        echo json_encode($data);
    }

}
