-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2015 at 09:32 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `newsimage`
--

CREATE TABLE IF NOT EXISTS `newsimage` (
`id` int(11) NOT NULL,
  `generateid` text NOT NULL,
  `description` text,
  `path` text NOT NULL,
  `title` text
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsimage`
--

INSERT INTO `newsimage` (`id`, `generateid`, `description`, `path`, `title`) VALUES
(4, '9f081cd42f27bab4c2000aa03ca34c6d', 'Description Here', '1d304af4ca520f77437ef8d51737fed8.jpg', 'Title Here'),
(5, 'a27ccfe5794cbbfcdf702a5ff115b590', 'Description Here', '90e9488326a442b9c087a6e8039997cd.jpg', 'Title Here'),
(6, 'be41664a508d55711f0add66652e991a', 'Description Here', '497b167c9c7904905ca4ba7c648da769.jpg', 'Title Here'),
(7, '111c76cb29e5ef48088cfc772115d85c', 'Description Here', 'cab05e895455878ebb97cfca8d8d2918.jpg', 'Title Here'),
(8, '0c479ad6ee53b42efa31f6ad926909ee', 'Description Here', 'e3d6266b339eb2b966a325d1f642879f.jpg', 'Title Here'),
(9, '01d401bd1c2b77c143a8ca42b77f4735', 'Description Here', 'a657522134bb7e8512dcf144a1207cd1.jpg', 'Title Here'),
(10, '36c75a64c1eaf8395ab6492d122046c0', 'Description Here', '02e13bf003a685c3f1f74140679e2ace.jpg', 'Title Here'),
(11, '5fbfccc6fe36303b0cd414a45a03c720', 'Description Here', '8e2a23c03822138dc086028ad783cbc6.jpg', 'Title Here'),
(12, '6aa980021bc498dc8de2c0e758d7c580', 'Description Here', '94ce0ea39e80e9c75bfa5ead0ef2bee4.jpg', 'Title Here');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `newsimage`
--
ALTER TABLE `newsimage`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `newsimage`
--
ALTER TABLE `newsimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
