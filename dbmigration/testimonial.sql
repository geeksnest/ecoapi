-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2015 at 04:48 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE IF NOT EXISTS `testimonial` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `comporg` text NOT NULL,
  `message` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `name`, `email`, `comporg`, `message`, `status`) VALUES
(2, 'dfasd', 'dsafdsf', 'dsf', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures, and ideologies have become intractable, humanity urgently needs to rise above these small problems and conflicts to become one in our concern for the Earth. Peace will come when we collectively recognize that the Earth''s fate will determine humanity''s fate, and we come to hold the Earth as our shared concern and value.', 'fsdafsdf'),
(3, 'adsfsadf', 'sdfsadf', 'dsfdsa', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures, and ideologies have become intractable, humanity urgently needs to rise above these small problems and conflicts to become one in our concern for the Earth. Peace will come when we collectively recognize that the Earth''s fate will determine humanity''s fate, and we come to hold the Earth as our shared concern and value.', 'dafsadf'),
(4, 'dsafsda', 'fsdaf', 'dsafsda', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures, and ideologies have become intractable, humanity urgently needs to rise above these small problems and conflicts to become one in our concern for the Earth. Peace will come when we collectively recognize that the Earth''s fate will determine humanity''s fate, and we come to hold the Earth as our shared concern and value.', 'dsafsd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
