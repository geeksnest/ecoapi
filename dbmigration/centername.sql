-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2015 at 02:41 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `centername`
--

CREATE TABLE IF NOT EXISTS `centername` (
`id` int(11) NOT NULL,
  `centername` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `centername`
--

INSERT INTO `centername` (`id`, `centername`) VALUES
(1, 'AZ - Glendale'),
(2, 'AZ - Scottsdale'),
(3, 'AZ - Sun City'),
(4, 'AZ - Tempe'),
(5, 'CA - Anaheim Hills'),
(6, 'CA - Brea'),
(7, 'CA - Burbank'),
(8, 'CA - Cerritos'),
(9, 'CA - Chatsworth'),
(10, 'CA - Escondido'),
(11, 'CA - Fremont'),
(12, 'CA - Garden Grove'),
(13, 'CA - Glendale'),
(14, 'CA - Irvine'),
(15, 'CA - Monrovia'),
(16, 'CA - Oceanside'),
(17, 'CA - Pasadena'),
(18, 'CA - Rolling Hills'),
(19, 'CA - San Mateo'),
(20, 'CA - San Ramon'),
(21, 'CA - Santa Clara'),
(22, 'CA - Tarzana'),
(23, 'CA - Torrance'),
(24, 'CA - Valley'),
(25, 'CA - Westchester'),
(26, 'CA - Wilshire'),
(27, 'CO - Denver'),
(28, 'CO - Golden'),
(29, 'CO - Kipling'),
(30, 'CO - Littleton'),
(31, 'CO - Westminster'),
(32, 'DC - Washington DC'),
(33, 'FL - Miami'),
(34, 'FL - Pinecrest'),
(35, 'GA - Buckhead'),
(36, 'GA - Duluth'),
(37, 'GA - Peachtree Buckhead'),
(38, 'GA - Roswell'),
(39, 'HI - Aiea'),
(40, 'HI - Honolulu'),
(41, 'HI - Kaimuki'),
(42, 'IL - Bloomingdale'),
(43, 'IL - Glen Ellyn'),
(44, 'IL - Libertyville'),
(45, 'IL - Mt. Prospect'),
(46, 'IL - Northbrook'),
(47, 'IL - Oak Park'),
(48, 'IL - Orland Park'),
(49, 'IL - Skokie'),
(50, 'IL - St. Charles'),
(51, 'IL - Westmont'),
(52, 'MD - Aspen Hill'),
(53, 'MD - Beltsville'),
(54, 'MD - Bethesda'),
(55, 'MD - Gaithersburg'),
(56, 'MD - Rockville'),
(57, 'MA - Andover'),
(58, 'MA - Arlington'),
(59, 'MA - Brookline'),
(60, 'MA - Cambridge'),
(61, 'MN - Maple Grove'),
(62, 'NV - Durango'),
(63, 'NV - Henderson'),
(64, 'NV - Montecito'),
(65, 'NV - North Las Vegas'),
(66, 'NV - Rainbow'),
(67, 'NJ - CGI'),
(68, 'NJ - Madison'),
(69, 'NJ - Ramsey'),
(70, 'NJ - Ridgefield'),
(71, 'NJ - Wyckoff'),
(72, 'NM - Albuquerque'),
(73, 'NM - Cottonwood'),
(74, 'NM - Downtown Albuquerque'),
(75, 'NM - Santa Fe'),
(76, 'NY - Babylon Village'),
(77, 'NY - Bay Ridge'),
(78, 'NY - Bronx'),
(79, 'NY - Brooklyn Heights'),
(80, 'NY - East Meadow'),
(81, 'NY - Flushing'),
(82, 'NY - Forest Hills'),
(83, 'NY - Franklin Square'),
(84, 'NY - Great Neck'),
(85, 'NY - Kew Gardens Hills'),
(86, 'NY - Lynbrook'),
(87, 'NY - Manhattan'),
(88, 'NY - Mineola'),
(89, 'NY - New City'),
(90, 'NY - New Rochelle'),
(91, 'NY - Park East'),
(92, 'NY - Smithtown'),
(93, 'NY - Sunnyside'),
(94, 'NY - Syosset'),
(95, 'NY - Union Square'),
(96, 'NY - Westchester'),
(97, 'NC - Raleigh'),
(98, 'OR - Beaverton'),
(99, 'OR - Portland'),
(100, 'OR - West Linn'),
(101, 'TX - Belt Line'),
(102, 'TX - Champion'),
(103, 'TX - Copperfield'),
(104, 'TX - Katy'),
(105, 'TX - Kingwood'),
(106, 'TX - Missouri City'),
(107, 'TX - The Woodlands'),
(108, 'TX - Voss'),
(109, 'TX - Westpark'),
(110, 'VA - Alexandria'),
(111, 'VA - Burke'),
(112, 'VA - Centreville'),
(113, 'VA - Vienna'),
(114, 'WA - Everett'),
(115, 'WA - Kirkland'),
(116, 'WA - Lynnwood'),
(117, 'WA - Mill Creek'),
(118, 'WA - Ravenna Park'),
(119, 'WA - Tacoma');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `centername`
--
ALTER TABLE `centername`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `centername`
--
ALTER TABLE `centername`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=120;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
