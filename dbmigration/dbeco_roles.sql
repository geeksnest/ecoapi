CREATE DATABASE  IF NOT EXISTS `dbeco` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbeco`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: dbeco
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `roleCode` varchar(200) NOT NULL,
  `roleDescription` varchar(255) NOT NULL,
  `rolePage` varchar(255) NOT NULL,
  `roleGroup` varchar(255) NOT NULL,
  PRIMARY KEY (`roleCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES ('calendarrole','Calendar','addcalendar,viewcalendar','calendar'),('donationsrole','Donations','donationlist, bnbregistration','donation'),('imagesrole','Sliders','create, create_album, manage_album,edit_album','sliders'),('newslettersrole','News Letter','addsubscriber,subscriberslist,editsubscriber,createnewsletter,managenewsletter, editnewsletter, sendnewsletter','subscribers'),('newsrole','News','createnews, managenews, editnews, category','news'),('pagesrole','Pages','managepages,createpage,editpage','pages'),('pearmarksrole','Peacemarks','create, managepeacemarks, edit','peacemark'),('projectsrole','Projects','createfeaturedproject,managefeaturedproject,editfeaturedproject','featuredprojects'),('proposalesrole','Proposals','createproposals, manageproposals','proposals'),('testimonialsrole','Testimonials','index, readtestimonial,edit','testimonials'),('usersrole','Users','create, createSave, list, members','users');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-25 21:57:32
