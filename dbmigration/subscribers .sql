-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2015 at 10:49 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
`NMSid` int(11) NOT NULL,
  `NMSemail` varchar(300) NOT NULL,
  `NMSstat` int(11) NOT NULL,
  `NMSdate` date NOT NULL,
  `NMSstatTXT` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`NMSid`, `NMSemail`, `NMSstat`, `NMSdate`, `NMSstatTXT`) VALUES
(1, 'jimmy@gmail.com', 1, '2015-01-25', 'subscriber'),
(2, 'NMSemail@gmail.com', 1, '2015-01-25', 'subscriber'),
(4, 'jack@gmail.com', 1, '2015-01-25', 'subscriber'),
(5, 'abk@email.com', 0, '2015-02-05', 'non-subscriber'),
(6, 'asd@asd.com', 1, '2015-02-03', 'subscriber'),
(8, 'xyz@xyz.com', 1, '2015-02-03', 'subscriber'),
(11, 'test3@test.com', 1, '2015-02-03', 'subscriber'),
(13, 'test5@test.com', 1, '2015-02-03', 'subscriber'),
(14, 'jimboy@gmail.com', 1, '2015-02-04', 'subscriber'),
(16, '123@mail.com', 1, '2015-02-05', 'subscriber'),
(17, 'asdf@mail.com', 1, '2015-02-05', 'subscriber'),
(18, 'abkd@mail.com', 1, '2015-02-05', 'subscriber');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
 ADD PRIMARY KEY (`NMSid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
MODIFY `NMSid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
