-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 20, 2015 at 05:22 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE IF NOT EXISTS `api` (
  `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `public_id` char(64) NOT NULL DEFAULT '',
  `private_key` char(64) NOT NULL DEFAULT '',
  `status` enum('ACTIVE','INACTIVE') DEFAULT 'ACTIVE',
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `private_key` (`private_key`),
  UNIQUE KEY `public_id` (`public_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `api`
--

INSERT INTO `api` (`client_id`, `public_id`, `private_key`, `status`) VALUES
(1, '', '593fe6ed77014f9507761028801aa376f141916bd26b1b3f0271b5ec3135b989', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE IF NOT EXISTS `donation` (
  `amount` float DEFAULT NULL,
  `usercount` float DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`amount`, `usercount`, `id`) VALUES
(2646, 233, 1);

-- --------------------------------------------------------

--
-- Table structure for table `memberconfirmation`
--

CREATE TABLE IF NOT EXISTS `memberconfirmation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `members_id` varchar(150) DEFAULT NULL,
  `members_code` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `memberconfirmation`
--

INSERT INTO `memberconfirmation` (`id`, `members_id`, `members_code`) VALUES
(1, '34', 'f1f836cb4ea6efb2a0b1b99f41ad8b103eff4b59'),
(2, '35', '972a67c48192728a34979d9a35164c1295401b71'),
(3, '36', 'fc074d501302eb2b93e2554793fcaf50b3bf7291'),
(4, '37', 'cb7a1d775e800fd1ee4049f7dca9e041eb9ba083'),
(5, '38', '5b384ce32d8cdef02bc3a139d4cac0a22bb029e8'),
(6, '2', 'da4b9237bacccdf19c0760cab7aec4a8359010b0'),
(7, '3', '77de68daecd823babbb58edb1c8e14d7106e83bb'),
(8, '4', '1b6453892473a467d07372d45eb05abc2031647a'),
(9, '5', 'ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4'),
(10, '6', 'c1dfd96eea8cc2b62785275bca38ac261256e278'),
(11, '7', '902ba3cda1883801594b6e1b452790cc53948fda'),
(12, '8', 'fe5dbbcea5ce7e2988b8c69bcfdfde8904aabc1f'),
(13, '9', '0ade7c2cf97f75d009975f4d720d1fa6c19f4897'),
(14, '10', 'b1d5781111d84f7b3fe45a0852e59758cd7a87e5'),
(15, '11', '17ba0791499db908433b80f37c5fbc89b870084b'),
(16, '12', '7b52009b64fd0a2a49e6d8a939753077792b0554'),
(18, '14', 'fa35e192121eabf3dabf9f5ea6abdbcbc107ac3b'),
(19, '15', 'f1abd670358e036c31296e66b3b66c382ac00812'),
(20, '16', '1574bddb75c78a6fd2251d61e2993b5146201319'),
(21, '17', '0716d9708d321ffb6a00818614779e779925365c'),
(22, '18', '9e6a55b6b4563e652a23be9d623ca5055c356940'),
(23, '19', 'b3f0c7f6bb763af1be91d9e74eabfeb199dc1f1f'),
(24, '63', 'a17554a0d2b15a664c0e73900184544f19e70227'),
(25, '64', 'c66c65175fecc3103b3b587be9b5b230889c8628'),
(26, '65', '2a459380709e2fe4ac2dae5733c73225ff6cfee1');

-- --------------------------------------------------------

--
-- Table structure for table `queryError`
--

CREATE TABLE IF NOT EXISTS `queryError` (
  `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `query` text,
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_string` varchar(1024) DEFAULT '',
  `error_no` int(10) unsigned DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `execution_script` varchar(1024) DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`error_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `roleCode` varchar(200) NOT NULL,
  `roleDescription` varchar(255) NOT NULL,
  `rolePage` varchar(255) NOT NULL,
  PRIMARY KEY (`roleCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`roleCode`, `roleDescription`, `rolePage`) VALUES
('calendarrole', 'Calendar', 'addcalendar,viewcalendar'),
('donationsrole', 'Donations', 'donationlist, bnbregistration'),
('imagesrole', 'Sliders', 'create, create_album, manage_album,edit_album'),
('newslettersrole', 'News Post', 'addsubscriber,subscriberslist,editsubscriber,createnewsletter,managenewsletter, editnewsletter, sendnewsletter'),
('newsrole', 'User Management', 'createnews, managenews, editnews, category'),
('pagesrole', 'Pages', 'managepages,createpage,editpage'),
('pearmarksrole', 'Peacemarks', 'create, managepeacemarks, edit'),
('projectsrole', 'Projects', 'createfeaturedproject,managefeaturedproject,editfeaturedproject'),
('proposalesrole', 'Proposals', 'createproposals, manageproposals'),
('testimonialsrole', 'Testimonials', 'index, readtestimonial,edit'),
('usersrole', 'Announcements', 'create, createSave, list, members');

-- --------------------------------------------------------

--
-- Table structure for table `runtimeError`
--

CREATE TABLE IF NOT EXISTS `runtimeError` (
  `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(2048) NOT NULL DEFAULT '',
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_type` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `server_name` varchar(100) DEFAULT NULL,
  `execution_script` varchar(1024) NOT NULL DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`error_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE IF NOT EXISTS `userroles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `userRoles` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(250) NOT NULL,
  `hdyha` varchar(250) NOT NULL,
  `referal` varchar(150) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(150) NOT NULL,
  `birthday` varchar(150) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `state` varchar(20) NOT NULL,
  `country` varchar(100) NOT NULL,
  `agentType` varchar(50) NOT NULL,
  `userLevel` varchar(50) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `email`, `password`, `hdyha`, `referal`, `firstname`, `lastname`, `birthday`, `gender`, `state`, `country`, `agentType`, `userLevel`) VALUES
(1, 'superagent', 'efrenbautistajr@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', '', '', 'Dick', 'Yanson', '0000-00-00 00:00:00', '', '', '', '', '1'),
(2, 'asdfas', 'adfasdfefrenbautistajr@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'asdgasdg', 'superagent', 'asdgasdg', 'agdasdg', 'Fri Jan 30 2015 00:00:00 GMT+0800 (PHT)', 'Female', 'HI', 'country', 'agent', 'active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
