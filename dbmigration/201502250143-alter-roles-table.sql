ALTER TABLE `dbeco`.`roles` 
ADD COLUMN `roleGroup` VARCHAR(255) NOT NULL AFTER `rolePage`;

UPDATE `dbeco`.`roles` SET `roleGroup`='calendar' WHERE `roleCode`='calendarrole';
UPDATE `dbeco`.`roles` SET `roleGroup`='donation' WHERE `roleCode`='donationsrole';
UPDATE `dbeco`.`roles` SET `roleGroup`='sliders' WHERE `roleCode`='imagesrole';
UPDATE `dbeco`.`roles` SET `roleDescription`='News Letter', `roleGroup`='subscribers' WHERE `roleCode`='newslettersrole';
UPDATE `dbeco`.`roles` SET `roleDescription`='News', `roleGroup`='news' WHERE `roleCode`='newsrole';
UPDATE `dbeco`.`roles` SET `roleGroup`='pages' WHERE `roleCode`='pagesrole';
UPDATE `dbeco`.`roles` SET `roleGroup`='peacemark' WHERE `roleCode`='pearmarksrole';
UPDATE `dbeco`.`roles` SET `roleGroup`='featuredprojects' WHERE `roleCode`='projectsrole';
UPDATE `dbeco`.`roles` SET `roleGroup`='proposals' WHERE `roleCode`='proposalesrole';
UPDATE `dbeco`.`roles` SET `roleGroup`='testimonials' WHERE `roleCode`='testimonialsrole';
UPDATE `dbeco`.`roles` SET `roleDescription`='Users', `roleGroup`='users' WHERE `roleCode`='usersrole';
