-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2015 at 07:31 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
`id` int(11) NOT NULL,
  `generateid` text NOT NULL,
  `description` text,
  `path` text NOT NULL,
  `title` text,
  `foldername` text NOT NULL,
  `folderid` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `generateid`, `description`, `path`, `title`, `foldername`, `folderid`) VALUES
(1, 'df58e058c9bb50414f6b42efbb95a78f', 'dalawang agila', 'be439474ac394172894392ff026623fd.jpg', 'agila', 'test', '457f40e5efe41023af667bd08776f909'),
(2, 'eb3bc06ebb89992871b75c1a682d3a1d', 'mga kannaway', '3b13e0d3efd33387d7baff5a3b5287ce.jpg', 'kannaway', 'test', '457f40e5efe41023af667bd08776f909'),
(3, 'ef96804caee41b7c37b3de3f54838687', 'mga langgam', '57c86e351aa49a7b944a1fc8a9d2868a.jpg', 'langgam', 'test', '457f40e5efe41023af667bd08776f909'),
(4, '5a3a6be72261284cca8de4bc71466541', 'pawikan at isda', 'ce207040d7bf16fc08cecf06d0e60ba7.jpg', 'pawikan', 'test', '457f40e5efe41023af667bd08776f909'),
(9, '774de2dc2d559161c88857a93a4e9a41', 'asd', '4570e6f9f9279caf678b0e8e54dfd942.jpg', 'asd', '3rd_album', '8990bada69690e37940c7ed8df053458'),
(10, '32d522e391127f193ef27756aa024184', 'asd', '7b244336a24366eddba9069a8b373c10.jpg', 'asd', '3rd_album', '8990bada69690e37940c7ed8df053458'),
(11, '897e9a36ae2e0d8e9eba60848dc599a7', 'asd', 'e70c100f976f4624d124a97c87582db9.jpg', 'asd', '3rd_album', '8990bada69690e37940c7ed8df053458'),
(12, 'b92478bfeb15c127b9f9fa084360bcbb', 'asd', '330287882e4a3d8abd3a84221c12a69d.jpg', 'asd', '3rd_album', '8990bada69690e37940c7ed8df053458'),
(13, 'b66dd04410b83e19e379945a7795fa70', 'asd', '4e7a899093071f328ffddfd70d544785.jpg', 'asd', '3rd_album', '8990bada69690e37940c7ed8df053458'),
(14, 'f9e6932a3f4d0350f1315116ef1d63ed', 'asd', '6ef101b8ca3042c53627d24a44dcb933.jpg', 'asd', '1st_album', '5808371544e23889bec7992f5e558212'),
(15, '2253c7ae44ac0f913e8cc9de4ce31645', 'asd', '6dcab864022881faa8050b40a8ef2e7c.jpg', 'asd', '1st_album', '5808371544e23889bec7992f5e558212'),
(16, '9b03575aa212c77023a76d85d6db923f', 'asd', '06581e40baef43943188f6aef6da8dd6.jpg', 'asd', '1st_album', '5808371544e23889bec7992f5e558212'),
(17, 'fbb3dac722052cd5f62c0cddb972deba', 'asd', 'a0c207e2b8376a3a3d82f07e5e6b993b.jpg', 'asd', '1st_album', '5808371544e23889bec7992f5e558212'),
(18, 'dfbdfa252e61d93c1615c3e64e297aa0', 'asd', 'fd45dd6e40c4b0eaf653661aedf1e889.jpg', 'asd', '1st_album', '5808371544e23889bec7992f5e558212'),
(19, '5b7bec710ff1cb7fed6b09a059efab0c', 'asd', 'dd3f3e4ee7b8ed10f12382b95c45e966.jpg', 'asd', '1st_album', '5808371544e23889bec7992f5e558212'),
(20, '2abd18ac26baf540ea8780f346bf266d', 'asd', '0f67423f71986ea5c0d6e1f081882986.jpg', 'asd', 'eco project', '811f12f26f3396ca6ca5e78e322ffb09'),
(21, 'a0f4bb995ffb321e43a7c6181328086e', 'asd', 'fc386815cc7285e727eb7b6d9c663cb1.jpg', 'asd', 'eco project', '811f12f26f3396ca6ca5e78e322ffb09'),
(22, '6e755f274bd0dbf01ba0b9003ba572d8', 'asd', 'dcfb7154bff9bbde33c2b3fa988c9bab.png', 'asd', 'eco project', '811f12f26f3396ca6ca5e78e322ffb09'),
(23, '86b41c8b7bf7a0f2ccbe0d6f759265d8', 'asd', 'c69d44aaabc8774ad081bbf2b9014ca0.jpg', 'asd', 'eco project', '811f12f26f3396ca6ca5e78e322ffb09'),
(24, '64c956dc6f1e9c63f5bbfd6ea73a415e', 'asd', '9722aab3a0ecac745c9f7eb5ccb9e7c1.jpg', 'asd', 'eco project', '811f12f26f3396ca6ca5e78e322ffb09'),
(25, '9acdfac706af6f16272924feebc80266', 'asd', '5e0eebc32fc008b1c3c7d49d7df8866e.jpg', 'asd', 'eco project', '811f12f26f3396ca6ca5e78e322ffb09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image`
--
ALTER TABLE `image`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
