-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2015 at 11:16 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `donationlog`
--

CREATE TABLE IF NOT EXISTS `donationlog` (
`id` int(11) NOT NULL,
  `useremail` varchar(150) DEFAULT NULL,
  `transactionId` varchar(45) DEFAULT NULL,
  `datetimestamp` datetime DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `paymentmode` varchar(255) NOT NULL,
  `forcheckmode` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donationlog`
--

INSERT INTO `donationlog` (`id`, `useremail`, `transactionId`, `datetimestamp`, `amount`, `paymentmode`, `forcheckmode`) VALUES
(1, 'hellohello99999@gmail.com', '1WR93681JJ482222V', '2014-12-04 18:58:57', 10, '', ''),
(2, 'taradahnd@gmail.com', '2N9251755E525673S', '2014-12-04 19:01:25', 10, '', ''),
(3, 'itshappyday@gmail.com', '074952768A461402A', '2014-12-04 19:02:59', 10, '', ''),
(4, '1210mk@gmail.com', '1BS31995D2338012B', '2014-12-04 19:03:57', 10, '', ''),
(5, 'Michelle1.cgi@gmail.com', '6B347225TW7919144', '2014-12-04 19:05:10', 10, '', ''),
(6, 'manioh@gmail.com', '4Y4346464J496301T', '2014-12-04 19:05:35', 10, '', ''),
(7, 'healingfamily@ilchi.com', '7TH335118U8464802', '2014-12-04 19:09:57', 10, '', ''),
(8, 'helenn32@gmail.com', '92X164488H402764E', '2014-12-04 19:52:53', 10, '', ''),
(9, 'estrellamj@gmail.com', '7SK5401537484374L', '2014-12-04 20:39:59', 10, '', ''),
(10, 'sayongk@gmail.com', '2H919332XD8032135', '2014-12-04 20:44:31', 10, '', ''),
(11, 'hyeonjakim@gmail.com', '80185600WK9591822', '2014-12-04 20:47:07', 10, '', ''),
(12, 'gnshin1050@gmail.com', '05X707015K216633Y', '2014-12-04 20:51:30', 10, '', ''),
(13, 'estrellamj@gmail.com', '7SK5401537484374L', '2014-12-04 20:53:12', 10, '', ''),
(14, 'jjane441@yahoo.com', '2SA768470U863325Y', '2014-12-09 05:18:42', 10, '', ''),
(15, 'TaoTravis@gmail.com', '69273561KS494314S', '2014-12-09 06:46:03', 10, '', ''),
(16, 'iqbalsingh91@yahoo.com', '04H409849H670553V', '2014-12-09 20:24:40', 10, '', ''),
(17, 'iqbalsingh91@yahoo.com', '04H409849H670553V', '2014-12-09 20:24:56', 10, '', ''),
(18, 'iqbalsingh91@yahoo.com', '04H409849H670553V', '2014-12-10 17:14:33', 10, '', ''),
(19, 'tarzana@bodynbrain.com', '5B171836V22934315', '2014-12-10 20:31:13', 10, '', ''),
(20, 'kristy4wellness@me.com', '34K93534996111916', '2014-12-14 03:26:26', 10, '', ''),
(21, 'Haejung.jung@gmail.com', '7KF78658UW569224Y', '2014-12-14 06:46:30', 10, '', ''),
(22, 'Rebecca.tinkle@yahoo.com', '54N94129CX609243M', '2014-12-14 08:32:00', 10, '', ''),
(23, 'hellomichela@aol.com', '76U221253L909741C', '2014-12-14 08:45:49', 10, '', ''),
(24, 'tonyasabumnim@yahoo.com', '7F975053DU485813D', '2014-12-15 00:56:52', 10, '', ''),
(25, 'Haejung.jung@gmail.com', '7KF78658UW569224Y', '2014-12-15 02:56:44', 10, '', ''),
(38, 'efrenbautistajr@yahoo.com.ph', '8SF15793M4937725F', '2014-12-20 07:37:53', 10, '', ''),
(39, 'onion012@hotmail.com', '64G19170FN793223Y', '2014-12-22 22:33:17', 10, '', ''),
(40, 'dbeal@powerbrainedu.com', '0000000000000', '2014-12-24 00:00:00', 20, '', ''),
(41, 'kyujongsmile@gmail.com', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(42, 'dbeal@powerbrainedu.com', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(43, 'balgmi2010@gmail.com ', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(44, 'truth.nobuko@gmail.com ', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(45, 'mokhwasung@gmail.com', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(46, 'hcpb100@yahoo.com ', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(47, 'jjane441@yahoo.com', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(48, 'dahnottogi0808@gmail.com', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(49, 'jh77071@gmail.com ', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(50, 'notregistered', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(51, 'notregistered', '0000000000000', '2014-12-24 00:00:00', 10, '', ''),
(52, 'notregistered', '0000000000000', '2014-12-24 00:00:00', 75, '', ''),
(53, 'mail@jordandiamond.com', '4U873714CX730601L', '2014-12-26 01:04:18', 10, '', ''),
(54, 'piaceniza@yahoo.com', '2S919904S3855501D', '2014-12-27 17:36:23', 10, '', ''),
(55, 'piaceniza@yahoo.com', '2S919904S3855501D', '2014-12-27 17:36:28', 10, '', ''),
(56, '', '3J614663WX432180Y', '2014-12-29 22:24:36', 10, '', ''),
(57, '', '4W046551XF844091R', '2014-12-29 22:26:03', 10, '', ''),
(58, '', '358727972C7552223', '2014-12-29 22:30:31', 10, '', ''),
(59, '', '9JM90966KY0413252', '2014-12-30 17:16:08', 30, '', ''),
(60, '', '14X34209W8258394P', '2014-12-30 17:17:37', 30, '', ''),
(61, '', '34L88713WS013870A', '2014-12-31 23:38:49', 300, '', ''),
(63, 'jimmyblantanojr@gmail.com', '', '2015-02-13 08:41:43', 50, 'Cash', '--'),
(65, '', '', '2015-02-13 10:05:17', NULL, 'Cash', '--');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `donationlog`
--
ALTER TABLE `donationlog`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `donationlog`
--
ALTER TABLE `donationlog`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
