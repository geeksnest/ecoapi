-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2015 at 06:30 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `proposals`
--

CREATE TABLE IF NOT EXISTS `proposals` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `proposals_body` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `unread` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proposals`
--

INSERT INTO `proposals` (`id`, `name`, `email`, `company`, `proposals_body`, `file`, `date`, `unread`) VALUES
(1, 'silver paul uson', 'szyslate@gmail.com', 'geeksnest', 'mayroon akong proposal sana magustuhan nyo..kasi magugustuhan niyo talaga ito', 'Nothing', '2015-01-27 14:13:58', 2),
(69, 'kyben', 'flipt_silent01@yahoo.com', 'geeksnest', 'hfkljsadhfasfiuashgdafhsdfgsdfhsdfhsfh', 'Nothing', '2015-01-27 12:55:04', 1),
(70, 'rainier', 'rainllarenas@gmail.com', 'geeksnest', 'ako ay my proposal..bow', 'Green-Nature-Eco-Friendly-Wallpapers-Large-9.jpg', '2015-02-11 05:09:16', 1),
(88, 'jimmy', 'jimmy.lantano@geeksnest.com', 'geeksnest', 'magbibigay po ako ng proposal ko', 'Screen Shot 2015-01-02 at 22.25.37.png', '2015-02-11 07:00:32', 1),
(89, 'paul', 'paul@yahoo.com.ph', 'geeksnest', 'magpropropose po ako ', '2015-02-05_07-43-12_3399d2d734e36b4a241ba729e494bd68.pdf', '2015-01-13 06:15:15', 1),
(91, 'jay', 'jay@gmail.com', 'geeksnest', 'i have a proposals plz txtback', '2015-02-05_07-48-18_attactfiles.png', '2015-02-02 05:14:24', 1),
(92, 'john', 'john@yahoo.com', 'geeksnest', 'proposal ito', '2015-02-05_08-39-25_3399d2d734e36b4a241ba729e494bd68.pdf', '2015-02-01 05:13:25', 1),
(95, 'jackson', 'jackson@gmail.com', 'MJ', 'proposals', '2015-02-05_09-34-49_Copy of sample OPCRIPCRfor WDS.xls', '2015-02-01 07:19:20', 1),
(96, 'hale', 'hale@yahoo.com', 'hale', 'shooting star', '2015-02-06_05-19-50_awaw.jpg', '2015-02-06 05:19:50', 1),
(104, 'fox', 'fox@facebook.com', 'ylvys', 'what did the fox say', '02-06-2015_06-48-12_alex prayer.docx', '02-06-2015 06:48:12', 0),
(139, 'Sandy Lauza', 'lakisungo@lips.com', 'Sungo Rider', 'meron akong proposal sana..', 'Nothing', '02-10-2015 04:10:15', 0),
(140, 'efren', 'efrenbautistajr@gmail.com', 'geeksnest', 'back to work', 'Nothing', '02-11-2015 06:44:07', 0),
(141, 'paul', 'szyslate@gmail.com', 'geeksnest', 'magpropropose po ako hah?', 'Nothing', '02-17-2015 10:10:41', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `proposals`
--
ALTER TABLE `proposals`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `proposals`
--
ALTER TABLE `proposals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=142;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
