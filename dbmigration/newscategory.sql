-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2015 at 09:32 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `newscategory`
--

CREATE TABLE IF NOT EXISTS `newscategory` (
`id` int(11) NOT NULL,
  `newsid` int(11) NOT NULL,
  `categoryid` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newscategory`
--

INSERT INTO `newscategory` (`id`, `newsid`, `categoryid`) VALUES
(6, 3, '3'),
(7, 3, '14'),
(8, 3, '27'),
(9, 4, '4'),
(10, 5, '4'),
(11, 6, '3'),
(12, 7, '3'),
(13, 8, '3'),
(14, 9, '3'),
(17, 12, '3'),
(18, 13, '3'),
(19, 14, '3'),
(20, 15, '3'),
(21, 16, '3'),
(22, 17, '3'),
(24, 19, '3'),
(25, 20, '3'),
(26, 21, '3'),
(27, 22, '3'),
(28, 23, '3'),
(29, 24, '3'),
(30, 25, '3'),
(31, 26, '3'),
(33, 28, '3'),
(34, 29, '3'),
(35, 31, '3'),
(36, 32, '4'),
(37, 33, '6'),
(41, 37, '4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `newscategory`
--
ALTER TABLE `newscategory`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `newscategory`
--
ALTER TABLE `newscategory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
