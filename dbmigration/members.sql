-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2015 at 01:39 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
`userid` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(250) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(150) NOT NULL,
  `birthday` varchar(150) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `location` text NOT NULL,
  `zipcode` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `centername` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`userid`, `username`, `email`, `password`, `firstname`, `lastname`, `birthday`, `gender`, `location`, `zipcode`, `status`, `centername`) VALUES
(1, 'Paul', 'hellohello99999@gmail.com', '76422a22903cc60ea8223f719d1d2dae5e2c2394', 'Paul', 'Yanson', '30/9/1979', 'male', 'United States', '86351', 1, ''),
(2, 'ebautistajr', 'efrenbautistajr@yahoo.com.ph', '41b20d33b55fee4b792f1d59c3c5e3f36b43a914', 'efren', 'bautista', '17/4/1997', 'male', 'AndorrA', '123', 1, ''),
(3, 'hufferb', 'huff_brian@hotmail.com', '0a2b9827e548969e4dfe1b0d16c072ef347836d6', 'Brian', 'Huff', '10/2/1973', 'male', 'United States', '30075', 0, ''),
(4, 'dbeal123', 'dbeal@powerbrainedu.com', '6d9a25fb6d7ff39996025cd942102c8a71e54158', 'Dav', 'Beal', '17/6/1977', 'male', 'United States', '11554', 1, ''),
(5, 'kyujong', 'kyujongsmile@gmail.com', 'a4cfe9febc1ed2bff08c545d9e3cf04e4a1e3d91', 'Kyu Jong', 'Hwang', '30/10/1972', 'male', 'United States', '85234', 1, ''),
(6, 'itshappyday', 'itshappyday@gmail.com', '4e6e93f0688b71b9eefd49a2d59fbd411195afc3', 'MinJae', 'Kim', '8/4/1972', 'male', 'United States', '85234', 1, ''),
(7, 'damikim', 'taradahnd@gmail.com', 'a3ea974c865249300728e97db42797d138230a38', 'dami', 'kim', '16/10/1972', 'female', 'United States', '86336', 1, ''),
(8, 'gold100', 'goldkum100@gmail.com', 'd1a366aa7f4fc34018b359470dccb253b7682543', 'Dahee', 'Kum', '6/8/1972', 'female', 'United States', '60089', 1, ''),
(9, 'Mani', 'manioh@gmail.com', 'a56a44dc282d57f5aca864efd93a56b8bb3723ca', 'Jiyoung', 'Oh', '7/5/1969', 'female', 'United States', '86351', 1, ''),
(10, 'Bama', 'freshmossk@gmail.com', '660090ffdf2b94be3a8854c1aef945bf46f4a68f', 'Bama', 'Kim', '1/10/1972', 'female', 'United States', '85234', 1, ''),
(11, '1210mjk', '1210mk@gmail.com', '642e223fb3e7952330791c5f6bddb25fa2ebe878', 'min', 'kim', '1/10/1976', 'female', 'United States', '10003', 1, ''),
(12, 'Solarbody', 'healingfamily@ilchi.com', '964640d96fbef203588799c271a6a2e597132fd1', 'Sung Wook', 'Lee', '25/1/1972', 'male', 'United States', '86351', 1, ''),
(13, 'ujandarc', 'hcpb100@yahoo.com', 'def5a931f70fb4553f3283be2adfbacacc65cb6e', 'Hongik', 'Cheong', '28/1/1970', 'female', 'United States', '91101', 1, ''),
(14, 'JungIn', 'michelle1.cgi@gmail.com', '028f89c8351004448fd3433414716108e6924151', 'Jung in', 'Moon', '23/1/1978', 'female', 'United States', '07624', 1, ''),
(15, 'nobukotodd', 'truth.nobuko@gmail.com', '6879159d8d62185dcbee8ea8921d2a47297ac486', 'Nobuko', 'Todd', '6/3/1970', 'female', 'United States', '96816', 1, ''),
(16, 'dawnquaresima', 'powerthruunity@aol.com', '54dbf00feb7d492c0eb1e2d56846e7bceb592418', 'Dawn', 'Quaresima', '12/4/1977', 'female', 'United States', '11554', 1, ''),
(17, 'vickieoh', 'vickie.ds.oh@gmail.com', 'acdf7bf70a2fd6967bc9d5756b5beb651f9637d6', 'Daeson', 'Oh', '24/12/1972', 'female', 'United States', '86326', 1, ''),
(18, 'balgmi2010', 'balgmi2010@gmail.com', 'a524268c82806df37912bd3c328d9e9fa8600485', 'Jungsun', 'lee', '15/1/1969', 'female', 'United States', '77069', 1, ''),
(19, 'PS', 'gnshin1050@gmail.com', 'c2bc368eefae1b3b66277380598667aa18c818f0', 'Gyo E', 'Shin', '28/1/1970', 'female', 'United States', '85234', 1, ''),
(20, 'hyeranihm', 'hyeranster@gmail.com', '666a61b7ca2eacc727f71a590bb75327686d8662', 'Hyeran', 'Ihm', '26/2/1970', 'female', 'United States', '87109', 1, ''),
(21, 'bigbangfresh', 'brianafresh@gmail.com', '194b3c3a7f2a76a3a40d897ae003c6402116e5d1', 'Soyoung', 'Shin', '24/12/1973', 'female', 'United States', '20878', 1, ''),
(22, 'dap10000', 'gio1928@gmail.com', '639f4a54ab47715bb62139d0165799cdc52d721b', 'JE CHON', 'YANG', '9/7/1972', 'male', 'United States', '77057', 1, ''),
(23, 'BoGo', 'dahnhwa@gmail.com', 'e395bf069ad4e5bb0ae9d6eb997212839c35411f', 'hye', 'Jeon', '15/1/1973', 'female', 'United States', '85234', 1, ''),
(24, 'mokhwa', 'mokhwasung@gmail.com', 'afbef37357a02308d5032f3f85546265c07b84a0', 'Hwa Sook', 'Sung', '7/8/1971', 'female', 'United States', '11020', 1, ''),
(25, 'Chogreenwater', 'chogreenwater@gmail.com', '340c1a56fef592bb569d9c1fc74cf295c845764c', 'Hyung', 'Cho', '26/8/1972', 'male', 'United States', '98033', 1, ''),
(26, 'Suhopark', 'estrellamj@gmail.com', '16470effbdcb1b370cf313f53cd58f77a181ba62', 'Suho', 'Parks', '10/7/1974', 'female', 'United States', '30075', 1, ''),
(27, 'helenn32', 'helenn32@gmail.com', '06949b551b32ded629682629382955db15449c1d', 'helen', 'kim', '7/5/1969', 'female', 'United States', '97223', 1, ''),
(28, 'sayong', 'sayongk@gmail.com', 'bca722ee083f1677dbbba62131022b75c38b3e15', 'Sayong', 'Kim', '11/1/1961', 'male', 'United States', '86336', 1, ''),
(29, 'oceana', 'jh77071@gmail.com', 'c8921b8bd99448b0f705689dbe640260154f316b', 'jung', 'hong', '25/7/1970', 'female', 'United States', '33133', 1, ''),
(30, 'hyeonjakim', 'hyeonjakim@gmail.com', 'c688b7d1b9bdbb3a4ec5d2f3f53ad8d18e2b1d75', 'Garam', 'Kim', '31/3/1955', 'female', 'United States', '07401', 1, ''),
(31, 'sungcjang', 'sungcjang@gmail.com', 'c9fa9fadf999dfd42fa9edb4c0166c311674353a', 'sungchul', 'jang', '15/11/1970', 'male', 'United States', '89117', 0, ''),
(33, 'username', 'efrenbautistajr@gmail.com', 'b231c44f937bc3f6578bc2c1700d82830fbd90e0', 'Efren', 'bautista', '7/8/2005', 'male', 'Anguilla', '12314213', 0, ''),
(34, 'jjane441', 'jjane441@yahoo.com', '4c245d302564fb461a95212b6d9e70dab6a0a4ae', 'Eun Jeong', 'Lee', '22/6/1975', 'female', 'United States', '91204', 1, ''),
(35, 'taotravis', 'taotravis@gmail.com', '768668683e279d0b64d20f6afd507da4fefff8d1', 'Travis', 'Kronenberg', '16/2/1978', 'male', 'United States', '91101', 1, ''),
(36, 'iqbalsingh', 'iqbalsingh91@yahoo.com', '66d56a7f0f56206b1ba971e12dfa02f170ef3d1d', 'Iqbal', 'Singh', '1/9/1991', 'male', 'United States', '94587', 1, ''),
(37, 'rosemaryvart', 'tarzana@bodynbrain.com', '5d556373b52848434d37342ae70f4d4d9dffaf9e', 'rosemary', 'chakmakjian', '10/2/1962', 'female', 'United States', '91356', 1, ''),
(38, 'dahnottogi', 'dahnottogi0808@gmail.com', '5c7603a4d79dc996a1f6a3b947fabea51a0179c2', 'eun hee', 'kim', '8/8/1971', 'female', 'United States', '90005', 1, ''),
(39, 'vanason29', 'vannason888@gmail.com', 'ecd48e2daf5cf15d1b32b05b4ad15d6d537b6ab4', 'vanessa', 'nason', '29/12/1981', 'female', 'United States', '96701', 1, ''),
(40, 'feb111963', 'kristy4wellness@me.com', '336c6b0a61992995a64d42348d939119bc7fad1f', 'Kristy', 'Corwin', '2/11/1963', 'female', 'United States', '91301', 1, ''),
(41, 'onion012', 'onion012@hotmail.com', '26c21cdc2465c2ff1e9e689330a2e211ceaa615e', 'Michelle', 'Seo', '29/11/1980', 'female', 'United States', '86351', 1, ''),
(42, 'hellolindasuh', 'hellolindasuh@gmail.com', 'ee712728c20072d8a1fab1e0a58bf752becc2a45', 'Linda', 'Suh', '9/3/1989', 'female', 'United States', '86336', 1, ''),
(43, 'Jiu', 'haejung.jung@gmail.com', 'c79d759dc5ddfb87ce9d8028b5c18f143d29798a', 'Haejung', 'Jung', '8/6/1979', 'female', 'United States', '86336', 1, ''),
(44, 'hijjkim', 'hijjkim@gmail.com', '5e11b44d76b40148da49c1232ce2bd1f4ef5ef4e', 'Joy', 'Kim', '11/5/1973', 'female', 'United States', '86326', 0, ''),
(45, 'silvery100', 'suepark7720@gmail.com', '3512a8044eb1c17744a0b9b14734ff2437ee7750', 'SUE', 'PARK', '15/2/1977', 'female', 'United States', '86326', 1, ''),
(46, 'hellomichela', 'hellomichela@aol.com', '4e861409dbad2b3a8db9240779d21184bd82a860', 'Michela', 'Mangiaracina', '23/6/1976', 'female', 'United States', '86351', 1, ''),
(47, 'happycow', 'dmikis@gmail.com', '247a9b8b06111315d83777cba8aaf60a50e4e22b', 'Mina', 'Kim', '30/8/1985', 'female', 'United States', '86351', 0, ''),
(48, 'Rebeccatinkle', 'rebecca.tinkle@yahoo.com', '22665f9cd19cc9946cf921623d4dcab834b221e4', 'Rebecca', 'Tinkle', '1/12/1978', 'female', 'United States', '86251', 1, ''),
(49, 'salpiro', 'tonyasabumnim@yahoo.com', '865f0e1c8b4283a06696744a8a6ef819451692ff', 'Tonya', 'Whelan', '31/7/1975', 'female', 'United States', '86341', 1, ''),
(50, 'bellrocker', 'mrbellrock@gmail.com', 'f1b304932236d3cb6426497c0187af0cf7af6983', 'bell', 'rocker', '2/3/1995', 'other', 'Guinea', '12345', 0, ''),
(51, 'jhlee111', 'jhlee111@gmail.com', '63fb22d06a2fe610de01acf43beb48e73d048bb3', 'John', 'Lee', '14/11/1973', 'male', 'United States', '85284', 0, ''),
(52, 'jed726', 'mail@jordandiamond.com', '2b6b9d253760c2529aa0d194dbd30dc5e9921bd9', 'Jordan', 'Diamond', '26/7/1988', 'male', 'United States', '30328', 1, ''),
(53, 'piaceniza', 'piaceniza@yahoo.com', '5776981c876e7f434414a7285320fd7a15e51e00', 'Phaedra', 'Ceniza', '2/6/1973', 'female', 'United States', '77478', 1, ''),
(54, 'catsawai', 'catsawai1@gmail.com', 'f17db1d9ee87abaf8f03a7f05c645e5e6debd8f3', 'CAt', 'Sawai', '3/12/1966', 'female', 'United States', '96701', 1, ''),
(55, 'AnneO', 'annejoffe@gmail.com', '9301b35032750568f325ad1f6c5dace144f2525a', 'Anne', 'Offe', '7/5/1950', 'female', 'United States', '77450', 0, ''),
(60, 'jimmyblantanojr@gmail.com', 'jimmyblantanojr@gmail.com', '1f8ac10f23c5b5bc1167bda84b833e5c057a77d2', 'Jimmy', 'Lantano', '17/6/1985', 'Male', 'United States', '2428', 0, 'AZ - Glendale'),
(61, 'jimmyblantanojr@yahoo.com', 'jimmyblantanojr@yahoo.com.ph', '1f8ac10f23c5b5bc1167bda84b833e5c057a77d2', 'Jimmy', 'Lantano', '17/6/1985', 'Male', 'United States', '2428', 0, 'AZ - Glendale'),
(62, 'test', 'test@gmail.com', '1f8ac10f23c5b5bc1167bda84b833e5c057a77d2', 'test', 'test', '01/1/1960', 'Male', 'United States', '1234', 0, 'AZ - Scottsdale');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
 ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
