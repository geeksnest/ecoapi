-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2015 at 12:58 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `menusub`
--

CREATE TABLE IF NOT EXISTS `menusub` (
`id` int(11) NOT NULL,
  `sub_name` text NOT NULL,
  `page_id` int(11) NOT NULL,
  `main_id` int(11) NOT NULL,
  `sub1` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menusub`
--

INSERT INTO `menusub` (`id`, `sub_name`, `page_id`, `main_id`, `sub1`) VALUES
(1, 'teswt', 20, 1, 0),
(2, 'test1', 6, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menusub`
--
ALTER TABLE `menusub`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menusub`
--
ALTER TABLE `menusub`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
